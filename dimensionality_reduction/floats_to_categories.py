# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import sys
import numpy as np
from scipy.misc import comb
from itertools import product, combinations


class BasicPartition:

    def __init__(self, data, variables, should_keep_order, n_categories=None):
        """
        Given some inputs they are stored

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        variables : iterable
                    list of labels of the variables to be categorized
        should_keep_order : Boolean
                            Must the created categories keep the pre-existing order relation?
        n_categories : integer or dictionary
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a dictionary its length must be the same as the list of vaiables.
        """

        # Store input parameters for further use
        self.data = data.copy()
        self.variables = variables
        self.nData = data.shape[0]
        self.keep_order = should_keep_order
        self.information_gain = {}
        self.mdl_index = {}
        self.labels = {}

        # Calculate bounds of each variable
        self.bounds = {}
        for variable in self.variables:
            # Calculate candidates to be the extremes of the intervals
            array = self.data[[variable]].values
            self.bounds[variable] = (np.min(array), np.max(array))

        # Modify the type of n_categories if required
        if n_categories is None:
            pass
        elif isinstance(n_categories, int):
            n_categories = {key: n_categories for key in variables}
        elif isinstance(n_categories, dict):
            if set(n_categories.keys()) != set(variables):
                raise ValueError("All the elements, and no more, must be keys of <n_categories>")
            else:
                for key, value in n_categories.items():
                    if not isinstance(value, int):
                        raise ValueError("The value of the key {} of <n_categories> should be an"
                                         " integer instead of {}".format(key, value))
        else:
            raise ValueError("Variable <n_categories> must be an integer or a dictionary.")
        self.n_categories = n_categories

    def _create_labels_from_partition(self):
        """
        Given a tuple of cut points, descriptive labels are generated
        """
        # To begin with create as many labels as intervals
        for variable in self.variables:
            cuts = self.cut_points[variable]
            self.labels[variable] = []
            for index in range(len(cuts) - 1):
                a, b = cuts[index], cuts[index + 1]
                label = "{0:.2e} < x <= {1:.2e}".format(a, b)
                self.labels[variable].append(label)

    def _merge_intervals(self, threshold=0.01):
        """
        Merge intervals by checking if a combination of them do not produce a large loss of
         information.

        Parameters
        ----------
        threshold : float
                    maximum value of the information loss allowed at each merge
        """

        new_dict_of_labels = {}
        for variable in self.variables:
            # To begin with create as many labels as intervals
            labels = self.labels[variable]
            # Evaluate reference
            out = self._information_gain(self.cut_points[variable], variable, labels)
            ig_ref = out[0]
            while True:
                unique_labels = set(labels)
                min_ig_loss = np.inf
                # Check all possible combinations of intervals to select the optimum merger
                for l1, l2 in combinations(unique_labels, 2):
                    # Modify labels
                    mod_labels = [l1 if label == l2 else label for label in labels]
                    # Evaluate information gain of the new partition
                    out = self._information_gain(self.cut_points[variable], variable, mod_labels)
                    ig_loss = 1. - out[0] / ig_ref
                    # print(mod_labels)
                    # If the information lost is lower than the current best candidate, the current
                    #  partition substitute the previous best
                    if ig_loss < min_ig_loss:
                        min_ig_loss = ig_loss
                        pair_to_be_merged = (l1, l2)
                # Is the loss lower than the maximum accepted? Then accept the new partition and try
                #  to look for a new one
                keep_on_looping = min_ig_loss < threshold
                if keep_on_looping:
                    l1, l2 = pair_to_be_merged
                    new_label = " OR ".join(pair_to_be_merged)
                    labels = [label if (label != l1 and label != l2) else new_label
                              for label in labels]

                else:
                    break
            new_dict_of_labels[variable] = labels
        self.labels = new_dict_of_labels

    def _remove_duplicates(self):
        """
        Removes contiguous intervals with the same label
        """

        for variable in self.variables:
            # Look for repeated, consecutive labels
            mask = [self.labels[variable][ii] != self.labels[variable][ii + 1]
                    for ii in range(len(self.labels[variable]) - 1)]
            # Filter labels and cut points
            labels = [self.labels[variable][0]]
            cut_points = [self.cut_points[variable][0]]
            for index, flag in enumerate(mask):
                if flag:
                    labels.append(self.labels[variable][index + 1])
                    cut_points.append(self.cut_points[variable][index + 1])
            cut_points.append(self.cut_points[variable][-1])
            # Update variables
            self.labels[variable] = labels
            self.cut_points[variable] = tuple(cut_points)

    def _substitute_floats_by_categories(self):
        """
        The values of the variables of the original database are substituted by the categories
        """

        if self.keep_order:
            for variable in self.variables:
                if len(set(self.labels[variable])) != len(self.labels[variable]):
                    self.keep_order = False
                    sys.stdout.write("WARNING! Variables have been merged and order relations "
                                     "cannot be kept.\n")
                    break

        # Substitute float values by labels
        for variable in self.variables:
            cuts = self.cut_points[variable]
            values = self.data[variable].values.copy()
            for index in range(len(cuts) - 1):
                a, b = cuts[index], cuts[index + 1]
                mask = np.logical_and(values >= a, values <= b)
                self.data.loc[mask, variable] = index
            self.data[variable] = self.data[variable].astype("category")

        if not self.keep_order:
            for variable in self.variables:
                self.data[variable].categories = self.labels[variable]

    def _evaluate_fitness(self):
        """
        Evaluate the infomation gain and the MLP (Minimum Description Length) index at each
         variable using the current partition

        Returns
        -------
        None
        """

        for variable in self.variables:
            if len(self.labels) != 0:
                labels = self.labels[variable]
            else:
                labels = np.array(range(len(self.cut_points[variable]) - 1))
            out = self._information_gain(self.cut_points[variable], variable, labels)
            self.information_gain[variable], self.mdl_index[variable] = out

    def _information_gain(self, cuts, variable, labels=None):
        """
        Calculates the gain in the information by splitting the distribution by some points.

        Parameters
        ----------
        cuts : list
               values where the distribution is going to be cut
        variable : string
                   label of the variable to be taken into consideration

        Returns
        -------
        inf_gain : float
                   value of the information gain
        delta : float
                value of the threshold of the minimum, acceptable information gain
        """
        # Entropy of the original distribution
        ht, k, nDataFullRange = self._calculate_entropy([(cuts[0], cuts[-1])], variable)
        if nDataFullRange == 1:
            return -np.inf, 0.
        dt = np.log2(nDataFullRange - 1)
        # Entropy of the conditional distribution
        htp = 0.
        if k < 10:
            dtp = np.log2(3 ** k - 2.) - k * ht
        else:
            dtp = k * (np.log2(3.) - ht)
        if labels is None:
            for index in range(len(cuts) - 1):
                hc, k, nPieceOfData = self._calculate_entropy([(cuts[index], cuts[index + 1])],
                                                              variable)
                p = nPieceOfData / nDataFullRange
                htp += p * hc
                dtp += k * hc
        else:
            if len(cuts) != len(labels) + 1:
                raise ValueError("The length of <cuts> must be equal to the length of <labels> + 1")
            for label in set(labels):
                indexes = [ii for ii, l in enumerate(labels) if l == label]
                intervals = []
                for index in indexes:
                    intervals.append((cuts[index], cuts[index + 1]))
                hc, k, nPieceOfData = self._calculate_entropy(intervals, variable)
                p = nPieceOfData / nDataFullRange
                htp += p * hc
                dtp += k * hc

        # Return information gain
        inf_gain = (ht - htp) * nDataFullRange / self.nData
        delta = (dt - dtp) / nDataFullRange * nDataFullRange / self.nData
        return inf_gain, delta

    def _calculate_entropy(self, intervals, variable):
        """
        Calculates the entropy of the distribution of one variable of the dataframe within a given
         range.

        Parameters
        ----------
        lower_bound : float
                      value of the minimum of the interval
        upper_bound : float
                      value of the maximum of the interval
        variable : string
                   label of the variable to be treated

        Returns
        -------
        h : float
            value of the entropy
        k : integer
            number of different classes within the interval
        nData : integer
                number of points within the range
        """
        mask = np.zeros(self.data[variable].shape, dtype=bool)
        for interval in intervals:
            lower_bound, upper_bound = interval
            # print(lower_bound, upper_bound)
            partial_mask = np.logical_and(self.data[variable] > lower_bound,
                                          self.data[variable] <= upper_bound)
            mask = np.logical_or(mask, partial_mask)

        nData = np.sum(mask)
        h, k = 0., 0
        for state in self.categories:
            if nData == 0:
                x = 0
            else:
                x = np.sum(np.logical_and(mask, self.data[self.target] == state)) / nData
            h -= x * np.log2(x + 1e-300)
            if not np.isclose(x, 0.):
                k += 1

        return h, k, nData


class PartitionGivenIntervals(BasicPartition):

    def __init__(self, data, variables, cut_points, should_keep_order=True):
        """
        Given some non-compulsory inputs they are stored and, if requered, checked if they are
         correctly formatted.

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        variables : iterable
                    list of labels of the variables to be categorized
        cut_points : dictionary of lists
                     values of the points that defines a partitions of the domain for each variable
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        """

        super().__init__(data=data, variables=variables, should_keep_order=should_keep_order)

        # Check and store the variable <cut_points>
        if cut_points is not None:
            flag = isinstance(cut_points, dict)
            flag &= set(cut_points.keys()) == set(variables)
            if flag:
                for variable in variables:
                    flag &= isinstance(cut_points[variable], (list, np.array))
            if not flag:
                raise ValueError("Variable cutpoints should be a dictionary of lists of floats.")
            self.cut_points = cut_points

        self._substitute_database()


class EquallySpacedPartition(BasicPartition):

    def __init__(self, data, variables, n_categories, target, should_keep_order=True,
                 must_merge_categories=True):
        """
        Creates categories from floating-point variables by defining equispaced categories.
        Similar to EquispacedPartition, but partitions the data based on the values in the
        target column.

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        variables : iterable
                    list of labels of the variables to be categorized
        n_categories : integer or dictionary
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a dictionary its length must be the same as the list of vaiables.
        target : string
                 label of the variable to be predicted, that is, the variable that will be used to
                  determine where the variables must be splitted.
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        must_merge_categories : Boolean, optional
                                Must it try to merge categories to reduce their number?
        """
        # Initilize
        super().__init__(data=data, variables=variables,
                         should_keep_order=should_keep_order,
                         n_categories=n_categories)
        self.target = target
        self.categories = data[target].unique()

        # Obtain equispaced partitions
        self.cut_points = {}
        for variable in variables:
            self.cut_points[variable] = tuple(np.linspace(self.bounds[variable][0],
                                                          self.bounds[variable][1],
                                                          self.n_categories[variable] + 1))
        # Post-process data
        if target is not None:
            self._evaluate_fitness()
        self._create_labels_from_partition()
        if must_merge_categories:
            self._merge_intervals()
            self._remove_duplicates()
        self._substitute_floats_by_categories()


class EquispacedPartition:

    def __init__(self, data, variables, n_partitions, initial_dx=0.25, min_dx=1e-3,
                 threshold=1e-2, should_keep_order=True):
        """
        Given a Pandas DataFrame the float values of some of its variables are substituted by
        discrete categories. Similar to EquallySpacedPartition but partitions based on each
        column.

        Parameters
        ----------
        data : Pandas DataFrame
               Original dataset
        variables : iterable
                    list of labels of the variables to be categorized
        n_partitions : integer or list
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a list its length must be the same as the list of variables.
        initial_dx : float
                     Value of the step at the beginning of the Hooke-Jeeves / Pattern search
                     optimization algorithm.
        min_dx : float
                 Minimum value of the step for the Hooke-Jeeves / Pattern search optimization
                  algorithm.
        threshold : float, optional
                    For values of deviation from the equally-distributed partition lower than this
                     the iterative process is stopped.
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        """

        # Initialize some values
        self.data = data.copy()
        self.variables = variables
        self.n_samples = data.shape[0]
        self.keep_order = should_keep_order
        self.cuts = {}
        # Modify the type of n_categories if required
        if isinstance(n_partitions, int):
            n_partitions = {key: n_partitions for key in variables}
        elif isinstance(n_partitions, dict):
            if set(n_partitions.keys()) != set(variables):
                raise ValueError("All the elements, and no more, must be keys of <n_categories>")
            else:
                for key, value in n_partitions.items():
                    if isinstance(value, int):
                        raise ValueError("The value of the key {} of <n_categories> should be an"
                                         " integer instead of {}".format(key, value))
        else:
            raise ValueError("Variable <n_categories> must be an integer or a dictionary.")
        self.n_partitions = n_partitions

        # Calculate partitions for all variables
        for variable in self.variables:
            ordered_data = np.sort(self.data[variable].values)
            # Define optimal, target distribution of points
            n_target = [int(np.round(self.n_samples / self.n_partitions[variable]))
                        for _ in range(self.n_partitions[variable])]
            n_target[-1] = self.n_samples - np.sum(n_target[:-1])

            # Calculate initial condition
            lower_bound = ordered_data[n_target[0]]
            upper_bound = ordered_data[-n_target[-1]]
            cutpoints = self._create_equispaced_partition(lower_bound, upper_bound, variable)
            minimum_error = self._evaluate_error(ordered_data, cutpoints, n_target, variable)

            dx = initial_dx
            interval_size = upper_bound - lower_bound
            while minimum_error > threshold:
                dl = dx * interval_size
                results = []
                for sl, su in product([-1, +1], repeat=2):
                    cutpoints = self._create_equispaced_partition(lower_bound + sl * dl,
                                                                  upper_bound + su * dl,
                                                                  variable)
                    error = self._evaluate_error(ordered_data, cutpoints, n_target, variable)
                    results.append((sl, su, error))

                sl, su, error = sorted(results, key=lambda x: x[2])[0]
                if error > minimum_error:
                    dx /= 2
                    if dx < min_dx:
                        break
                else:
                    minimum_error = error
                    lower_bound += sl * dl
                    upper_bound += su * dl

            # Store the extremes of the intervals for further use
            cutpoints = list(self._create_equispaced_partition(lower_bound, upper_bound, variable))
            cutpoints = [ordered_data[0]] + cutpoints + [ordered_data[-1]]
            self.cuts[variable] = cutpoints

        # Substitute float values by labels
        self.labels = {}
        for variable in self.variables:
            cuts = self.cuts[variable]
            values = self.data[variable].values
            self.labels[variable] = []
            for index in range(len(cuts) - 1):
                a, b = cuts[index], cuts[index + 1]
                label = "{0:.2e} < x <= {1:.2e}".format(a, b)
                self.labels[variable].append(label)
                mask = np.logical_and(values >= a, values <= b)
                self.data.loc[mask, variable] = label

        if self.keep_order:
            for variable in variables:
                self.data[variable] = self.data[variable].replace(self.labels[variable],
                                                                  range(len(self.labels[variable])))

    def _create_equispaced_partition(self, lower_bound, upper_bound, variable):
        """

        Parameters
        ----------
        lower_bound : float
                      value of the lowest cutpoint. It defines the interval -inf < x <=lower_bound
        upper_bound : float
                      value of the highest cutpoint.
        variable : string
                   label of the variable to treat

        Returns
        -------
        cutpoints : NumPy array
                    values of the boundaries of each interval
        """

        # Initialize bounds of each interval
        cutpoints = np.nan * np.zeros((self.n_partitions[variable] - 1,))
        cutpoints[0] = lower_bound
        cutpoints[-1] = upper_bound
        #
        l = cutpoints[-1] - cutpoints[0]
        dl = l / (self.n_partitions[variable] - 2)
        for ii in range(1, self.n_partitions[variable] - 2):
            cutpoints[ii] = cutpoints[0] + ii * dl
        return cutpoints

    def _evaluate_error(self, dataset, cutpoints, n_target, variable):
        """
        Evaluates the error of the equispaced partition to have equally-distributed points

        Parameters
        ----------
        dataset : NumPy array
                  data in ascending order
        cutpoints : list
                    values of the cutpoints
        n_target : list
                   target, equispaced distribution
        variable : string
                   label of the variable to treat

        Returns
        -------
        error : float
                value of the deviation of the current distribution from the equally-spaced.
        """

        n_actual = [np.sum(dataset < cutpoints[0])]
        for ii in range(self.n_partitions[variable] - 2):
            n = np.sum(np.logical_and(dataset >= cutpoints[ii], dataset < cutpoints[ii + 1]))
            n_actual.append(n)
        n_actual.append(np.sum(dataset >= cutpoints[-1]))
        sum_squared_errors = np.sum([(v1 - v2) ** 2 for v1, v2 in zip(n_actual, n_target)])
        return np.sqrt(sum_squared_errors) / self.n_samples


class ITBPartition(BasicPartition):
    """
    Information Theory Based Partition
    """

    def __init__(self, data, target, variables, n_categories, should_calculate_boundaries=True,
                 minimum_range=0.1, standarization_criterion={}, should_keep_order=True):
        """
        Given a Pandas DataFrame the float values of some of its variables are substituted by
         discrete categories.

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        target : string
                 label of the variable to be predicted, that is, the variable that will be used to
                  determine where the variables must be splitted.
        variables : iterable
                    list of labels of the variables to be categorized
        n_categories : integer or list
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a list its length must be the same as the list of vaiables.
        should_calculate_boundaries : Boolean, optional
                                      Calculating the boundary points might be expensive if the
                                       dataset is large but, anyway, it is up to the user.
        minimum_range : float, optional
                        minimum size of each of the intervals in which every variable is splitted.
        standarization_criterion : string or dictionary
                                   for each variable (key) the method used to normalize the minimum
                                    range value (value) is stated. If any variable in <variables> is
                                    not a key of the dictionary, minimum range will be used as it is
                                    over the values of the unmodified varibale.
                                   If the value is a string same value is applied to all variables.
                                   Values of this dictionary can only be "normal" or "minmax".
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        """

        # Initialize
        super().__init__(data=data, variables=variables,
                         should_keep_order=should_keep_order,
                         n_categories=n_categories)
        self.target = target
        self.categories = data[target].unique()
        self.should_calculate_boundaries = should_calculate_boundaries
        self.minimum_range = minimum_range
        if isinstance(standarization_criterion, str):
            self.standarization_criterion = {var: standarization_criterion for var in variables}

        # Calculate minimum ranges for each variable
        self.interval_size = {}
        for variable in variables:
            nm = standarization_criterion.get(variable, None)
            if nm == "normal":
                std = np.std(self.data[variable].values)
                self.interval_size[variable] = minimum_range * std
            elif nm == "minmax":
                min_value = np.min(self.data[variable].values)
                max_value = np.max(self.data[variable].values)
                self.interval_size[variable] = (max_value - min_value) * minimum_range
            elif nm is None:
                self.interval_size[variable] = minimum_range
            else:
                error_msg = "Normalization method <{}> for variable <{}> is not implemented."
                raise ValueError(error_msg.format(nm, variable))

        # Calculate candidates to be cut points
        self.candidates = {}
        for variable in variables:
            # Calculate candidates to be the extremes of the intervals
            array = data[[variable]].values
            self.bounds[variable] = (np.min(array), np.max(array))
            array = data[[variable, self.target]].values
            if should_calculate_boundaries:
                candidates = self._look_for_boundaries(variable)
            else:
                candidates = array[array[:, 0].argsort()][:, 0]
            self.candidates[variable] = self._filter_candidates(candidates,
                                                                self.interval_size[variable])

    def _look_for_boundaries(self, variable):
        """
        Seeks the boundaries of the data, those points where the class changes from one point to
         the next.

        Parameters
        ----------
        variable : string
                   label of the variable to be taken into consideration

        Returns
        -------
        boundaries : iterable
                     values of the boundary points
        """
        # Take all data related to the variable and the categories and sort them using the variable
        array = self.data[[variable, self.target]].values
        array = array[array[:, 0].argsort()]
        # For each point calculate the midpoint between it and the next
        midpoints = np.zeros((self.nData - 1, 2))
        # TODO: We could probably speed up this code by doing something like:
        # midpoints = 0.5 * (array[:-1] + array[1:])
        # But I'm a coward and won't change code at this point of the project...
        for indx in range(self.nData - 1):
            midpoints[indx, :] = 0.5 * (array[indx, :] + array[indx + 1, :])
        # Return the boundaries after deleting repeated values
        boundaries = midpoints[np.abs((midpoints[:, 1] - 0.5) % 1) < 1e-10][:, 0]
        boundaries = list(set(boundaries)) + [-np.inf, np.inf]
        return sorted(boundaries)

    @staticmethod
    def _filter_candidates(candidates, minimum_range):
        """
        Given a set of candidates this method takes some of them so no two candidates are too near.

        Parameters
        ----------
        candidates : iterable
                     values of the candidate points
        minimum_range : float
                        minimum size of each interval

        Returns
        -------
        candidates : NumPy array
                     filtered candidates
        """
        if len(candidates) < 2:
            return candidates
        # Get minimum and maximum values
        min_val = np.min(candidates)
        max_val = np.max(candidates)
        # Initiate the list of filtered candidates with the minimum value
        filtered_candidates = [min_val]
        # For all inputted values check if they are further from the last added-to-the-list point.
        #  If so, add it to the list
        for elem in candidates[1:]:
            if (elem - filtered_candidates[-1]) >= minimum_range:
                filtered_candidates.append(elem)
        # If the last point is too near the maximum it must be removed
        if max_val != np.inf and (max_val - filtered_candidates[-1]) < minimum_range:
            filtered_candidates.pop()
        # Remove the first point
        filtered_candidates.pop(0)
        return np.array(filtered_candidates)


class OptimalPartition(ITBPartition):

    def __init__(self, data, target, variables, n_categories, should_calculate_boundaries=True,
                 minimum_range=0.1, n_maximum_iters=1000, standarization_criterion={},
                 should_keep_order=True, must_merge_categories=True):
        """
        Given a Pandas DataFrame the float values of some of its variables are substituted by
         discrete categories.

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        target : string
                 label of the variable to be predicted, that is, the variable that will be used to
                  determine where the variables must be splitted.
        variables : iterable
                    list of labels of the variables to be categorized
        n_categories : integer or dictionary
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a dictionary its length must be the same as the list of vaiables.
        should_calculate_boundaries : Boolean, optional
                                      Calculating the boundary points might be expensive if the
                                       dataset is large but, anyway, it is up to the user.
        minimum_range : float, optional
                        minimum size of each of the intervals in which every variable is splitted.
        n_maximum_iters : integer, optional
                          number of maximum iterations that should be performed for each variable.
        standarization_criterion : string or dictionary
                                   for each variable (key) the method used to normalize the minimum
                                    range value (value) is stated. If any variable in <variables> is
                                    not a key of the dictionary, minimum range will be used as it is
                                    over the values of the unmodified varibale.
                                   If the value is a string same value is applied to all variables.
                                   Values of this dictionary can only be "normal" or "minmax".
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        must_merge_categories : Boolean, optional
                                Must it try to merge categories to reduce their number?
        """

        # Initialize
        super().__init__(data=data, target=target, variables=variables,
                         n_categories=n_categories,
                         should_calculate_boundaries=should_calculate_boundaries,
                         minimum_range=minimum_range,
                         standarization_criterion=standarization_criterion,
                         should_keep_order=should_keep_order)

        # Split each variable into non-intersecting intervals
        optimal_cuts = {}
        for variable in variables:
            # Perform all-at-once partition
            maximum_information = -1.
            n_options = comb(len(self.candidates[variable]), self.n_categories[variable] - 1)
            prob = n_maximum_iters / n_options
            for cuts in combinations(self.candidates[variable], self.n_categories[variable] - 1):
                if np.random.rand() > prob:
                    continue
                cuts = [-np.inf] + list(cuts) + [np.inf]
                inf_gain, _ = self._information_gain(cuts, variable)
                if inf_gain > maximum_information:
                    maximum_information = inf_gain
                    optimal_cuts[variable] = cuts

        # Store the extremes of the intervals for further use
        self.cut_points = optimal_cuts
        # Post-process data
        self._evaluate_fitness()
        self._create_labels_from_partition()
        if must_merge_categories:
            self._merge_intervals()
            self._remove_duplicates()
        self._substitute_floats_by_categories()


class BinarySplitPartition(ITBPartition):

    def __init__(self, data, target, variables, n_categories, should_calculate_boundaries=True,
                 minimum_range=0.1, standarization_criterion={}, threshold=0.015,
                 should_keep_order=True, must_merge_categories=True):
        """
        Given a Pandas DataFrame the float values of some of
        its variables are substituted by discrete categories.

        Parameters
        ----------
        data : Pandas DataFrame
               dataset
        target : string
                 label of the variable to be predicted, that is, the variable that will be used to
                  determine where the variables must be splitted.
        variables : iterable
                    list of labels of the variables to be categorized
        n_categories : integer or dictionary
                       number of categories in which the variables must be splitted. If it is an
                        integer, every variable will be splitted into the same number of categories.
                        If it is a dictionary its length must be the same as the list of vaiables.
        should_calculate_boundaries : Boolean, optional
                                      Calculating the boundary points might be expensive if the
                                       dataset is large but, anyway, it is up to the user.
        minimum_range : float, optional
                        minimum size of each of the intervals in which every variable is splitted.
        standarization_criterion : string or dictionary
                                   for each variable (key) the method used to normalize the minimum
                                    range value (value) is stated. If any variable in <variables> is
                                    not a key of the dictionary, minimum range will be used as it is
                                    over the values of the unmodified varibale.
                                   If the value is a string same value is applied to all variables.
                                   Values of this dictionary can only be "normal" or "minmax".
        threshold : float, optional
                    All those points that provide a value of the ratio information gain / entropy
                     of the variable without any partition lower than this threshold are removed.
                     Only used if <calculate_using_tree> is True.
        should_keep_order : Boolean, optional
                            Must the created categories keep the pre-existing order relation?
        must_merge_categories : Boolean, optional
                                Must it try to merge categories to reduce its number?
        """

        # Initialize
        super().__init__(data=data, target=target, variables=variables,
                         n_categories=n_categories,
                         should_calculate_boundaries=should_calculate_boundaries,
                         minimum_range=minimum_range,
                         standarization_criterion=standarization_criterion,
                         should_keep_order=should_keep_order)
        self.threshold = threshold

        # Split each variable into non-intersecting intervals
        optimal_cuts = {}
        for variable in variables:
            # Perform binary partitions
            left_boundary = (0, self.bounds[variable][0], 0., 1.)
            right_boundary = (0, self.bounds[variable][1], 0., 1.)
            self.maximum_tier = np.ceil(np.log2(self.n_categories[variable])) + 1
            partition = self._binary_split([left_boundary, right_boundary], 1, variable)
            # Delete those cut-points that do not verify the MDLP criterion. MDLP comes from
            # Minimum Description Length Principle.
            while True and len(partition) > 2:
                l = sorted(partition, key=lambda x: (-x[0], x[3]))[0]
                if l[3] <= 0.:
                    index = partition.index(l)
                    partition.pop(index)
                else:
                    break
            # If the information gained by adding a given cut is lower than the inputted
            #  threshold then remove it.
            ht, _, _ = self._calculate_entropy([(self.bounds[variable][0],
                                                 self.bounds[variable][1])],
                                               variable)
            while True and len(partition) > 2:
                l = sorted(partition, key=lambda x: (-x[0], x[2]))[0]
                if l[2] <= self.threshold * ht:
                    index = partition.index(l)
                    partition.pop(index)
                else:
                    break
            # Remove points if there are too many categories
            while len(partition) > (self.n_categories[variable] + 1):
                l = sorted(partition, key=lambda x: (-x[0], x[2]))[0]
                index = partition.index(l)
                partition.pop(index)
            optimal_cuts[variable] = list(zip(*partition))[1]

        # Store the extremes of the intervals for further use
        self.cut_points = optimal_cuts
        # Post-process data
        if target is not None:
            self._evaluate_fitness()
        self._create_labels_from_partition()
        if must_merge_categories:
            self._merge_intervals()
            self._remove_duplicates()
        self._substitute_floats_by_categories()

    def _binary_split(self, boundaries, current_tier, variable):
        """
        Splits a the values of a given variable in two pieces

        Parameters
        ----------
        boundaries : List of tuples
                     List that contains the tuples (tier, boundary, information gain) for all the
                      points added up to this iteration
        current_tier : integer
                       depth of the tree at which the process currently is
        variable : string
                   label of the variable to be processed

        Returns
        -------
        boundaries : List of tuples
                     As the inputted variable plus the new cut point
        """
        # Have the process reached the maximum, allowed depth? Then break
        if current_tier == self.maximum_tier:
            return boundaries
        # Calculate which candidates are in the current range
        boundary_left, boundary_right = boundaries
        candidates = self.candidates[variable]
        filtered_candidates = candidates[np.logical_and(candidates > boundary_left[1],
                                                        candidates < boundary_right[1])]
        # Look for the boundary which maximizes the information gain. That is the point by which we
        #  want to split the current interval.
        optimum_candidate = None
        maximum_information = 0.
        threshold = None
        for candidate in filtered_candidates:
            mi, delta = self._information_gain([boundary_left[1], candidate, boundary_right[1]],
                                               variable)
            if mi > maximum_information:
                maximum_information = mi
                optimum_candidate = candidate
                threshold = delta
        new_boundary = optimum_candidate
        # If there is no candidates stop the process. Otherwise execute this splitting method over
        #  the new two intervals in which the current interval have been splitted.
        if new_boundary is None:
            return boundaries
        else:
            new_boundary = (current_tier, new_boundary, maximum_information,
                            maximum_information - threshold)

            left_partition = self._binary_split([boundary_left, new_boundary], current_tier + 1,
                                                variable)
            right_partition = self._binary_split([new_boundary, boundary_right], current_tier + 1,
                                                 variable)
            return left_partition[:-1] + right_partition


