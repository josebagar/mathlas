# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import gzip
from pathlib import Path
try:
    import cPickle as pickle
except ImportError:
    import pickle


class MathlasObject(object):
    """
    This is a base object class customized for Mathlas.
    It implements automatic caching and reloading of derived objects
    as long as the attributes in such objects are picklable
    """

    def __init__(self):
        super(MathlasObject, self).__init__()

    def cache(self, path, compress=True, unstorableAttribs=None):
        """
        Cache the current object to the given path in disk.

        This should work for all the attributes that the user stores,
        as long as they're picklable.

        Parameters
        ----------
        path : string or pathlib.Path
               Path where the pickle'd object will get written to as a pickle
               object.

        compress : boolean, optional
                   Whether the compressed file should be written to in a
                   compressed form (through gzip) or not.

                   The default is to use compression when pickling.

        unstorableAttribs : iterable, optional
                            iterable (list, tuple, set...) with the names of the
                            object attributes which should not be stored
        """
        # Convert to a Path object
        path = Path(path)

        if unstorableAttribs is None:
            unstorableAttribs = set()
        elif not isinstance(unstorableAttribs, set):
            unstorableAttribs = set(unstorableAttribs)

        # The object that will get stored are those that are part
        # of the object but not part of the class
        # Not perfect, but works fine for our case
        objectAttribs = set(dir(self))
        classAttribs = set(dir(self.__class__))
        storableAttribs = objectAttribs - classAttribs - unstorableAttribs
        attribs = {}
        for attrib in storableAttribs:
            attribs[attrib] = getattr(self, attrib)

        # Store the objects, create the dir if needed
        path.parent.mkdir(mode=0o755, parents=True, exist_ok=True)

        if compress:
            with gzip.open(str(path), 'wb') as o:
                pickle.dump(attribs, o)
        else:
            with path.open('wb') as o:
                pickle.dump(attribs, o)

    @classmethod
    def from_cache(cls, path):
        """
        Create a MathlasObject object by loading a previously stored cache file

        This method will just call the __new__ method for a class, then call cls._load
        on the given path

        Parameters
        ----------
        path: str or pathlib.Path
              Path to the JSON file containing the textual description
              of the Customer Journey.

        Returns
        -------
        obj : MathlasObject
        """
        obj = cls.__new__(cls)
        obj._load(path)

        return obj

    def _load(self, path, return_data=False):
        """
        Load object state from a previously stored pickle object.

        The method automatically determines if the given file is compressed
        with gzip or not, and acts accordingly

        Parameters
        ----------
        path : string or pathlib.Path
               Path where the pickle'd object will get read from.
               The file reading process is first attempted  through gzip and,
               if that fails, as an uncompressed file.
        return_data : Boolean
                      Whether data should be returned for further inspection
                      without any modifications to the self object
         """
        # Convert to a Path object
        path = Path(path)

        # Store the object in path into data
        # First, try opening the file with gzip, if that fails, try opening
        # normally, otherwise fail
        try:
            with gzip.open(str(path), 'rb') as i:
                data = pickle.load(i)
        except IOError:
            with path.open('rb') as i:
                data = pickle.load(i)

        if return_data:
            return data
        else:
            for key in data.keys():
                setattr(self, key, data[key])
