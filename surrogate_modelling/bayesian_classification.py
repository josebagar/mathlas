# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
import pandas as pd
from mathlas.surrogate_modelling.rbf_setup import RBFSetup
from itertools import product, combinations_with_replacement
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject as pco


class BayesianClassification(RBFSetup):

    def __init__(self, training_data, result_column, rescaling="standarizing", training_method=None,
                 path=None):

        super(BayesianClassification, self).__init__(training_data, result_column, False,
                                                     rescaling, path)
        self.likelihood = None
        self.derivatives = None
        self.category_labels = sorted(list(set(training_data[result_column])))
        self.n_categories = len(self.category_labels)
        if training_method is None:
            self.training_method = "multiple" if self.n_categories > 2 else "binary"
        else:
            self.training_method = training_method
        self.categories = np.zeros((self.n_samples, self.n_categories))
        indexes = self.training_data[result_column].replace(self.category_labels, range(len(self.category_labels))).values
        self.categories[np.arange(self.n_samples), indexes] = 1.

    def train(self, rbf_label, scale_parameter,
              centres=None,
              are_centres_normalized=None):

        # Initialize basis
        self.rbf_label = rbf_label
        self.rbf_function = self.rbfs[rbf_label]
        self._set_centres(centres, are_centres_normalized)
        self.scale_parameter = scale_parameter
        # Calculate the covariance matrix and, from it, the linear coefficients
        self._build_covariance_matrix()

        if self.training_method == "binary":
            self.likelihood = self.likelihood_binary
            self.derivatives = self.derivatives_binary
            self.w = np.zeros(self.n_centres)
        elif self.training_method == "multiple":
            self.likelihood = self.likelihood_multiple
            self.derivatives = self.derivatives_multiple
            self.w = np.zeros((self.n_centres, self.n_categories))
        else:
            raise ValueError("Inputted method to train regression is not recognized")

        n_its = 0
        while True:
            y, _, ll = self.likelihood()
            g, _, d = self.derivatives()

            if np.linalg.norm(g) < 1e-6 or n_its > 100:
                break
            elif np.any(np.isnan(g)) or np.any(np.isinf(g)):
                if self.training_method == "binary":
                    self.w = np.random.randn(self.n_centres)
                elif self.training_method == "multiple":
                    self.w = np.zeros((self.n_centres, self.n_categories))
                else:
                    raise ValueError("Inputted method to train regression is not recognized")

            else:

                phi = (1. + np.sqrt(5)) / 2.

                x1 = 0.
                _, _, e1 = self.likelihood(self.w.copy() + x1 * d)

                x4 = 1.
                _, _, e4 = self.likelihood(self.w.copy() + x4 * d)

                x2 = x4 - (x4 - x1) / phi
                _, _, e2 = self.likelihood(self.w.copy() + x2 * d)

                x3 = x1 + (x4 - x1) / phi
                _, _, e3 = self.likelihood(self.w.copy() + x3 * d)

                while (x4 - x1) > 0.1:
                    if (e2 < e1 and e2 < e3) or (e1 < e2 and e1 < e3 and e1 < e4):
                        x1, e1 = x1, e1
                        x4, e4 = x3, e3
                        x3, e3 = x2, e2
                        x2 = x4 - (x4 - x1) / phi
                        _, _, e2 = self.likelihood(self.w.copy() + x2 * d)
                    elif (e3 < e2 and e3 < e4) or (e4 < e1 and e4 < e2 and e4 < e3):
                        # x4, e4 = x4, e4
                        x1, e1 = x2, e2
                        x2, e2 = x3, e3
                        x3 = x1 + (x4 - x1) / phi
                        _, _, e3 = self.likelihood(self.w.copy() + x3 * d)
                    else:
                        break

                x = [x1, x2, x3, x4][np.argmin([e1, e2, e3, e4])]
                x = 1e-3 if x < 1e-3 else x
                self.w += x * d
            n_its += 1

    def likelihood_binary(self, w=None):

        if w is None:
            w = self.w.copy()

        a = self.C.T.dot(w)
        y = self.sigmoid(a)
        e = y - self.sampled_values

        v1 = self.sampled_values * np.log(y + 1e-16)
        v2 = (1. - self.sampled_values) * np.log(1. - y + 1e-16)
        ll = -1. * (v1 + v2).sum() / self.n_samples

        return y, e, ll

    def likelihood_multiple(self, w=None):

        if w is None:
            w = self.w.copy()

        a = self.C.T.dot(w)
        y = self.softmax(a)
        e = y - self.categories
        ll = -1. * (self.categories * np.log(y + 1e-16)).sum() / self.n_samples

        return y, e, ll

    def derivatives_binary(self):

        a = self.C.T.dot(self.w)
        y = self.sigmoid(a)
        e = y - self.sampled_values
        v = y * (1. - y)
        g = self.C.dot(e)
        h = self.C.dot(np.diag(v)).dot(self.C.T) + 1e-16 * np.eye(self.n_centres)

        g, h = g / self.n_samples, h / self.n_samples
        try:
            d = - np.linalg.solve(h, g)
        except:
            d = - g

        return g, h, d

    def derivatives_multiple(self):

        a = self.C.T.dot(self.w)
        y = self.softmax(a)
        e = y - self.categories
        g = self.C.dot(e)
        h = np.zeros((self.n_centres * self.n_categories, self.n_centres * self.n_categories))
        for ii, jj in combinations_with_replacement(range(self.n_categories), 2):
            cte = 1 if ii == jj else 0
            v = y[:, ii] * (cte - y[:, jj])
            piece_of_h = self.C.dot(np.diag(v)).dot(self.C.T) + 1e-16 * np.eye(self.n_centres)
            ii0, ii1 = self.n_centres * ii, self.n_centres * (ii + 1)
            jj0, jj1 = self.n_centres * jj, self.n_centres * (jj + 1)
            h[ii0:ii1, jj0:jj1] = h[jj0:jj1, ii0:ii1] = piece_of_h
        h += 1e-10 * np.eye(h.shape[0])

        g, h = g / self.n_samples, h / self.n_samples
        try:
            d = - np.linalg.solve(h, g.T.flatten())
            d = np.reshape(d, (self.n_categories, self.n_centres)).T
        except:
            d = - g

        return g, h, d

    def check_derivative_binary(self, rbf_label, scale_parameter, centres=None, seed=211104,
                                n_tests=10, delta=1e-4, are_centres_normalized=True):
        # Initialize methods to evaluate likelihoods and derivatives
        self.likelihood = self.likelihood_binary
        self.derivatives = self.derivatives_binary
        # Initialize basis
        self.rbf_label = rbf_label
        self.rbf_function = self.rbfs[rbf_label]
        self._set_centres(centres, are_centres_normalized)
        self.scale_parameter = scale_parameter
        # Calculate the covariance matrix and, from it, the linear coefficients
        self._build_covariance_matrix()

        np.random.rand(seed)

        for _ in range(n_tests):
            # Initialize weights
            self.w = np.random.randn(self.n_centres)
            w0 = self.w.copy()
            # Calculate analytical gradient and Hessian
            g, h, _ = self.derivatives()
            # Calculate difference between analytical and numerical gradient
            err_g = g.copy()
            _, _, ll0 = self.likelihood()
            for ii in range(self.n_centres):
                # Calculate forward point
                self.w = w0.copy()
                self.w[ii] += delta
                _, _, llp = self.likelihood()
                # Calculate backward point
                self.w = w0.copy()
                self.w[ii] -= delta
                _, _, llm = self.likelihood()
                # Subtract the numerical derivative to the analytical
                err_g[ii] -= (llp - llm) / (2. * delta)
            # Calculate difference between analytical and numerical Hessian
            err_h = h.copy()
            for i1, i2 in combinations_with_replacement(range(self.n_centres), 2):
                if i1 == i2:
                    # Calculate forward point
                    self.w = w0.copy()
                    self.w[i1] += delta
                    _, _, llp = self.likelihood()
                    # Calculate backward point
                    self.w = w0.copy()
                    self.w[i1] -= delta
                    _, _, llm = self.likelihood()
                    # Subtract the numerical derivative to the analytical
                    err_h[i1, i1] -= (llp - 2. * ll0 + llm) / (delta * delta)
                else:
                    # Calculate forward-forward point
                    self.w = w0.copy()
                    self.w[i1] += delta
                    self.w[i2] += delta
                    _, _, llpp = self.likelihood()
                    # Calculate forward-backward point
                    self.w = w0.copy()
                    self.w[i1] += delta
                    self.w[i2] -= delta
                    _, _, llpm = self.likelihood()
                    # Calculate backward-forward point
                    self.w = w0.copy()
                    self.w[i1] -= delta
                    self.w[i2] += delta
                    _, _, llmp = self.likelihood()
                    # Calculate backward-backward point
                    self.w = w0.copy()
                    self.w[i1] -= delta
                    self.w[i2] -= delta
                    _, _, llmm = self.likelihood()
                    # Subtract the numerical derivative to the analytical
                    v = (llpp - llpm - llmp + llmm) / (4. * delta * delta)
                    err_h[i1, i2] -= v
                    err_h[i2, i1] -= v

            eg = np.linalg.norm(err_g) / np.sqrt(err_g.size)
            eh = np.linalg.norm(err_h) / np.sqrt(err_h.size)
            msg = "Difference in gradient and Hessian are {:3e} and {:3e}, respectively\n"
            sys.stdout.write(msg.format(eg, eh))

    def check_derivatives_multiple(self, rbf_label, scale_parameter, centres=None, seed=211104,
                                   n_tests=10, delta=1e-4, are_centres_normalized=True):

        self.likelihood = self.likelihood_multiple
        self.derivatives = self.derivatives_multiple
        # Initialize basis
        self.rbf_label = rbf_label
        self.rbf_function = self.rbfs[rbf_label]
        self._set_centres(centres, are_centres_normalized)
        self.scale_parameter = scale_parameter
        # Calculate the covariance matrix and, from it, the linear coefficients
        self._build_covariance_matrix()

        np.random.rand(seed)

        for _ in range(n_tests):
            # Initialize weights
            self.w = np.random.randn(self.n_centres, self.n_categories)
            w0 = self.w.copy()
            # Calculate analytical gradient and Hessian
            g, h, _ = self.derivatives()
            # Calculate difference between analytical and numerical gradient
            err_g = g.copy()
            _, _, ll0 = self.likelihood()
            for ii, jj in product(range(self.n_centres), range(self.n_categories)):
                # Calculate forward point
                self.w = w0.copy()
                self.w[ii, jj] += delta
                _, _, llp = self.likelihood()
                # Calculate backward point
                self.w = w0.copy()
                self.w[ii, jj] -= delta
                _, _, llm = self.likelihood()
                # Subtract the numerical derivative to the analytical
                err_g[ii, jj] -= (llp - llm) / (2. * delta)
            # Calculate difference between analytical and numerical Hessian
            err_h = h.copy()
            for i1, i2 in product(range(self.n_centres), repeat=2):
                for j1, j2 in product(range(self.n_categories), repeat=2):
                    if j1 == j2 and i1 == i2:
                        # Calculate forward point
                        self.w = w0.copy()
                        self.w[i1, j1] += delta
                        _, _, llp = self.likelihood()
                        # Calculate backward point
                        self.w = w0.copy()
                        self.w[i1, j1] -= delta
                        _, _, llm = self.likelihood()
                        # Subtract the numerical derivative to the analytical
                        rr = self.n_centres * j1 + i1
                        err_h[rr, rr] -= (llp - 2. * ll0 + llm) / (delta * delta)
                    else:
                        # Calculate forward-forward point
                        self.w = w0.copy()
                        self.w[i1, j1] += delta
                        self.w[i2, j2] += delta
                        _, _, llpp = self.likelihood()
                        # Calculate forward-backward point
                        self.w = w0.copy()
                        self.w[i1, j1] += delta
                        self.w[i2, j2] -= delta
                        _, _, llpm = self.likelihood()
                        # Calculate backward-forward point
                        self.w = w0.copy()
                        self.w[i1, j1] -= delta
                        self.w[i2, j2] += delta
                        _, _, llmp = self.likelihood()
                        # Calculate backward-backward point
                        self.w = w0.copy()
                        self.w[i1, j1] -= delta
                        self.w[i2, j2] -= delta
                        _, _, llmm = self.likelihood()
                        # Subtract the numerical derivative to the analytical
                        rr = self.n_centres * j1 + i1
                        ss = self.n_centres * j2 + i2
                        v = (llpp - llpm - llmp + llmm) / (4. * delta * delta)
                        err_h[rr, ss] -= v

            eg = np.linalg.norm(err_g) / np.linalg.norm(g)
            eh = np.linalg.norm(err_h) / np.linalg.norm(h)
            msg = "Difference in gradient and Hessian are {:3e} and {:3e}, respectively\n"
            sys.stdout.write(msg.format(eg, eh))

    def predict(self, prediction, provide_variance=False):
        """
        Get the constructed estimator's prediction at the given points.

        Calculates the best linear unbiased prediction (BLUP) for the given
        point. If `provideRMSE` is `True` the method also provides the
        root-mean-square error at those points.

        Parameters
        ----------
        prediction : NumPy array-like or Pandas DataFrame
                     Positions of the points where the estimation is
                           requested.
                           If predictionPoints is a DataFrame or the
                           `sampleValues` parameter used to construct the model
                           was one, the returned BLUP will also be a DataFrame
                           whose indices will match those of predictionPoints.

                           predictionPoints must either be a 1D or 2D array.
                           If it is a 1D array, the parameter is assumed to
                           contain the coordinates of a single point.

        provide_variance : boolean, optional
                           Must the method return the uncertainty? Default is False.

        Returns
        -------
        out : tuple or NumPy array-like
        If `provideRMSE` is `False`:
            A NumPy array or Pandas DataFrame with the BLUP.
        If `provideRMSE` is `True`:
            A tuple with the BLUP and a NumPy array with the RMSE.

        Raises
        ------
        ValueError
            If the number of columns in `points` does not match the number of
            parameters used to create the model.
        """

        normal_prediction = self.normalize(prediction[self.data_columns].values,
                                           self.points_mean, self.points_std)
        n_predictions = normal_prediction.shape[0]
        if self.rbf_label in ["Linear spline", "Cubic spline",
                              "Thin plate spline", "Quadric spline"]:
            phi = np.zeros((self.n_centres, n_predictions))
        elif self.rbf_label in ["Gaussian", "Multiquadric", "Inverse quadric",
                                "Inverse multiquadric"]:
            phi = np.ones((self.n_centres, n_predictions))
        else:
            raise ValueError("Value of <rbf_label> unrecognized.")
        for r, xt in enumerate(self.rbf_centres):
            for p, x in enumerate(normal_prediction):
                phi[r, p] = self.rbf_function(x, xt)

        y = phi.T.dot(self.w)

        if self.training_method == "binary":
            results = self.sigmoid(y)
            prediction[self.result_label] = results
            return prediction[self.result_label].values
        elif self.training_method == "multiple":
            results = self.softmax(y)
            prediction[self.result_label] = [pco({label: value for label, value
                                                  in zip(self.category_labels, vec)})
                                             for vec in results]
            # return {k: v for k, v in zip(self.category_labels, results.T)}
            return pd.DataFrame(results, columns=self.category_labels, index=prediction.index)
        else:
            raise ValueError("Training method must be either 'binary' or 'multiple'")

    @staticmethod
    def sigmoid(a):
        return 1. / (1. + np.exp(-a))

    @staticmethod
    def softmax(a, normalize=True):

        if normalize:
            y = a.max(axis=1)
            mask = y != np.abs(a).max(axis=1)
            one = np.ones(mask.shape)
            one[mask] = -1
            a -= np.repeat([one * y], a.shape[1], axis=0).T
        return (np.exp(a).T / np.sum(np.exp(a), axis=1)).T
