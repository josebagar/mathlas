# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.special import beta as betaf
from scipy.special import polygamma, betainc
from scipy.interpolate import griddata
from scipy.spatial import Delaunay
from itertools import product
from mathlas.object import MathlasObject


class Beta_Interpolation(MathlasObject):

    def __init__(self, sampledFeats=None, sampleValues=None,
                 featsRange=None, valuesRange=None, nPoints=None,
                 use_all_mesh=True, sigma_fit=0.01, sigma_smooth=0.025,
                 path=None):
        """
        Constructor of the Beta regression object, a probabilistic
         discriminative model.

        Parameters
        ----------
        sampledFeats : NumPy array, optional.
                       Co-ordinates of the sampled points. The array must
                       contain one row with co-ordinates per sampled point.
                       The number of rows in `sampledPoints` must match the
                       number of rows in `sampleValues`.

                       If None (default), `path` must be provided.

        sampleValues : NumPy array, optional.
                       Value of the sampled function at `sampledPoints`.
                       The number of rows in `sampleValues` must match the
                       number of rows in `sampledPoints`.

                       If None (default), `path` must be provided.

        featsRange : List of 2-tuple of floats.
                     List of minimum and maximum values for each of the feats.

                     If None (default), `path` must be provided.

        valuesRange : 2-tuple of floats.
                      Minimum and maximum values of the sampled function.

                      If None (default), `path` must be provided.

        nPoints : integer or list, optional.
                  Number of points used to mesh the space of features. The
                  larger the more precise the model will be.

                  If it is a list it must have the same length as sampleFeats
                  and each element is the number of points in which that feat
                  is going to be divided.

                  If None (default), `path` must be provided.

        use_all_mesh : Boolean, optional.
                       Controls if all points of the mesh must be used to
                       smooth the fitting parameters or not. If not, only
                       those points inside the convex hull plus the point
                       itself are used.

                       Default value is True.

        sigma_fit : Float, optional.
                    Standard deviation of the Gaussian model used to weight
                    the points in the process of fitting a Beta distribution
                    for each of the points of the mesh.

                    If None (default), 0.01 is chosen.

        sigma_smooth : Float, optional.
                       Standard deviation of the Gaussian model used in the
                       smoothing (convolution) process. This value should be
                       higher than `sigma_fit` or the smoothing process will
                       not be of any use.

                       If None (default), 0.05 is chosen.

        path : String, optional.
               Path where the model is going to be stored or will be read.

               If None (default), `sampledFeats` , `sampleValues`,
               `featsRange`, `valuesRange`, and `nPoints` must be provided. If
               it is provided and none of this variables must be provided, the
               model will be read. If all this variables are provided and also
               `path` is provided the object will be stored once trained.

        Raises
        ------

        ValueError
            * If a path is not provided and one the following variables is also
              not provided: `sampledFeats` , `sampleValues`, `featsRange`,
              `valuesRange`, and `nPoints`.
            * If `sampleValues` is not as long as `sampledFeats`.
            * If length of `featsRange` is not equal to the length of
              `sampledFeats`.
            * If `nPoints` is a list but its length is not equal to the length
              of `sampledFeats`.
        """
        # If a path is provided and no other input is provided the object is
        #  loaded from file instead of training the model
        if path is not None and (sampledFeats is None and
                                 sampleValues is None and
                                 featsRange is None and
                                 valuesRange is None and
                                 nPoints is None):
            self._load(path)
            return
        else:
            # If no path is provided all we check that all the required inputs
            #  are correctly initialized.
            if (sampledFeats is None or
                    sampleValues is None or
                    featsRange is None or
                    nPoints is None):
                raise ValueError("The values of X, Y, xRanges, yRange, and" +
                                 " nx must be provided as inputs")

        # Store values of the feats
        self.sampledFeats = sampledFeats
        self.nSampledPoints, self.nParameters = sampledFeats.shape
        self.nFunctions = 1

        # Check that data are coherent
#         for d in range(self.nParameters):
#             minX = np.min(self.sampledFeats[:, d])
#             maxX = np.max(self.sampledFeats[:, d])
#             if minX < featsRange[d][0] or maxX > featsRange[d][1]:
#                 raise ValueError("Some values of the feats are out the " +
#                                  "provided ranges")

        # Store values of the response function
        if self.nSampledPoints != len(sampleValues):
            raise ValueError("Response vector is not as large as training" +
                             " set of feats")
        self.sampleValues = sampleValues

        # Check that data are coherent
        minY = np.min(self.sampleValues)
        maxY = np.max(self.sampleValues)
        if minY < valuesRange[0] or maxY > valuesRange[1]:
            raise ValueError("Some values of the response are out the " +
                             "provided ranges")

        # Check and store values of the ranges
        if self.nParameters != len(featsRange):
            raise ValueError("Number of ranges is not equal to the number" +
                             " of feats")
        # Calculate and store the minimum and maximum values for all the feats
        self.minX = []
        self.maxX = []
        for this_range in featsRange:
            minX, maxX = this_range
            self.minX.append(minX)
            self.maxX.append(maxX)
        self.minX = np.array(self.minX)
        self.maxX = np.array(self.maxX)
        self.minY, self.maxY = valuesRange

        # Store other inputs
        self.sigma_fit = sigma_fit
        self.sigma_smooth = sigma_smooth
        self.use_all_mesh = use_all_mesh

        # Turn feats and response into non-dimensional variables
        self.XI = (sampledFeats - self.minX) / (self.maxX - self.minX)
        self.ETA = (sampleValues - self.minY) / (self.maxY - self.minY)

        # Generate meshes
        if isinstance(nPoints, int):
            nPoints = [nPoints for _ in range(self.nParameters)]
        elif isinstance(nPoints, list) and len(nPoints) != self.nParameters:
            raise ValueError("<nx> does not have the correct size")
        meshes = [np.linspace(mn, mx, n) for mn, mx, n in
                  zip(self.minX, self.maxX, nPoints)]
        self.nPoints = nPoints

        # Calculation of the beta functions
        if len(meshes) == 1:
            iterator = product(meshes[0])
        else:
            iterator = product(*meshes)

        # Loop for all points of the meshes to calculate the weighted model
        self.alpha, self.beta, self.scatterPoints = [], [], []
        for p in iterator:
            # Make point co-ordinates non-dimensional
            pt = (np.array(p) - self.minX) / (self.maxX - self.minX)
            # Calculate weights
            N = 0
            f = 1.
            while N < 10:
                d = np.sum((pt - self.XI) ** 2, 1)
                W = np.exp(- 0.5 * d / (f * sigma_fit) ** 2)
                N = np.sum(W)
                f *= 2

            # Calculate initial estimation
            if f > 10e300:
                alpha = 1e-300
                beta = 1e-300
            else:
                f0 = np.sum(W * np.log(self.ETA[:, 0])) / N
                f1 = np.sum(W * np.log(1. - self.ETA[:, 0])) / N
                g0 = np.exp(f0)
                g1 = np.exp(f1)
                f = (0.5 / (1. - g0 - g1))
                alpha = 0.5 + g0 * f
                beta = 0.5 + g1 * f

                # Calculate values of alpha and beta using a Newton-Raphson
                # method to iterate
                should_loop = alpha < 1000 and beta < 1000
                while should_loop:
                    # Calculate some useful values
                    a0, a1 = polygamma(0, alpha), polygamma(1, alpha)
                    b0, b1 = polygamma(0, beta), polygamma(1, beta)
                    c0, c1 = polygamma(0, alpha + beta), polygamma(1, alpha + beta)
                    # Evaluate function
                    f = np.array([[a0 - c0 - f0], [b0 - c0 - f1]])
                    # Evaluate Jacobian of the function
                    jf = np.array([[a1 - c1, -c1], [-c1, b1 - c1]])
                    # Modify values and decide if continue iteration
                    delta = np.linalg.solve(jf, f)
                    alpha -= delta[0, 0]
                    beta -= delta[1, 0]
                    should_loop = np.linalg.norm(delta) > 1e-6

            # store values for further use
            self.scatterPoints.append(pt)
            self.alpha.append(alpha)
            self.beta.append(beta)

        # Convert mesh of fitting parameters to Numpy arrays
        self.scatterPoints = np.array(self.scatterPoints)
        self.alpha = np.array(self.alpha)
        self.beta = np.array(self.beta)

        # Smooth the values of the fitting parameters
        a, b = self._smoothingExponents()
        self.alpha = a
        self.beta = b

        # If a path is provided results are stored
        if path is not None:
            self.cache(path)

    def _smoothingExponents(self):
        """
        Method to smooth the fitting parameters.

        Returns
        -------
        out : 2-tuple of NumPy arrays.
              Arrays of the smoothed values of the alpha and beta fitting
              parameters.
        """
        # Initialize variables
        alpha, beta = [], []
        if self.use_all_mesh:
            # If all points must be used then data is dumped.
            L = self.scatterPoints
            A = self.alpha
            B = self.beta
            mask = [True for _ in range(len(L))]
        else:
            # If we do no want to use all the mesh, only those points inside
            # the convex hull are used.
            L, A, B, mask = [], [], [], []
            if self.nParameters == 1:
                # If only a single feat is used the convex hull reduces to the
                # range between the minimum and the maximum.
                x0, x1 = min(self.XI), max(self.XI)
                for indx, xi in enumerate(self.scatterPoints):
                    if x0 <= xi <= x1:
                        L.append(xi)
                        A.append(self.alpha[indx])
                        B.append(self.beta[indx])
                        mask.append(True)
                    else:
                        mask.append(False)
            else:
                # For several feats the convex hull must be calculated.
                hull = Delaunay(self.XI)
                for indx, xi in enumerate(self.scatterPoints):
                    if self._in_hull(xi, hull):
                        L.append(xi)
                        A.append(self.alpha[indx])
                        B.append(self.beta[indx])
                        mask.append(True)
                    else:
                        mask.append(False)
        # Convert list of points of the mesh into a Numpy array.
        L = np.array(L)
        for indx, xi in enumerate(self.scatterPoints):
            if mask[indx]:
                # If the point is inside the convex hull, all those point
                # inside it are used.
                this_L = L[:]
                this_A = A[:]
                this_B = B[:]
            else:
                # If the point is outside the convex hull, all those point
                # inside it plus the current point is used.
                this_L = np.array(list(L) + [xi])
                this_A = A + [self.alpha[indx]]
                this_B = B + [self.beta[indx]]

            # Gaussian weights are calculated
            d2 = np.sum((xi - this_L) ** 2, 1)
            W = np.exp(- 0.5 * d2 / self.sigma_smooth ** 2)
            # Fitting parameters are smoothed
            alpha.append(np.exp(np.sum(W * np.log(this_A)) / np.sum(W)))
            beta.append(np.exp(np.sum(W * np.log(this_B)) / np.sum(W)))
        return np.array(alpha), np.array(beta)

    def getPrediction(self, X, provideRMSE=False,
                      predictionType="Approximate median"):
        """
        Provides a prediction and, if explicitly asked for, the standard
        deviation.

        Parameters
        ----------
        X : NumPy array.
            Non-normalized values of the feats. `M1xK` size.

        provideRMSE : Boolean, optional.
                      Parameter that controls if the standard deviation is
                      provided or not.
        predictionType : string, optional.
                         Indicates what type of prediction should be provided.
                         It can take the values Average, Approximate median,
                         Median, and Mode.

        Returns
        -------
        out : Numpy array of floats.
              It may be a 1D or 2D array (see `productOfVariables` above).
        """
        # Normalize inputs
        XI = (X - self.minX) / (self.maxX - self.minX)
        # Interpolate values of the fitting parameters
        alphaInt, betaInt = self._interpolateExponents(XI)

        # Convert to array
        alphaInt = np.asarray(alphaInt)
        betaInt = np.asarray(betaInt)
        # Get prediction
        if predictionType == "Average":
            prediction = alphaInt / (alphaInt + betaInt)
        elif predictionType == "Approximate median":
            prediction = (alphaInt - 1. / 3.) / (alphaInt + betaInt - 2. / 3.)
        elif predictionType == "Median":
            prediction = self._solveIncompleteBeta(alphaInt, betaInt, 0.5)
        elif predictionType == "Mode":
            prediction = (alphaInt - 1.) / (alphaInt + betaInt - 2.)
        prediction = self.minY + (self.maxY - self.minY) * prediction
        # Evaluate standard deviation
        if provideRMSE:
            variance = alphaInt * betaInt
            variance /= (alphaInt + betaInt + 2.)
            variance /= (alphaInt + betaInt) ** 2.
            return prediction, np.sqrt(variance)

        return prediction

    def getPdf(self, X, Y, productOfVariables=False):
        """
        Calculates the probability of getting a response Y if X has happened,
        i.e., p(Y|X).

        Parameters
        ----------
        X : NumPy array.
            Non-normalized values of the feats. `M1xK` size.

        Y : NumPy array.
            Non-normalized values of the responses whose probability must be
            calculated. `M2x1`

        productOfVariables : Boolean.
                             If True, M1 must be equal to M2 and the
                             probability, p(Y[j, 0]|X[j,:]) is calculated for
                             each row; thus, the result is an array of size
                             `Mx1` where `M=M1=M2`. If False For each value of
                             the response and each point in the features space
                             a probability is calculated. In this case an array
                             of size `M1xM2` is obtained; it has the values
                             p(Y[j,0]|[X[k, :]).

        Returns
        -------
        out : Numpy array of floats.
              It may be a 1D or 2D array (see `productOfVariables` above).
        """
        # Normalize inputs
        XI = (X - self.minX) / (self.maxX - self.minX)
        # Interpolate values of the fitting parameters
        alphaInt, betaInt = self._interpolateExponents(XI)

        results = []
        if not productOfVariables:
            for alpha, beta, y in zip(alphaInt, betaInt, Y):
                if alpha < 0.1 or beta < 0.1:
                    results.append(0.)
                else:
                    results.append(self.evaluatePdf(y, alpha, beta))
        else:
            for alpha, beta in zip(alphaInt, betaInt):
                if alpha < 0.1 or beta < 0.1:
                    results.append(0.)
                else:
                    results.append(self.evaluatePdf(Y, alpha, beta))
        return np.array(results)

    def getCdfQuantile(self, X, Y):
        """
        Evaluates the quantile for a given response.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Y : float or NumPy array.
            Non-normalized values of the responses.

        Returns
        -------
        Q : float or Numpy array.
            Quantiles.
        """
        # Normalize inputs
        XI = (X - self.minX) / (self.maxX - self.minX)
        # Interpolate values of the fitting parameters
        alphaInt, betaInt = self._interpolateExponents(XI)

        eta = betainc(alphaInt, betaInt, Y)
        return eta

    def getCdfPoint(self, X, Q):
        """
        Evaluates the value of the response for a given quantile.

        Parameters
        ----------
        X : float or NumPy array.
            Non-normalized values of the feats.

        Q : float or Numpy array.
            Quantiles.

        Returns
        -------
        Y : float or NumPy array.
            Non-normalized values of the responses.
        """
        # Normalize inputs
        XI = (X - self.minX) / (self.maxX - self.minX)
        # Interpolate values of the fitting parameters
        alphaInt, betaInt = self._interpolateExponents(XI)

        eta = self._solveIncompleteBeta(alphaInt, betaInt, Q)
        return self.minY + (self.maxY - self.minY) * eta

    def _solveIncompleteBeta(self, alpha, beta, Q):
        """
        Given a list of exponents of a beta distribution and a list of
        quantiles, the values of the variable for which the CDF takes the
        quantile value is calculated.

        Parameters
        ----------
        alpha : float or NumPy array.
                First exponent of the beta distribution.

        beta : float or NumPy array.
               Second exponent of the beta distribution.

        Q : float or NumPy array.
            Value of the quantile.

        Returns
        -------
        result : NumPy array.
                 Array of values of the variable for which the beta
                 distribution reachs quantile Q.

        Notes
        -----
        `alpha` and `beta` must have the same type: float or NumPy array. If
        `alpha` and `beta` are scalars, `Q` must also be an scalar. If `Q` is
        an array, `alpha` and `beta` must be arrays. However, `Q` can be an
        scalar even if `alpha` and `beta` are arrays; in that case the same
        value of Q is used for all the pairs (`alpha`, `beta`).

        All arrays must be of the be of the same size.
        """
        # exponents may be scalar values, not arrays. Turn them into vectors.
        if isinstance(alpha, (int, float)):
            alpha = np.array([alpha])
        if isinstance(beta, (int, float)):
            beta = np.array([beta])

        # Even if the exponent are arrays, the quantile can be a scalar. In
        #  that case it is supposed that all the pairs of exponents are solved
        # for the same quantile.
        N = len(alpha)
        if isinstance(Q, (int,  float)):
            Q = np.array([Q] * N)

        # After all this transformations all the arrays must have the same size
        if not (N == len(beta) == Q.shape[0]):
            raise ValueError("Inputs do not have the correct size")

        # Solve the incomplete beta using a bisection method
        result = []
        for a, b, q in zip(alpha, beta, Q):
            x0, x2 = 0., 1.
            y0 = betainc(a, b, x0) - q
            y2 = betainc(a, b, x2) - q
            if y0 * y2 > 0:
                raise ValueError("Something strange has happened")
            err = x2 - x0
            while err > 1e-4:
                x1 = 0.5 * (x0 + x2)
                y1 = betainc(a, b, x1) - q

                if y0 * y1 > 0:
                    x0 = x1
                    y0 = y1
                else:
                    x2 = x1
                    y2 = y1
                err = x2 - x0
            result.append(x1)

        return np.asarray(result)

    def evaluatePdf(self, Y, alpha, beta):
        """
        Evaluates the value of the beta pdf.

        Parameters
        ----------
        Y : NumPy array.
            Non-normalized values of the responses whose probability must be
            calculated.

        alpha : Float.
                Value of the first exponent of the Beta distribution.

        beta : Float.
               Value of the second exponent of the Beta distribution.

        Returns
        -------
        out : Numpy array of floats.
              Values of the pdf.
        """
        # Evaluate pdf
        xi = (Y - self.minY) / (self.maxY - self.minY)
        B = betaf(alpha, beta)
        gap = self.maxY - self.minY
        if B == 0:
            # If alpha, beta >> 1, there may be problems. The Stirling
            # approximation is used.
            ly = ((alpha - 1.) * np.log(xi + 1e-300))
            ly += ((beta - 1.) * np.log(1. - xi + 1e-300))
            ly -= np.log(gap)
            a = alpha - 1.
            b = beta - 1.
            c = alpha + beta - 1.
            lb = (a * np.log(a) - a + 0.5 * np.log(2. * np.pi * a) +
                  b * np.log(b) - b + 0.5 * np.log(2. * np.pi * b) -
                  c * np.log(c) + c - 0.5 * np.log(2. * np.pi * c))
            return np.exp(ly - lb)
        else:
            # Otherwise calculation is straightforward.
            y = (xi ** (alpha - 1.)) * ((1. - xi) ** (beta - 1.))
            return y / B / gap

    def _interpolateExponents(self, XI):
        """
        Provides values of the exponents for values of the feats not
        calculated, that is, the exponents are interpolated.

        Parameters
        ----------
        XI : NumPy array.
            Normalized values of the features.

        Returns
        -------
        alphaInt : Float.
                   Interpolated values of the first exponent of the Beta
                   distribution.

        betaInt : Float.
                  Interpolated values of the second exponent of the Beta
                  distribution.
        """

        # Choose interpolation method
        if self.nParameters in [1, 2]:
            method = "cubic"
        else:
            method = "linear"
        # Interpolate values of the fitting parameters
        alphaInt = np.exp(griddata(self.scatterPoints, np.log(self.alpha), XI,
                                   method))
        betaInt = np.exp(griddata(self.scatterPoints, np.log(self.beta), XI,
                                  method))
        return alphaInt, betaInt

    def _in_hull(self, p, hull):
        """
        Test if points in `p` are in `hull`.

        Parameters
        ----------
        p : NumPy array, optional.
            Co-ordinates of the points we want to know if it is inside the
            convex hull or not. It should be a `NxK` Numpy array of `N` points
            in `K` dimensions

        hull : NumPy array or scipy.spatial.Delaunay object.
               If it is not a Delaunay object it must be the coordinates of
               `M` points in `K`dimensions for which Delaunay triangulation
               will be computed.

        Returns
        -------
        out : Numpy array of boolean.
              True if the point is inside the convex hull.
        """
        if not isinstance(hull, Delaunay):
            hull = Delaunay(hull)
        return hull.find_simplex(p) >= 0
