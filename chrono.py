# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time


class Chrono(object):
    """Chronograph utilities"""

    def __init__(self):
        """Starts a Chronograph"""
        self.origin = time.time()
        self.start = self.origin
        self.finish = self.origin
        self.end = None
        self.lapse = float(0)
        self.glapse = float(0)
        self.lapse_str = '%.3f' % self.lapse
        self.glapse_str = '%.3f' % self.glapse

    def click(self):
        """Pauses the chrono and computes the elapsed time since last click"""
        self.finish = time.time()  # Check the time
        # Computes the elapsed time since last click event
        self.lapse = self.finish - self.start
        self.lapse_str = '%.3f' % self.lapse
        # Restart the start_click for future events
        self.start = self.finish

    def stop(self, restart=True):
        """Pauses the chrono and computes the elapsed time since start"""
        self.end = time.time()  # Check the time
        # Computes the global elapsed time
        self.glapse = self.end - self.origin
        self.glapse_str = '%.3f' % self.glapse
        # Restart the Chrono if requested
        if restart:
            self.__init__()
