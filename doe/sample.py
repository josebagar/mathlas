# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Auxiliary class for representing the sampling of a vectorial function in a multi-dimensional
parameter space
"""

import pandas as pd


class Sample(object):
    def __init__(self, dims, indep_labels=None, dep_labels=None,
                 genmode='empty'):
        """
        Constructor

        Parameters
        ----------
        dims : tuple (two elements)

               * dims[0] Number of independent vars
               * dims[1] Number of dependent vars

        indep_labels : list of strings
                       Each element corresponds to the
                       label of the corresponding indepenent variable (columns in dataframe X)

        dep_labels : list of strings
                     Each element corresponds to the

                     label of the corresponding dependent variable (columns in dataframe Y)

        genmode : string

                  * 'empty' Generates an empty sampling.
        """

        if genmode == 'empty':
            # Independent variables DataFrame: X
            if indep_labels:
                try:
                    assert dims[0] == len(indep_labels)
                except:
                    raise ValueError('dims[0] and len(indep_labels) must ' +
                                     'coincide')
                self.X = pd.DataFrame(columns=indep_labels)
            else:
                self.X = pd.DataFrame(columns=range(dims[0]))

            # Dependent variables DataFrame: Y
            if dep_labels:
                self.Y = pd.DataFrame(columns=dep_labels)
                try:
                    assert dims[1] == len(dep_labels)
                except:
                    raise ValueError('dims[1] and len(dep_labels) must ' +
                                     'coincide')
            else:
                self.Y = pd.DataFrame(columns=range(dims[1]))
        else:
            raise ValueError('Incorrect Sampling argument "genmode"')

    def add(self, X, Y, labels=None, overwrite=False):
        """
        Add one or more points to the sample

        Parameters
        ----------
        X : list of lists
            Each element is a list of floats that define
            the value of the independent variables at each point. Warning: if only
            one point is added, X must be a list whose only element is a list.

        Y : list of lists
            Each element is a list of floats that define
            the dependent variables at each point. Warning: if only
            one point is added, Y must be a list whose only element is a list.

        labels : list of strings
                 Each element corresponds to the label
                 of the point. If not labels, the method generate it automatically.

        overwrite : Boolean

                    * If False: if any label already exists raises an ValueError
                    * If True: allows overwriting if any label already exists
        """
        # Check input arguments:
        try:
            assert len(X) == len(Y)
        except:
            raise ValueError('Number of points in X and Y must coincide')

        # Check new labels in case overwriting not allowed
        if (not overwrite and labels):
            coincidences = set(labels).intersection(set(self.X.index))
            if coincidences:
                raise ValueError('Overwrite not allowed')

        # Define the points labels
        if not labels:
            np0 = len(self.X.index)  # Number of points already in the
            # dataframe
            np1 = np0 + len(X)
            labels = range(np0, np1)

        # Check dimensions:
        nXcol = len(self.X.columns)
        nYcol = len(self.Y.columns)
        try:
            for x, y in zip(X, Y):
                assert len(x) == nXcol
                assert len(y) == nYcol
        except:
            raise ValueError('X and Y elements and Sample columns ' +
                             'dimensions must coincide.')

        # Add points
        for label, point in zip(labels, X):
            self.X.loc[label] = point
        for label, point in zip(labels, Y):
            self.Y.loc[label] = point

    def setlabels(self, indep_labels, dep_labels):
        """
        Set the labels of the X and Y dataframes.

        Parameters
        ----------
        indep_labels : list of strings
                      Each element corresponds to the
                      label of the corresponding indepenent variable (columns in dataframe X)

        dep_labels : list of strings
                     Each element corresponds to the
                     label of the corresponding dependent variable (columns in dataframe Y)

        Returns
        -------
        X : list of lists
            Each element correspond to the list of
            values of the independent variables at the points.

        Y : list of lists
            Each element correspond to the list of
            values of the dependent variables at the points.
        """
        try:
            self.X.columns = indep_labels
            self.Y.columns = dep_labels
        except:
            raise ValueError('indep_labels and dep_labels dimensions must ' +
                             'coincide with X.columns and Y.columns ' +
                             'dimensions, respectively.')

    def get_points(self, points_id):
        """
        Return one or more points from the sampling

        Parameters
        ----------
        points_id : list

                    * If list of integers: indexes of the points to be returned
                    * If lists of strings: labels of the points to be returned
        """

        X = []
        Y = []

        different_types = set([type(elem) for elem in points_id])
        if len(different_types) > 1:
            raise ValueError('points_id must be a list of integers or a list' +
                             'of strings')
        if len(different_types) == 1:
            if different_types == {int}:  # List of indexes provided
                try:
                    for i in points_id:
                        X.append([self.X.iloc[i][col]
                                  for col in self.X.columns])
                        Y.append([self.Y.iloc[i][col]
                                  for col in self.Y.columns])
                except:
                    raise ValueError('Some points_id indexes out of range')
            elif different_types == {str}:  # List of labels provided
                try:
                    for label in points_id:
                        X.append([self.X.loc[label][col]
                                  for col in self.X.columns])
                        Y.append([self.Y.loc[label][col]
                                  for col in self.Y.columns])
                except:
                    raise ValueError('Same points_id labels not in indexes')
            else:
                raise TypeError('Elements of points_id must be int or str')
        return X, Y
