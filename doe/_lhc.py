# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Private module that imports a pure-python or a Numba implementation
of the required routines depending on the available environment
"""

import numpy as np
try:
    # Use numba if available and version number is above the
    # minimum required version
    from numba import jit, float64, __version__
    reqVersion = [0, 20, 0]
    if [int(i) for i in __version__.split('.')] >= [0, 20, 0]:
        withNumba = True
    else:
        withNumba = False
    del reqVersion
except ImportError:
    withNumba = False
    import warnings
    warnings.warn('Please install numba >= 0.20.0 for a faster LHC implementation')
from itertools import combinations, product


if withNumba:
    # The @jit decorator is not available if numba could not be imported,
    # hence the need to hide it inside the "if" block
    @jit(nopython=True)
    def __dot(v1, v2):
        """Calculate the dot product of two vectors."""
        if v1.shape[0] != v2.shape[0]:
            raise ValueError('Vector sizes do not match')

        norm = float64(0.0)
        for i in range(v1.shape[0]):
            norm += v1[i]*v2[i]

        return norm

    @jit(nopython=True)
    def __evaluate_distances(X):
        """
        Evaluate the squared distances between all the points in matrix `X`

        The distances are returned in a square matrix, with size `N`, where `N`
        is the number of points in `X`.
        Distances are measured with np.linalg.norm and are, therefore, the
        euclidean distance between points. It is assumed that the point
        coordinates are given in a cartesian system.

        Parameters
        ----------
        X : NumPy array
            A NumPy array with the cartesian coordinates of each of the points
            in the rows.

        Returns
        -------
        out : A matrix with the squared distances between the points.
              Item i-j contains the squared distance from point i to j in the
              array.

        Example
        -------
        >>>  A = np.array([[0, 0], [0, 1], [2, 0]])
        >>>  __evaluate_distances(A)
        array([[ 0.,  1.,  4.],
               [ 1.,  0.,  5.],
               [ 4.,  5.,  0.]])
        """
        N = X.shape[0]
        nDims = X.shape[1]
        D2 = np.zeros((N, N))
        vec = np.zeros(nDims)
        for ii in range(N):
            for jj in range(N):
                for kk in range(nDims):
                    vec[kk] = X[ii, kk] - X[jj, kk]
                D2[ii, jj] = D2[jj, ii] = __dot(vec, vec)

        return D2

    @jit(nopython=True)
    def __update_distances(X, D2, l):
        """
        Updates the matrix of squared distances for rows/columns whose index is
        in the provided list.

        Distances are measured with np.linalg.norm and are, therefore, the
        euclidean distance between points. It is assumed that the point
        coordinates are given in a cartesian system.

        Parameters
        ----------
        X : NumPy array
            A NumPy array with the cartesian coordinates of each of the points
            in the rows.

        D2 : NumPy array
             A matrix with the squared distances between the points.
             Item i-j contains the squared distance from point i to j in the
             array.

        l : list
            list of indices of those rows/columns to update

        Returns
        -------
        D2 : updated input of the same name

        Example
        -------
        >>>  A = np.array([[0, 0], [0, 1], [2, 0]])
        >>>  __evaluate_distances(A)
        array([[ 0.,  1.,  4.],
               [ 1.,  0.,  5.],
               [ 4.,  5.,  0.]])
        """
        nPoints = D2.shape[0]
        nDims = X.shape[1]
        D2_ = D2.copy()
        vec = np.zeros(nDims)
        for ii in l:
            for jj in range(nPoints):
                for kk in range(nDims):
                    vec[kk] = X[ii, kk] - X[jj, kk]
                D2_[ii, jj] = D2_[jj, ii] = __dot(vec, vec)

        return D2_
else:
    def __evaluate_distances(X):
        """
        Evaluate the squared distances between all the points in matrix `X`

        The distances are returned in a square matrix, with size `N`, where `N`
        is the number of points in `X`.
        Distances are measured with np.linalg.norm and are, therefore, the
        euclidean distance between points. It is assumed that the point
        coordinates are given in a cartesian system.

        Parameters
        ----------
        X : NumPy array
            A NumPy array with the cartesian coordinates of each of the points
            in the rows.

        Returns
        -------
        out : A matrix with the squared distances between the points.
              Item i-j contains the squared distance from point i to j in the
              array.

        Example
        -------
        >>>  A = np.array([[0, 0], [0, 1], [2, 0]])
        >>>  __evaluate_distances(A)
        array([[ 0.,  1.,  4.],
               [ 1.,  0.,  5.],
               [ 4.,  5.,  0.]])
        """
        N = X.shape[0]
        D2 = np.zeros((N, N))
        for ii, jj in combinations(range(N), 2):
            D2[ii, jj] = D2[jj, ii] = np.linalg.norm(X[ii, :] - X[jj, :]) ** 2

        return D2

    def __update_distances(X, D2, l):
        """
        Updates the matrix of squared distances for rows/columns whose index is
        in the provided list.

        Distances are measured with np.linalg.norm and are, therefore, the
        euclidean distance between points. It is assumed that the point
        coordinates are given in a cartesian system.

        Parameters
        ----------
        X : NumPy array
            A NumPy array with the cartesian coordinates of each of the points
            in the rows.

        D2 : NumPy array
             A matrix with the squared distances between the points.
             Item i-j contains the squared distance from point i to j in the
             array.

        l : list
            list of indices of those rows/columns to update

        Returns
        -------
        D2 : updated input of the same name

        Example
        -------
        >>>  A = np.array([[0, 0], [0, 1], [2, 0]])
        >>>  __evaluate_distances(A)
        array([[ 0.,  1.,  4.],
               [ 1.,  0.,  5.],
               [ 4.,  5.,  0.]])
        """
        N = D2.shape[0]
        D2_ = D2.copy()
        for ii, jj in product(l, range(N)):
            D2_[ii, jj] = D2_[jj, ii] = (np.linalg.norm(X[ii, :] - X[jj, :]) **
                                         2)

        return D2_
