# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
Sequential point selection method
"""
import numpy as np


def seq_select(df, npoints, initial_points=None):
    """
    Select the subset of points of df that better fill the space.

    Starting from `initial_points` add points from `df` sequentially until
    `npoints` are selected. Each step, the point of df whose distance to the
    subset of already selected points is maximum is selected.

    Parameters
    ----------
    df : pandas DataFrame
         Set of points from which points will be selected.
    npoints : integer
         Total number of points to be selected.
    initial_points : list of strings, optional
        A list of labels of the points in the initial set.
        If not given, the first point in `df` will be used as the set of
        initial points.

    Returns
    -------
    spacefillingdf : pandas DataFrame
        `npoints` points from `df` that fill the parameter space the best.
    """

    # =========================================================================
    # Normalize the parameter space
    # =========================================================================
    # Normalize columns of df linearly between 0 and 1
    df_norm = (df - df.min()) / (df.max() - df.min())

    # =========================================================================
    # Set the starting domain
    # =========================================================================
    if not initial_points:  # Only the first point of the dataframe is selected
        initial_points = [df.index[0]]

    # =========================================================================
    # Selects sequentially the points that better fill the space
    # =========================================================================
    ntpoints = df.shape[0]  # Total number of points
    distance_array = np.zeros((ntpoints, 1))  # Stores the minimum distance of
    # each point to the set of selected points. When a new point is selected
    # and added to the set, each element of the distance_array is updated
    for index in range(ntpoints):
        distance_array[index] = \
            min([np.linalg.norm(df_norm.iloc[index] -
                                df_norm.loc[label])
                 for label in initial_points])

    selected_points = list(initial_points)
    while len(selected_points) < npoints:
        # Locates the index of the point to be selected (maximum distance)
        new_index = np.argmax(distance_array)
        # Update the list of selected points
        selected_points.append(df_norm.index[new_index])

        # Update the distance array
        for index in range(ntpoints):
            distance_array[index] = \
                min([distance_array[index],
                     np.linalg.norm(df_norm.iloc[index] -
                                    df_norm.loc[selected_points[-1]])])

    # Return a dataframe with the selected points
    return df.loc[selected_points]
