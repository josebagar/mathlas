Introduction
============

This directory should contain the client certificate and key files (without password) needed in order to connect to the spanish cadastre's website.
The files must be named:
  * *certificate.crt*: User certificate
  * *certificate.key*: Certificate signing key

Now, it's obviously a **very bad idea** to keep your digital certificates unsecured, so be smart with what you do with this directory.

Extraction instructions
=======================

In order to extract the files from the PFX file that is provided by the Spanish authorities, you can perform the following actions (taken from [here](http://serverfault.com/questions/515833/how-to-remove-private-key-password-from-pkcs12-container)). I'm assuming that your certificate file is called `certificate.pfx`.
  * `openssl pkcs12 -clcerts -nokeys -in "certificate.pfx" -out certificate.crt -password pass:[PASSWORD] -passin pass:[PASSWORD]`
  * `openssl pkcs12 -nocerts -in "certificate.pfx" -out private.key -password pass:[PASSWORD] -passin pass:[PASSWORD] -passout pass:TemporaryPassword`
  * `openssl rsa -in private.key -out "certificate.key" -passin pass:TemporaryPassword`
  * `rm private.key`
