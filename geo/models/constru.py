# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
A class representing a construction object in the DB
"""

from .parcela import Parcela
from .base import CadastreInfoBase
from sqlalchemy.orm import relationship
from sqlalchemy import ForeignKeyConstraint
from sqlalchemy import Column, Integer, String, Float

# Some constants for defining part types
# Taken from annex 1 from:
#       http://www.catastro.minhap.es/ayuda/manual_descriptivo_shapefile.pdf
BALCON = 0
TRIBUNA = 1
TERRAZA = 2
PORCHE = 3
SOPORTAL = 4
PASAJE = 5
MARQUESINA = 6
PATIO = 7
COBERTIZO = 8
ENTREPLANTA = 9
SEMISOTANO = 10
ALTILLO = 11
PISCINA = 12
PISTA_TENIS = 13
ESTANQUE = 14
SILO = 15
SUELO = 16
PERGOLA = 17
DEPOSITO = 18
ESCALERA = 19
TRANSFORMADOR = 20
JARDIN = 21
JARDIN_VALORA = 22
CAMPO_FUTBOL = 23
VOLADIZO = 24
ZONA_DEPORTIVA = 25
RUINA = 26
CONSTRUCCION = 27
PRESA = 28
ESTANQUE_VALORA = 29
URBANIZACION_INTERIOR = 30
CAMPO_GOLF = 31
CAMPING = 32
HORREO = 33
PANTALAN = 34
DARSENA = 35
PISOS = 36
UNKNOWN = -1


class Constru(CadastreInfoBase):
    __tablename__ = 'CONSTRU'

    # The attributes we'll be using from the DB
    # Here we're trying to be as faithful to the existing schema
    # as possible
    # TODO: Add methods to decypher the info in `constru`
    ninterno = Column('NINTERNO', Float, primary_key=True)
    pcat1 = Column('PCAT1', String(7))
    pcat2 = Column('PCAT2', String(7))
    mapa = Column('MAPA', Integer)
    delegacio = Column('DELEGACIO', Integer)
    municipio = Column('MUNICIPIO', Integer)
    masa = Column('MASA', String(5))
    parcela_id = Column('PARCELA', String(5))
    hoja = Column('HOJA', String(7))
    tipo = Column('TIPO', String(1))
    constru = Column('CONSTRU', String(16))
    # coords are stored in EPSG:4326 (WGS 84)
    coorx = Column('COORX', Float)      # lon
    coory = Column('COORY', Float)      # lat
    numsymbol = Column('NUMSYMBOL', Integer)
    area = Column('AREA', Integer)
    fechaalta = Column('FECHAALTA', Integer)
    fechabaja = Column('FECHABAJA', Integer)
    refcat = Column('REFCAT', String(14))

    # Add the composite foreign key constraint
    __table_args__ = (ForeignKeyConstraint([masa, parcela_id],
                                           [Parcela.masa, Parcela.parcela]),
                      {})
    parcela = relationship(Parcela, backref='construs')

    # Will hold part type info
    _parts = None

    @property
    def parts(self):
        if self._parts is not None:
            return self._parts
        else:
            self._parts = []
            for part in self.constru.split('+'):
                # All the abbreviations in the cadastre
                if part == 'B':
                    self._parts.append(BALCON)
                elif part == 'T':
                    self._parts.append(TRIBUNA)
                elif part == 'TZA':
                    self._parts.append(TERRAZA)
                elif part == 'POR':
                    self._parts.append(PORCHE)
                elif part == 'SOP':
                    self._parts.append(SOPORTAL)
                elif part == 'PJE':
                    self._parts.append(PASAJE)
                elif part == 'MAR':
                    self._parts.append(MARQUESINA)
                elif part == 'P':
                    self._parts.append(PATIO)
                elif part == 'CO':
                    self._parts.append(COBERTIZO)
                elif part == 'EPT':
                    self._parts.append(ENTREPLANTA)
                elif part == 'SS':
                    self._parts.append(SEMISOTANO)
                elif part == 'ALT':
                    self._parts.append(ALTILLO)
                elif part == 'PI':
                    self._parts.append(PISCINA)
                elif part == 'TEN':
                    self._parts.append(PISTA_TENIS)
                elif part == 'ETQ':
                    self._parts.append(ESTANQUE)
                elif part == 'SILO':
                    self._parts.append(SILO)
                elif part in ('SUELO', 'TERRENY'):
                    self._parts.append(SUELO)
                elif part == 'PRG':
                    self._parts.append(PERGOLA)
                elif part == 'DEP':
                    self._parts.append(DEPOSITO)
                elif part == 'ESC':
                    self._parts.append(ESCALERA)
                elif part == 'TRF':
                    self._parts.append(TRANSFORMADOR)
                elif part == 'JD':
                    self._parts.append(JARDIN)
                elif part == 'YJD':
                    self._parts.append(JARDIN_VALORA)
                elif part == 'FUT':
                    self._parts.append(CAMPO_FUTBOL)
                elif part == 'VOL':
                    self._parts.append(VOLADIZO)
                elif part == 'ZD':
                    self._parts.append(ZONA_DEPORTIVA)
                elif part == 'RUINA':
                    self._parts.append(RUINA)
                elif part == 'CONS':
                    self._parts.append(CONSTRUCCION)
                elif part == 'PRESA':
                    self._parts.append(PRESA)
                elif part == 'ZBE':
                    self._parts.append(ESTANQUE_VALORA)
                elif part == 'ZPAV':
                    self._parts.append(URBANIZACION_INTERIOR)
                elif part == 'GOLF':
                    self._parts.append(CAMPO_GOLF)
                elif part == 'CAMPING':
                    self._parts.append(CAMPING)
                elif part == 'HORREO':
                    self._parts.append(HORREO)
                elif part == 'PTLAN':
                    self._parts.append(PANTALAN)
                elif part == 'DARSENA':
                    self._parts.append(DARSENA)
                else:
                    if part.startswith('-'):
                        code = part[1:]
                    else:
                        code = part
                    if not all([l in 'IVXLCDM' for l in code]):
                        self._parts.append(UNKNOWN)
                    else:
                        self._parts.append(PISOS)

            return self._parts

    def __repr__(self):
        return '<Constru {}: {} {}>'.format(int(self.ninterno), self.masa, self.parcela)
