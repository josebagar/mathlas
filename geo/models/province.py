# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
A class representing a province object in the DB
"""

from .base import MunicipalitiesBase
from sqlalchemy import Column, Integer, String


class Province(MunicipalitiesBase):
    __tablename__ = 'provinces'

    # The attributes we'll be using from the DB
    # Here we're trying to be as faithful to the existing schema
    # as possible
    ine_id = Column('id', Integer, primary_key=True)
    name = Column('name', String)

    def __repr__(self):
        return '<Province {}: {}>'.format(self.ine_id, self.name)
