# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
A class representing a street object in the DB
"""

from .base import CadastreInfoBase
from sqlalchemy import Column, Integer, String


class Carvia(CadastreInfoBase):
    __tablename__ = 'Carvia'

    # The attributes we'll be using from the DB
    # Here we're trying to be as faithful to the existing schema
    # as possible
    via = Column('VIA', Integer, primary_key=True)
    # Map id where this street belongs
    mapa = Column('MAPA', Integer)
    # TTGGSS (tema, grupo y subgrupo) indicating what kind
    # of entry this one is
    ttggss = Column('TTGGSS', String(6))
    # name of the street
    denomina = Column('DENOMINA', String(25))
    # Date this street was registered
    # Might be in the future, see:
    #    http://www.catastro.minhap.es/ayuda/manual_descriptivo_shapefile.pdf (annex 3)
    fechaalta = Column('FECHAALTA', Integer)
    fechabaja = Column('FECHABAJA', Integer)

    def __repr__(self):
        return '<Carvia {}: {}>'.format(self.via, self.denomina)

    def __str__(self):
        return self.denomina
