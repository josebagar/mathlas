# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.object import MathlasObject
from mathlas.machine_learning.neural_network import NeuralNetwork


class InputOutputHiddenMarkovModel(MathlasObject):

    def __init__(self, mode, task=None, **kwargs):

        ##########################################################################################
        # Let's start checking the basics
        self.mode = mode
        if self.mode not in ["load", "train"]:
            raise ValueError("Inputted value of <mode> not recognized")

        ##########################################################################################
        # If required, auto-detect task to be performed
        additional_inputs = []
        if self.mode == "train":
            # There must be samples to check what to do with them
            if "observed_outputs" not in kwargs.keys():
                raise ValueError("No samples are provided to train the HMM")
            # Store now paramters for further use
            self.n_sequences = len(kwargs["observed_outputs"])
            self.length_of_sequences = [len(seq) for seq in kwargs["observed_outputs"]]

            # If we want to perform a classification and the emitted states are not provided we can
            #  look for how many different elements are in the samples
            if not (task == "classification" and "emitted_states" in kwargs.keys()):
                # This is the maximum number of different elements that there are in the samples.
                #  Only used if the variables are integers, otherwise it is not used.
                max_n_int_states = 5
                # Samples can only be integers, floating-point numbers, or strings
                input_type = type(kwargs["observed_outputs"][0][0])
                if input_type not in [int, float, str, np.int32]:
                    raise ValueError("The type of the variables is not an expected type, namely it "
                                     "is {}".format(input_type))
                # Check that every sample has the same type and, if we are dealing with integers,
                #  count how many different elements there are
                emitted_states = set([])
                n_different_values = 0
                for seq in kwargs["observed_outputs"]:
                    if ((input_type is np.int32) and (n_different_values <= max_n_int_states)) \
                            or ((input_type is int) and (n_different_values <= max_n_int_states)) \
                            or (input_type is str):
                        emitted_states = emitted_states.union(set(seq))
                        n_different_values = len(emitted_states)
                    for elem in seq:
                        if type(elem) != input_type:
                            raise ValueError("There are at least two different types of values in"
                                             " the samples, namely {} and {}".format(input_type,
                                                                                     type(elem)))

                # Perform auto-detection of the task to be performed.
                if task is None:
                    if input_type is float:
                        self.task = "regression"
                    elif input_type is str:
                        self.task = "classification"
                    elif input_type is int:
                        if n_different_values <= max_n_int_states:
                            self.task = "classification"
                        else:
                            self.task = "regression"
                elif task == "classification":
                    self.task = "classification"
                else:
                    self.task = task

                # Store detected emitted states and/or check that the inputted states meet the
                #  detected ones.
                if self.task == "classification":
                    if "emitted_states" in kwargs.keys():
                        self.emitted_states = kwargs["emitted_states"]
                        if (set(self.emitted_states) - emitted_states) != set():
                            raise ValueError(
                                "There are more states in the samples than in the list of"
                                " emitted states.")
                        elif (emitted_states - set(self.emitted_states)) != set():
                            raise Warning("There are more inputted states that those found on the"
                                          " samples. Input is ignored.")
                    self.emitted_states = list(emitted_states)
                    self.n_emitted_states = len(self.emitted_states)
            else:
                self.task = task
                self.emitted_states = kwargs["emitted_states"]
                self.n_emitted_states = len(self.emitted_states)

        ##########################################################################################
        # Check that all mandatoty inputs have been provided and store them as attributes
        required_inputs = {"load": ["path"],
                           "train": ["observed_inputs", "observed_outputs", "n_hidden_states",
                                     "nUnits", "activation_functions"]
                                     # "nUnits_transition", "activation_function_transition",
                                     # "nUnits_emission", "activation_function_emission"]
                           }
        for key in (required_inputs[mode] + additional_inputs):
            if key not in kwargs.keys():
                raise AttributeError("Required argument <{}> not provided".format(key))
            setattr(self, key, kwargs[key])
        self.path = kwargs.get("path", None)
        if self.mode == "train":
            self.min_em_improvement = kwargs.get("min_em_improvement", 1e-4)

        ##########################################################################################
        # What do we do with all this pre-processed data?
        if mode == "load":
            # ********** Load a previously instanced class **********
            self.cache(self.path)
            return

        elif mode == "train":
            # ********** Train a HMM from provided data **********
            # Check that inputs and outputs have the same number of sequences and elements
            x = self.observed_inputs
            if len(x) != self.n_sequences:
                raise ValueError("#sequences of inputs and #sequences of outputs are not the same.")
            for s in range(self.n_sequences):
                if len(x[s]) != self.length_of_sequences[s]:
                    raise ValueError("Some sequence of inputs do not have the same number of "
                                     "element its corresponding sequence of outputs have.")
            # Check that all inputs have the same size
            self.n_inputted_variables = len(x[0][0])
            for sequence in x:
                for elem in sequence:
                    if len(elem) != self.n_inputted_variables:
                        raise ValueError("Not all the elements of the inputted sequences have the "
                                         "same number of variables.")
            # Check that units and activation functions have compatible sizes
            if type(self.nUnits) != type(self.activation_functions):
                raise ValueError("<nUnits> and <activation funtions> must be of the same type")
            # Initialize number of units per network
            if isinstance(self.nUnits, dict):
                for label in ["initial", "transition", "emission"]:
                    setattr(self, "nUnits_" + label, self.nUnits[label])
            elif isinstance(self.nUnits, list):
                for label in ["initial", "transition", "emission"]:
                    setattr(self, "nUnits_" + label, self.nUnits)
            else:
                raise ValueError("<nUnits> must be a list or a dictionary")
            # Add initial and final layer to the neural networks
            for ii, var in enumerate([self.nUnits_initial, self.nUnits_transition,
                                      self.nUnits_emission]):
                var.insert(0, self.n_inputted_variables)
                if ii != 2:
                    var.insert(len(var), self.n_hidden_states)
                else:
                    var.insert(len(self.nUnits_emission), self.n_emitted_states)
            # Initialize list of methods for activation functions
            for label in ["initial", "transition", "emission"]:
                if isinstance(getattr(self, "activation_functions"), dict):
                    functions = self.activation_functions[label]
                elif isinstance(getattr(self, "activation_functions"), list):
                    functions = self.activation_functions
                else:
                    raise ValueError("<activation_functions> must be a list or a dictionary")
                activation_functions = []
                for elem in functions:
                    if elem == "sigmoid":
                        activation_functions.append(self.sigmoid)
                    elif elem == "linear":
                        activation_functions.append(self.linear)
                    elif elem == "tanh":
                        activation_functions.append(self.tanh)
                    elif elem == "ReLU":
                        activation_functions.append(self.relu)
                    else:
                        raise TypeError(
                            "Variable activation_function takes the incorrect value %s" % elem)
                setattr(self, "activation_functions_" + label, activation_functions)

            # Load seed of the generator of random numbers
            if "seed" in kwargs.keys():
                self.seed = kwargs["seed"]
                np.random.seed(kwargs["seed"])
            else:
                self.seed = None
                raise Warning("A seed to generate random numbers has not been provided.")
            # Initialize hyperparameters
            self.initial_hidden_state = np.random.rand(self.n_hidden_states)
            self.initial_hidden_state /= np.sum(self.initial_hidden_state)

            self.weights_initial = self.initialize_weights(self.nUnits_initial)
            self.weights_transition = []
            for n in range(self.n_hidden_states):
                self.weights_transition.append(self.initialize_weights(self.nUnits_transition))
            self.weights_emission = []
            for n in range(self.n_hidden_states):
                self.weights_emission.append(self.initialize_weights(self.nUnits_emission))

            # alpha, beta, cte = self.expectation()
            # log_likelihood = self.evaluate_log_likelihood(cte)
            # # print("transition matrix = ", self.transition_matrix)
            # # print("emission matrix = ", self.emission_matrix)
            # # print("log_likelihood = ", log_likelihood)
            #
            # while True:
            #     self.transition_matrix, self.emission_matrix = self.maximization(alpha, beta, cte)
            #     alpha, beta, cte = self.expectation()
            #     updated_log_likelihood = self.evaluate_log_likelihood(cte)
            #     # print("transition matrix = ", self.transition_matrix)
            #     # print("emission matrix = ", self.emission_matrix)
            #     # print("log_likelihood = ", updated_log_likelihood)
            #     # print("delta = ", updated_log_likelihood - log_likelihood)
            #     # input("LOL")
            #     if (updated_log_likelihood - log_likelihood) < self.min_em_improvement:
            #         break
            #     else:
            #         log_likelihood = updated_log_likelihood
            # self.log_likelihood = log_likelihood

    def generate_data(self, inputs):

        # Check that inputs are correct
        for x in inputs:
            if len(x) != self.n_inputted_variables:
                raise ValueError("Not all the inputs have the correct number of variables.")

        # Initialize stuff
        n_points = len(inputs)
        hidden_states = []
        observed_states = []
        phs = self.evaluate_network(self.weights_initial, self.activation_functions_initial,
                                    inputs[0])
        for n in range(n_points):
            # Choose a hidden state at random
            x = np.random.choice(range(self.n_hidden_states), p=phs)
            hidden_states.append(x)
            # Generate a new emission
            if self.task == "classification":
                pos = self.evaluate_network(self.weights_emission[x],
                                            self.activation_functions_emission,
                                            inputs[n])
                o = np.random.choice(range(self.n_emitted_states), p=pos)
                observed_states.append(self.emitted_states[o])
            elif self.task == "regression":
                raise NotImplementedError("Nope")
            # Generate probability of the next hidden state
            if (n + 1) != n_points:
                phs = self.evaluate_network(self.weights_transition[x],
                                            self.activation_functions_transition,
                                            inputs[n+1])

        return hidden_states, observed_states

    def get_most_probable_hidden_states(self, list_of_inputs, list_of_outputs):
        """
        Calculates the most probable sequence of hidden states given a sequence of observed states
         using the Viterbi algorithm.

        Parameters
        ----------
        sequences : list of lists
                    all the sequences whose most probable hidden states the user wants to calculate.

        Returns
        -------
        X : list of lists
            list of most probable sequences of hidden states.
        """

        # Method to calculate the emission probability
        def prob(hmm, u, y):
            if hmm.task == "regression":
                raise NotImplementedError("Regression is not currently implemented")
            elif hmm.task == "classification":
                return hmm.evaluate_network(self.weights_emission[y],
                                            self.activation_functions_emission, u)

        X = []
        for inputs, outputs in zip(list_of_inputs, list_of_outputs):
            # Intitialize variables
            seq_length = len(inputs)
            if seq_length != len(outputs):
                raise ValueError("Some sequence of inputs and outputs do not have the same length")
            probabilities = []
            hidden_sequences = []

            # First step
            p_z = self.evaluate_network(self.weights_initial, self.activation_functions_initial,
                                        inputs[0])
            emission_matrix = self.calculate_emission_matrix(inputs[0])
            p = np.log2(p_z * emission_matrix[outputs[0], :])
            probabilities.append(p)
            z = np.zeros(self.n_hidden_states)
            hidden_sequences.append(z)
            # Forward steps
            for n in range(1, seq_length):
                p = probabilities[-1]
                transition_matrix = self.calculate_transition_matrix(inputs[n])
                emission_matrix = self.calculate_emission_matrix(inputs[n])
                a = np.log2(emission_matrix[outputs[n], :])
                b = np.log2(transition_matrix) + p
                probabilities.append(a + np.amax(b, axis=1))
                hidden_sequences.append(np.argmax(b, axis=1))
            # Backward steps
            x = np.ones(seq_length, dtype=int)
            x[-1] = np.argmax(probabilities[-1])
            for n in range(seq_length - 1, 0, -1):
                x[n - 1] = hidden_sequences[n][x[n]]
            X.append(x)

        return X

    def calculate_transition_matrix(self, x):
        """
        Calculates the transition matrix for a given input.

        Parameters
        ----------
        x : NumPy array
            vector of inputs

        Returns
        -------
        transition_matrix : NumPy array
                            The transition matrix, a_ij, from hidden state j to i
        """

        transition_matrix = np.zeros((self.n_hidden_states, self.n_hidden_states))
        for n in range(self.n_hidden_states):
            p = self.evaluate_network(self.weights_transition[n],
                                      self.activation_functions_transition, x)
            transition_matrix[:, n] = p

        return transition_matrix

    def calculate_emission_matrix(self, x):
        """
        Calculates the emission matrix for a given input.

        Parameters
        ----------
        x : NumPy array
            vector of inputs

        Returns
        -------
        emission_matrix : NumPy array
                          The emission matrix, a_ij, from hidden state j to observable state i
        """

        emission_matrix = np.zeros((self.n_emitted_states, self.n_hidden_states))
        for n in range(self.n_hidden_states):
            p = self.evaluate_network(self.weights_emission[n],
                                      self.activation_functions_emission, x)
            emission_matrix[:, n] = p

        return emission_matrix


    @staticmethod
    def evaluate_network(weights, activation_functions, x):
        """
        Evaluates a Neural Network for a given, single input.

        Parameters
        ----------
        weights : list of NumPy arrays
                  list weights between layers of the NN
        activation_functions : list of methods
                               methods that, typically, non-linearly transform the combination of
                                inputs at each layer.
        x : NumPy array
            input of the NN

        Returns
        -------
        out : NumPy array
              array of probabilities, i.e., its elements sum to one
        """

        for w, f in zip(weights, activation_functions):
            z = np.concatenate(([1], x))
            a = w.dot(z)
            x = f(a)

        return np.exp(x) / np.sum(np.exp(x))

    @staticmethod
    def initialize_weights(nUnits):
        """
        Generates the initial weights of all layers at once

        Parameters
        ----------
        nUnits : list of integers
                 number of units per layer

        Returns
        -------
        W : list of NumPy array
            list of matrices of weights. Its size is (L_out, L_in + 1) because the "average"/"bias"
             term must be included.
        """

        eps = 1.
        n_layers = len(nUnits) - 1
        weights = []
        for layerNo in range(n_layers):
            n_units_in = nUnits[layerNo]
            n_units_out = nUnits[layerNo + 1]
            weights.append(eps * (2. * np.random.rand(n_units_out, n_units_in + 1) - 1.))

        return weights

    @staticmethod
    def sigmoid(x, wanna_gradient=False):
        """
        Computes the sigmoid function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = 1. / (1.0 + np.exp(-x))
        if not wanna_gradient:
            return f
        else:
            gradf = f * (np.ones(f.shape) - f)
            return f, gradf

    @staticmethod
    def linear(x, wanna_gradient=False):
        """
        Computes the linear function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = x
        if not wanna_gradient:
            return f
        else:
            gradf = np.ones(f.shape)
            return f, gradf

    @staticmethod
    def tanh(x, wanna_gradient=False):
        """
        Computes the hyperbolic tangent function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = np.tanh(x)
        if not wanna_gradient:
            return f
        else:
            gradf = np.ones(f.shape) - f * f
            return f, gradf

    @staticmethod
    def relu(x, wanna_gradient=False):
        """
        Computes the rectified linear unit function and, if required, its gradient

        Parameters
        ----------
        x : float or NumPy array
            inputted values
        wanna_gradient : Boolean, optional
                         is gradient required? (default=False)

        Return
        ------
        f : float or NumPy array
            value of the sigmoid
        gradf : float or NumPy array
                gradient of f at x
        """
        f = np.maximum(x, np.zeros(x.shape))
        if not wanna_gradient:
            return f
        else:
            gradf = f.copy()
            gradf[f == x] = 1.
            return f, gradf