# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.doe.lhc import LHC
import numpy as np
from mathlas.plotting.mathlas_plots import MathlasPlot


# Number of points to be used ################################################
N = 15
nFixedPoints = 4  # Select 1 or 4
saveFigure = True  # If True it saves figure to pgn.file
##############################################################################
if nFixedPoints == 1:
    FixedPoints = np.array([[0.6, 0.5]])
elif nFixedPoints == 4:
    FixedPoints = np.array([[0.25, 0.55], [0.25, 0.75],
                            [0.45, 0.55], [0.45, 0.75]])


# A random case
X = LHC(N, 2, fixedPoints=FixedPoints, nIters=1, nSeeds=1)
# Plot it
plt = MathlasPlot()
plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k',
         subplot_index=121)
plt.scatter(X[:, 0], X[:, 1], marker_size=100,
            range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), subplot_index=121)
plt.scatter(FixedPoints[:, 0], FixedPoints[:, 1], marker_size=100,
            color="mathlas blue", range_x=(-0.05, 1.05), range_y=(-0.05, 1.05),
            subplot_index=121)
plt.equal_aspect_ratio()

# Optimize a configuration
X = LHC(N, 2, fixedPoints=FixedPoints)
# Plot it
plt.plot([0., 1., 1., 0., 0.], [0., 0., 1., 1., 0.], color='k',
         subplot_index=122)
plt.scatter(X[:, 0], X[:, 1], marker_size=100,
            range_x=(-0.05, 1.05), range_y=(-0.05, 1.05), subplot_index=122)
plt.scatter(FixedPoints[:, 0], FixedPoints[:, 1], marker_size=100,
            color="mathlas blue", range_x=(-0.05, 1.05), range_y=(-0.05, 1.05),
            subplot_index=122)
plt.equal_aspect_ratio()

# Save plotted figure if required
if saveFigure:
    plt.savefig('./example_lhc_n%s.png' % nFixedPoints)

# Show the results
plt.show()
