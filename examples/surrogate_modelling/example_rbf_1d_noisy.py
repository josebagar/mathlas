#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.rbf import RBF
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot
import numpy as np


def humps(x, addNoise=False):
    funVals = 1 / ((x - 0.3) ** 2 + 0.01) + 1 / ((x - 0.9) ** 2 + 0.04) - 6
    if addNoise:
        noise = 2.0 * np.random.randn(x.shape[0], x.shape[1])
        return funVals + noise
    else:
        return funVals


def test1D(rbf_kind, param=None):
    # 1D sample
    x = np.reshape(np.linspace(0., 2., 53), (53, 1))
    y = np.reshape(humps(x, addNoise=True), (53, 1))

    X = np.reshape(np.linspace(0., 2., 201), (201, 1))
    Y = np.reshape(humps(X), (201, 1))

    rbf_model_0 = RBF(x, y, rbf_kind, param=param, noiseFactor=None)
    tildeY0, S0 = rbf_model_0.get_prediction(X, provideRMSE=True)

    rbf_model_1 = RBF(x, y, rbf_kind, param=param, noiseFactor=1e-7)
    tildeY1, S1 = rbf_model_1.get_prediction(X, provideRMSE=True)

    plt = AdvancedMathlasPlot()

    plt.plot(X, Y, label="Original function", subplot_index=121)
    plt.plot(X, tildeY0, linestyle="--", label="Reconstructed function",
             color="mathlas orange", subplot_index=121)
    plt.scatter(x, y, marker_size=100, label='Control points',
                color="mathlas blue", subplot_index=121)
    plt.legend()

    plt.plot(X, Y, label="Original function", subplot_index=122)
    plt.plot(X, tildeY1, linestyle="--", label="Reconstructed function",
             color="mathlas orange", subplot_index=122)
    plt.scatter(x, y, marker_size=100, label='Control points',
                color="mathlas blue", subplot_index=122)
    plt.legend()

    plt.show()

    plt = AdvancedMathlasPlot()

    plt.plot(X, Y, label='Humps', subplot_index=121)
    plt.plot_prediction_and_uncertainty(X, tildeY0, 3. * S0, subplot_index=121)
    plt.scatter(x, y, marker_size=100, label='Control points',
                subplot_index=121)
    plt.legend()

    plt.plot(X, Y, label='Humps', subplot_index=122)
    plt.plot_prediction_and_uncertainty(X, tildeY1, 3. * S1, subplot_index=122)
    plt.scatter(x, y, marker_size=100, label='Control points',
                subplot_index=122)
    plt.legend()

    plt.show()

if __name__ == "__main__":

    test1D("Gaussian", param=1.)
    test1D("Cubic spline")
