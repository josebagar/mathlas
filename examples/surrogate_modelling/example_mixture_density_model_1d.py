#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot
from mathlas.surrogate_modelling.mixture_density_model import MixtureDensityModel


def generate_points(N=5000):

    np.random.seed(211104)
    # ========================================================================
    pi1 = 0.6
    mu1 = np.pi
    sg1 = 0.5 * np.pi
    # ========================================================================
    pi2 = 0.4
    mu2 = 0.
    sg2 = 0.1 * np.pi
    # ========================================================================

    # Generate data
    V1 = (mu1 + sg1 * np.random.randn(int(N * pi1)))
    V2 = (mu2 + sg2 * np.random.randn(int(N * pi2)))
    V = np.concatenate((V1, V2))

    return np.reshape(V, (len(V), 1))


if __name__ == "__main__":

    # Generate data
    X = generate_points()
    Y = np.exp(X)
    Z = X % (2. * np.pi)

#     print("Training model")
# #     mdm_norm = MDM(X, [2, 3, 4, 5])
# #     mdm_norm = MDM(X, [2, 3, 4, 5], path="./model_gauss.pickle")
#     mdm_norm = MDM(path="./model_gauss.pickle")
#     print("The number of components selected " +
#           "by the method is {}".format(mdm_norm.K))
#
#     meshX = np.linspace(-np.pi, 3 * np.pi, 201)
#     pdf = mdm_norm.getPdf(meshX)
#
#     plt = AdvancedMathlasPlot()
#     plt.hist(X, color="mathlas orange", nBins=50)
#     plt.plot(meshX, pdf)
#     plt.show()

#     print("Training model")
#     mdm_lognorm = MDM(Y, [2, 3, 4, 5],
#                       listOfDensityFunctions=["Log-normal"])
#     mdm_lognorm = MDM(Y, [2, 3, 4, 5],
#                       listOfDensityFunctions=["Log-normal"],
#                       path="./model_lognormal.pickle")
#     mdm_lognorm = MDM(path="./model_lognormal.pickle")
#     print("The number of components selected " +
#           "by the method is {}".format(mdm_lognorm.K))
#
#     meshY = np.linspace(0., 200, 201)
#     pdf = mdm_lognorm.getPdf(meshY)
#
#     plt = AdvancedMathlasPlot()
#     plt.hist(Y, color="mathlas orange", nBins=50, range_x=(0., 200))
#     plt.plot(meshY, pdf, range_x=(0., 200))
#     plt.show()

    print("Training model")
#     mdm_periodic = MDM(Z, [2, 3, 4, 5], listOfDensityFunctions=["Periodic"])
    mdm_periodic = MixtureDensityModel(Z, [2, 3, 4, 5], list_of_density_functions=["Periodic"],
                                       span_of_feats={0: 2. * np.pi},
                                       path="./model_periodic.pickle")
#     mdm_periodic = MDM(path="./model_periodic.pickle")
    print("The number of components selected " +
          "by the method is {}".format(mdm_periodic.n_density_functions))

    meshZ = np.linspace(0., 2. * np.pi, 201)
    pdf = mdm_periodic.get_pdf(meshZ)

    plt = AdvancedMathlasPlot()
    plt.hist(Z, color="mathlas orange", nBins=50)
    plt.plot(meshZ, pdf)
    plt.show()
