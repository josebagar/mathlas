#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.surrogate_modelling.kriging import Kriging
import matplotlib.pyplot as plt
from itertools import product


if __name__ == "__main__":

    xySamples = np.array([[0., 0.], [2., 0.], [1., 1.], [4., 1.],
                          [0., 3.], [2., 3.], [1., 4.], [4., 4.]])

    zSamples = []
    for x, y in xySamples:
        z0 = np.exp(-1.5 * (x ** 2 + y ** 2))
        z1 = np.exp(-3. * (x - 3.) ** 2 - 0.25 * (y - 3.) ** 2)
        z2 = np.exp(-1.5 * ((x - 4.) ** 2 + y ** 2))
        z3 = np.exp(-3. * (x - 3.) ** 2 - 0.25 * (y - 2.) ** 2)
        zSamples.append([z0, z1, z2, z3])
    zSamples = np.array(zSamples)

    K0 = Kriging(xySamples, zSamples, regressionType='constant')

    nPoints = 101
    xi = np.linspace(0., 4., nPoints)
    xy = []
    for x, y in product(xi, xi):
        xy.append([x, y])
    xy = np.array(xy)
    z, rmse = K0.get_prediction(xy, provideRMSE=True)
    rmse /= np.sqrt(K0.squaredSigma)
    rmse /= np.max(rmse, axis=0)

    plt.figure()

    X = np.reshape(xy[:, 0], (nPoints, nPoints))
    Y = np.reshape(xy[:, 1], (nPoints, nPoints))
    for ii in range(4):
        plt.subplot(221 + ii)
        Z = np.reshape(rmse[:, ii], (nPoints, nPoints))
        CS = plt.contour(X, Y, Z, colors='#005288', levels=[0.1, 0.25, 0.5,
                                                            0.75, 0.85, 0.95])
        plt.clabel(CS, fontsize=9, inline=1)
        plt.scatter(xySamples[:, 0], xySamples[:, 1], color='#e96d20')
        plt.axis("equal")
    plt.show()
