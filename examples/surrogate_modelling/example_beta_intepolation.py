#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.surrogate_modelling.beta_interpolation import Beta_Interpolation
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot


def genenerate_function(N):

    np.random.seed(131010)

    X = 2.0 * np.random.rand(N) - 0.5
    X = np.reshape(X, (len(X), 1))
    Y = []
    for x in X:
        lb = x * (2. - x)
        ub = x + 0.50
#         e = x + 1.
        m = x - .5
        n = (3. - 2. * x) / 4.
        continueLooping = True
        while continueLooping:
            r = np.random.rand()
#             continueLooping = 1. - np.abs(2. * r - 1.) ** e < np.random.rand()
            continueLooping = m * r + n < np.random.rand()
        y = r * (ub - lb) + lb
        Y.append(y)
    Y = np.reshape(np.array(Y), (len(Y), 1))

    return X, Y


if __name__ == "__main__":

    # Declare inputs
    X, Y = genenerate_function(N=2000)
    xRange = (-0.5, 1.5)
    yRange = (-1.2, 2.0)
    delta = 0.0001

    # Calculate model
    br = Beta_Interpolation(X, Y, [xRange], yRange, 51)
    plt = AdvancedMathlasPlot()
    # Plot model variables
    plt.plots(br.scatterPoints, [np.log10(br.alpha), np.log10(br.beta)],
              var_labels=[r"$\alpha$", r"$\beta$"],
              axis_labels=[r"X", r"$log_{10}(\alpha)$, " +
                           r"$log_{10}(\beta)$"],
              title="Exponents of the Beta distribution")
    plt.show()

    # Calculate pdf
    tildeX = np.linspace(xRange[0] + delta, xRange[1] - delta, 51)
    tildeY = np.linspace(yRange[0] + delta, yRange[1] - delta, 51)
    pdf = br.getPdf(tildeX, tildeY, productOfVariables=True)
    # Plot pdf
    plt = AdvancedMathlasPlot()
    plt.contour_plot_from_meshes(tildeX, tildeY, pdf.T, ["X", "Y"])
    plt.scatter(X, Y, transparency=True, title="Pdf and training samples")
    plt.show()

    # Calculate statistics
    average = br.getPrediction(tildeX, provideRMSE=False,
                               predictionType="Average")
    mode = br.getPrediction(tildeX, provideRMSE=False, predictionType="Mode")
    median = br.getPrediction(tildeX, provideRMSE=False,
                              predictionType="Median")
    app_median = br.getPrediction(tildeX, provideRMSE=False,
                                  predictionType="Approximate median")
    q90 = br.getCdfPoint(tildeX, .90)
    q10 = br.getCdfPoint(tildeX, .10)

    # Plot various predictions
    plt = AdvancedMathlasPlot()
    plt.plots(tildeX, [average, mode, median, app_median],
              var_labels=["Average", "Mode", "Median", "App. median"],
              axis_labels=["X", "Y"],
              colors=["mathlas orange", "mathlas orange", "mathlas blue",
                      "mathlas blue"],
              linestyles=["-", "--", "-", "--"],
              title="Various predictions for the same discriminative model")
    plt.show()

    # Plot uncertainty_interval
    plt = AdvancedMathlasPlot()
    plt.plot_prediction_and_uncertainty(tildeX, median,
                                        q90 - median, median - q10)
    plt.show()
