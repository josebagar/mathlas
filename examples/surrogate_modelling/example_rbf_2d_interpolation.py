#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from mathlas.surrogate_modelling.rbf import RBF
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot
import numpy as np


def BraninFunction2D(x, y):
    """
    Given the values of the points co-ordinates (x,y) provides
    the value of the Branin function.
    The conventional size of the domain is -5<=x<=10, 0<=y<=15
    It has three global minimums at
    i)   (x,y) = (-pi,12.275)
    ii)  (x,y) = ( pi, 2.275)
    iii) (x,y) = (9.42478,2.475)
    where it takes the value f(x,y) = 0.397887
    """
    value = ((y - x * x * 5. / 4 / np.pi / np.pi + x * 5. / np.pi - 6) ** 2. +
             10. * (1. - 1. / 8. / np.pi) * np.cos(x) + 10.)
    return value


def test2D():
    # 2D sample
    _x = np.linspace(0., 2., 7)
    x, y = map(lambda A: A.ravel(), np.meshgrid(_x, _x, sparse=False))
    sampleValues = BraninFunction2D(x, y)
    sampledPoints = np.array([x, y]).T

    _X = np.linspace(0., 2., 201)
    X, Y = map(lambda A: A.ravel(), np.meshgrid(_X, _X, sparse=False))
    Z = BraninFunction2D(X, Y)
    predictionPoints = np.array([X, Y]).T

    rbf_model = RBF(sampledPoints,
                    sampleValues,
                    "Thin plate spline", 0.5)
    tildeZ = rbf_model.get_prediction(predictionPoints)
    tildez = rbf_model.get_prediction(sampledPoints)

    print("The reconstruction error at the sampled points is")
    print(tildez[:, 0] - sampleValues)
    print('---------')
    err = np.abs((tildeZ[:, 0] - Z)/Z)

    plt = AdvancedMathlasPlot()
    plt.contourf(predictionPoints[:, 0].reshape((201, 201)),
                 predictionPoints[:, 1].reshape((201, 201)),
                 100.*err.reshape((201, 201)),
                 showColorBar=True, contour_label='Relative error (%)',
                 axis_labels=('x', 'y'))
    plt.show()


if __name__ == "__main__":

    test2D()
