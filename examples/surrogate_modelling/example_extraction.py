#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from mathlas.doe.lhc import LHC
from mathlas.surrogate_modelling.kriging import Kriging
from mathlas.plotting.advanced_mathlas_plots import AdvancedMathlasPlot


def evaluateToyModel(X):
    return np.exp(-3. * np.linalg.norm(X, axis=1))


if __name__ == "__main__":

    d = 4
    print("Calculating LHC design")
    X = LHC(10, 4)
    print("Evaluating function")
    Y = evaluateToyModel(X)
    print("Training model")
    model = Kriging(X, Y)

    print("Choosing reference points")
    D = np.linalg.norm(X, axis=1)
    indxNearest = np.argmin(D)
    indxFurthest = np.argmax(D)
    x0 = X[indxNearest, :]
    x1 = X[indxFurthest, :]
    indxMid = -1
    maxd = -1e30
    for indx in range(len(X)):
        d = (np.sqrt(np.linalg.norm(x0 - X[indx, :])) +
             np.sqrt(np.linalg.norm(x1 - X[indx, :])))
        if d > maxd:
            maxd = d
            indxMid = indx
    x2 = X[indxMid, :]
    y0 = Y[indxNearest]
    y1 = Y[indxFurthest]
    y2 = Y[indxMid]

    print("Performing first cut")
    corner = np.array([1., 1., 1., 1.])
    yc = model.get_prediction(corner)[0]
    S, xS, tildeX, tildeY, sigma = model.extract_line(x0, corner,
                                                      provide_std=True)
    plt = AdvancedMathlasPlot()
    plt.plot_prediction_and_uncertainty(S, tildeY, sigma)
    plt.scatter(xS[0], [y0], marker_size=100)
    plt.scatter(xS[1], [yc], marker_size=100, marker="v", color="mathlas blue")
    plt.show()

    print("Performing second cut")
    S, xS, tildeX, tildeY, sigma = model.extract_line(x0, x1,
                                                      provide_std=True)
    plt = AdvancedMathlasPlot()
    plt.plot_prediction_and_uncertainty(S, tildeY, sigma)
    plt.scatter(xS, [y0, y1], marker_size=100)
    plt.show()

    print("Performing third cut")
    S, xS, tildeX, tildeY, sigma = model.extract_grid(x0, x1, x2,
                                                      provide_std=True)
    plt = AdvancedMathlasPlot()
    plt.contourf(S[:, :, 0], S[:, :, 1], tildeY[:, :, 0], showColorBar=True,
                 subplot_index=211)
    plt.contourf(S[:, :, 0], S[:, :, 1], sigma[:, :, 0], showColorBar=True,
                 subplot_index=212)
    plt.scatter(xS[:, 0], xS[:, 1], marker_size=100, subplot_index=211)
    plt.scatter(xS[:, 0], xS[:, 1], marker_size=100, subplot_index=212)
    plt.show()
