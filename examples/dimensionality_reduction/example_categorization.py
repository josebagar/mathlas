# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pandas as pd
from mathlas.misc.shitty_profiler import measure_time
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.dimensionality_reduction.floats_to_categories import (EquallySpacedPartition,
                                                                   OptimalPartition,
                                                                   BinarySplitPartition)


def gen_data_2(npoints, random_seed=211104):

    np.random.seed(random_seed)
    x = np.random.rand(npoints, 3)
    for ii, r in enumerate(x):
        f0 = np.exp(-32 * (r[0] - 0.75) ** 2)
        c0 = f0 < np.random.rand()
        f1 = (np.exp(-64 * (r[1] - 0.2) ** 2) + np.exp(-64 * (r[1] - 0.8) ** 2))
        c1 = f1 < np.random.rand()
        x[ii, 2] = (c0 and c1)

    dataframe = pd.DataFrame(x, columns=["Middle", "Two-modes", "Class"])
    return dataframe


def gen_data_4(npoints, random_seed=211104):

    np.random.seed(random_seed)
    x = np.random.rand(npoints, 5)
    for ii, r in enumerate(x):
        f0 = np.exp(-8 * r[0] ** 2)
        # f0 = 1.1 if r[0] < 0.5 else -0.1
        c0 = f0 < np.random.rand()
        f1 = np.exp(-32 * (r[1] - 0.75) ** 2)
        # f1 = 1.1 if 0.25 < r[1] < 0.75 else -0.1
        c1 = f1 < np.random.rand()
        f2 = (np.exp(-64 * (r[2] - 0.2) ** 2) + np.exp(-64 * (r[2] - 0.8) ** 2))
        # f2 = 1.1 if ((0.15 < r[2] < 0.35) or (0.65 < r[2] < 0.85)) else -0.1
        c2 = f2 < np.random.rand()
        f3 = 1.5 * np.abs(r[3] - 0.4)
        # f3 = 1.1 if r[0] < 0.5 else -0.1
        c3 = f3 < np.random.rand()
        x[ii, 4] = (c0 and c1) or (c2 and c3)
        # x[ii, 4] = (c0 and c1 and c2 and c3)

    dataframe = pd.DataFrame(x, columns=["Near-zero", "Middle", "Two-modes", "Saw", "Class"])
    return dataframe


if __name__ == "__main__":

    df = gen_data_2(10000)
    n_categories = 5
    must_merge_categories = True

    labels = set(df.columns).difference({"Class"})
    sc = {key: "minmax" for key in labels}

    cat = dict([])
    f = measure_time(EquallySpacedPartition)
    cat["equispaced"] = f(df, variables=labels, n_categories=n_categories, target="Class",
                          must_merge_categories=must_merge_categories)
    f = measure_time(BinarySplitPartition)
    cat["binary split"] = f(df, target="Class", variables=labels, n_categories=n_categories,
                            should_calculate_boundaries=True, minimum_range=0.01,
                            standarization_criterion=sc,
                            must_merge_categories=must_merge_categories)
    f = measure_time(OptimalPartition)
    cat["optimal partition"] = f(df, target="Class", variables=labels, n_categories=n_categories,
                                 minimum_range=0.05, standarization_criterion=sc,
                                 must_merge_categories=must_merge_categories)

    for method in ["equispaced", "optimal partition", "binary split"]:
        for var in labels:
            plt = MathlasPlot()
            cut_points = cat[method].cut_points[var]
            interval_names = cat[method].labels[var]
            proxies = {e: ii for ii, e in enumerate(interval_names)}
            # Plot of positive events
            plt.hist(df[var][df["Class"] == True], transparency=True, normed=False, nBins=20,
                     subplot_index=211)
            for ii, ll in enumerate(interval_names):
                a = np.max([np.min(df[var]), cut_points[ii]])
                b = np.min([np.max(df[var]), cut_points[ii + 1]])
                plt.text(0.5 * (a + b), 900, "{}".format(proxies[ll]))
            # Plot of negative events
            plt.hist(df[var][df["Class"] == False], transparency=True, normed=False, nBins=20,
                     subplot_index=212, color="mathlas orange")
            for ii, ll in enumerate(interval_names):
                a = np.max([np.min(df[var]), cut_points[ii]])
                b = np.min([np.max(df[var]), cut_points[ii + 1]])
                plt.text(0.5 * (a + b), 900, "{}".format(proxies[ll]))
            # Add borders
            for p in cut_points:
                plt.plot([p, p], [0, 1000], subplot_index=211)
                plt.plot([p, p], [0, 1000], subplot_index=212)

            plt.suptitle(", ".join([method, var, "{:.3f}".format(cat[method].information_gain[var])]))
    plt.show()
