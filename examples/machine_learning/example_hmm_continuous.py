# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
from mathlas.machine_learning.hmm import HiddenMarkovModel
from mathlas.machine_learning.hmm_addons import train_several_hmm


if __name__ == "__main__":

    # Define Hidden Markov Machine
    transition_matrix = np.array([[0.4, 1, 0.4], [0.2, 0.0, 0.4], [0.4, 0.0, 0.2]])
    means = [None for _ in range(3)]
    comat = [None for _ in range(3)]
    means[0] = np.array([0., -1.00])
    comat[0] = np.array([[0.25, 0.], [0., 0.25]])
    means[1] = np.array([0., 4.])
    comat[1] = np.array([[1., 0.], [0., 1.]])
    means[2] = np.array([2.0, 0])
    comat[2] = np.array([[0.5, 0.], [0., 0.5]])
    # Generate data
    hmm = HiddenMarkovModel(mode="generate", var_type="continuous", seed=211104,
                            n_sequences=5, length_of_sequences=[100, 200, 1000, 500, 1000],
                            initial_hidden_state=np.array([0.5, 0.25, 0.25]),
                            transition_matrix=transition_matrix, emission_means=means,
                            emission_covariances=comat)

    # Get most probable hidden states by using the Viterbi algorithm
    hidden_states = hmm.get_most_probable_hidden_states(hmm.observed_samples)
    # Compare hidden states simulated and obtained by Viterbi
    for original_sequence, calculated_sequence in zip(hmm.latent_samples, hidden_states):
        error = np.sum([a != b for a, b in zip(original_sequence, calculated_sequence)])
        error *= 100. / len(original_sequence)
        sys.stdout.write("The error in the hidden states is {}%\n".format(error))
    # Train a new HMM from the generated data
    hmm_train = HiddenMarkovModel(mode="train", var_type="continuous", n_hidden_states=3,
                                  observed_samples=hmm.observed_samples, seed=211104,
                                  infimum_point=np.array([-2., -3.]),
                                  supremum_point=np.array([7., 5.]))
    hmm_best = train_several_hmm(var_type="continuous", data=hmm.observed_samples,
                                 n_hidden_states=list(range(2, 9)), n_models=10)
    sys.stdout.write("transition matrix,    {}\n".format(hmm_train.transition_matrix))
    sys.stdout.write("emission means,       {}\n".format(hmm_train.emission_means))
    sys.stdout.write("emission covariances, {}\n".format(hmm_train.emission_covariances))
    sys.stdout.write("Source model log-likelihood,  {}\n".format(np.sum(hmm.get_probability_of_sequences(hmm.observed_samples))))
    sys.stdout.write("Trained model log-likelihood, {}\n".format(hmm_train.log_likelihood))
    sys.stdout.write("Best model log-likelihood, {}\n".format(hmm_best.log_likelihood))
    sys.stdout.write("Selected number of hidden states, {}\n".format(hmm_best.n_hidden_states))
