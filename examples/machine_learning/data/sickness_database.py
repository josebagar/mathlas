import sys
import numpy as np
import pandas as pd
from scipy.stats import norm
from itertools import product


def sickness_database(number_of_data, symptoms, sickness, schema, add_noise=False,
                      noise_levels=0.05, random_seed=211104, float2categoric=None):
    """
    This method creates a database for testing decision trees.

    Parameters
    ----------
    number_of_data : integer
                     number of individual to generate

    symptoms : dictionary
               Definition of the symptoms
               Each symptom needs three fields.
                i) "float" or "categoric" depending on the character of the variable.
                ii) List of values the variable can take if i) is "categoric" or the range of
                    values the variable can take if i) is "float".
                iii) Probability of each of the categoris is i) is "categoric" or the generating
                     funciton is i) is "float".

    sickness : dictionary
               Definition of the sickneses. Each sickness has a list os values it can take.

    schema : dictionary
             List of trees that define how the we determine the resulting sickness. Desciption of
             the underlying decision tree

    add_noise : boolean. Default is False.
                Must the results follow a deterministic tree or not? If noise is added the clear
                pattern would be broken and false positives/negatives would be added to the
                database.

    noise_levels : float in the range [0, 1]. Default is 0.05
                   probability of getting a false result. It is only used if add_noise is True.

    random_seed : integer. Default is 211104.
                  value of the seed used to create random numbers

    float2categoric : None or dictionary. Default is None.
                      Dictionary of rules used to transform floating point values into categoric.

    Returns
    -------
    db : Pandas database
         The result you wanted.
    """

    if not add_noise:
        noise_levels = [0.]
    elif isinstance(noise_levels, float):
        noise_levels = [noise_levels]

    # Create Pandas database
    indices = range(number_of_data)
    sicknesses = sorted(list(sickness.keys()))
    sicknesses_mod = []
    for sname, noise in product(sicknesses, noise_levels):
        sicknesses_mod.append("{} {}".format(sname, noise))
    sickenesses_mod = sorted(sicknesses_mod)
    varnames = sorted(list(symptoms.keys())) + sicknesses_mod
    db = pd.DataFrame(np.empty((number_of_data, len(varnames))), index=indices, columns=varnames)
    # Begin the process of creating individuals
    np.random.seed(random_seed)
    for indx in indices:
        individual = {vname: None for vname in varnames}
        for symptom in symptoms:
            if symptoms[symptom][0] == "float":
                # In case the symtom is measured with floating point precission,
                # evaluate the associated function
                x0, x1 = symptoms[symptom][1]
                value = symptoms[symptom][2](np.random.rand(), x0, x1)
                # Do we not keep the whole floating point precission? Then transform variables
                if float2categoric:
                    for interval, category in float2categoric[symptom].items():
                        if interval[0] <= value < interval[1]:
                            value = category
                            break
                individual[symptom] = value
            elif symptoms[symptom][0] == "categoric":
                # If the variable is categoric choose one value at random
                individual[symptom] = np.random.choice(symptoms[symptom][1], p=symptoms[symptom][2])

        # For each sickness evaluate its schema
        for sickness_name in sickness.keys():
            d = schema[sickness_name]
            while not isinstance(d, str):
                key = list(d.keys())[0]
                if isinstance(individual[key], str):
                    d = d[key][individual[key]]
                else:
                    ldict = locals()
                    for complex_exp in d[key].keys():
                        truth_exp = True
                        for exp in complex_exp.split(":"):
                            exec("temp = {} {}".format(individual[key], exp), globals(), ldict)
                            truth_exp &= ldict["temp"]
                        if truth_exp:
                            d = d[key][complex_exp]
            l = sickness[sickness_name][:]
            ii = l.index(d)
            l.pop(ii)
            d0 = np.random.choice(l)
            r = np.random.rand()
            for noise_level in noise_levels:
                if add_noise and r < noise_level:
                    d = d0
                sickness_level = "{} {}".format(sickness_name, noise_level)
                individual[sickness_level] = d

        # Store data in the pandas database
        for vname in varnames:
            db.loc[indx, vname] = individual[vname]

    return db


def init_toy_database():

    # **** Definition of the symptoms ****
    # Each symptom needs three fields.
    # i) "float" or "categoric" depending on the character of the variable.
    # ii) List of values the variable can take if i) is "categoric" or the
    #     range of values the variable can take if i) is "float".
    # iii) Probability of each of the categoris is i) is "categoric" or the
    #      generating function is i) is "float".
    symptoms = {"temperature": ["float", [36., 40.],
                                lambda x, a, b: a + (b - a) * 6 * (0.5 * x ** 2 - (x ** 3) / 3.)],
                "headache": ["categoric", ["yes", "no"], [0.3, 0.7]],
                "vomits": ["categoric", ["never", "sometimes", "often"], [0.5, 0.4, 0.1]],
                "mucus": ["categoric", ["yes", "no"], [0.6, 0.4]]}

    # **** Definition of the sickneses ****
    # Each sickness has a list os values it can take.
    sickness = {"flu": ["yes", "no"],
                "gastroenteritis": ["no", "mild", "severe"]}

    # **** List of trees that define how the we determine the resulting sickness ****
    # Desciption of the underlying decision tree
    schema = {"flu": {"temperature": {"<37": "no",
                                      ">=37:<38": {"headache": {"no": {"mucus": {"yes": "yes",
                                                                                 "no": "no"}},
                                                                "yes": "yes"}},
                                      ">=38:<39": {"mucus": {"yes": "no",
                                                             "no": "yes"}},
                                      ">=39": {"mucus": {"yes": "no",
                                                         "no": "yes"}}}},
              "gastroenteritis": {"vomits": {"never": "no",
                                             "sometimes": {"headache": {"yes": "no",
                                                                        "no": {"temperature": {">=39": "no",
                                                                                               ">=38:<39": "severe",
                                                                                               ">=37:<38": "mild",
                                                                                               "<37": "mild"}}}},
                                             "often": {"temperature": {">=39": "no",
                                                                       ">=38:<39": "no",
                                                                       ">=37:<38": "mild",
                                                                       "<37": "severe"}}}}}
    return symptoms, sickness, schema


def init_hardcore_database():

    # **** Definition of the symptoms ****
    # Each symptom needs three fields.
    # i) "float" or "categoric" depending on the character of the variable.
    # ii) List of values the variable can take if i) is "categoric" or the
    #     range of values the variable can take if i) is "float".
    # iii) Probability of each of the categoris is i) is "categoric" or the
    #      generating funciton is i) is "float".
    symptoms = {
                # Personal data
                "age": ["float", [0., 100.],
                        lambda x, a, b: np.round(a + (b - a) * (1 - np.sqrt(1. - x)))],
                "socioeconomic status": ["categoric", ["high", "middle", "low"], [0.2, 0.5, 0.3]],
                "education": ["categoric", ["primary", "secondary", "college", "university", "PhD"],
                              [0.19, 0.4, 0.3, 0.1, 0.01]],
                "marital status": ["categoric", ["married", "single", "widowed", "civil union"],
                                   [0.25, 0.5, 0.05, 0.20]],
                "sex": ["categoric", ["Male", "Female"], [0.5, 0.5]],
                "sexual orientation": ["categoric", ["Heterosexual", "Homosexual",
                                                     "Bisexual", "Asexual"],
                                       [0.8, 0.15, 0.04, 0.01]],
                "#children": ["categoric",
                              ["0", "1", "2", "3", "4", "5"],
                              [0.7, 0.15, 0.1, 0.03, 0.01, 0.01]],
                "weight [pounds]": ["float", [130, 15], lambda x, a, b: a + b * norm.ppf(x)],
                "height [inches]": ["float", [70, 4], lambda x, a, b: a + b * norm.ppf(x)],
                "hair color": ["categoric",
                               ["black", "blonde", "brown", "auburn", "red", "gray/white"],
                               [0.3, 0.2, 0.25, 0.1, 0.05, 0.1]],
                "nationality": ["categoric",
                                ["spanish", "french", "portuguese", "german", "belgian", "dutch",
                                 "swiss", "italian", "danish", "finnish", "czech", "romanian",
                                 "polish", "austrian", "slovene", "croat", "greek", "bosniak",
                                 "serb", "hungarian", "romanian", "bulgarian", "slovak", "ucranian",
                                 "lithuanian"],
                                [0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04, 0.04,
                                 0.04, 0.04, 0.04]],
                "has piercings": ["categoric", ["None", "1", "2", ">2"], [0.8, 0.1, 0.08, 0.02]],
                "has tatoos": ["categoric", ["no", "yes"], [0.9, 0.1]],
                # Medical history
                "self-declared sexual risky behavior": ["categoric", ["yes", "no"], [0.1, 0.9]],
                "recent blood donation": ["categoric", ["yes", "no"], [0.3, 0.7]],
                "recent blood receiver": ["categoric", ["yes", "no"], [0.1, 0.9]],
                "recent head injury": ["categoric", ["yes", "no"], [0.2, 0.8]],
                "recent arm injury": ["categoric", ["yes", "no"], [0.2, 0.8]],
                "recent leg injury": ["categoric", ["yes", "no"], [0.2, 0.8]],
                "recent chest injury": ["categoric", ["yes", "no"], [0.2, 0.8]],
                # Real symptoms
                "blisters": ["categoric", ["many", "scarce", "none"], [0.01, 0.04, 0.95]],
                "feeling tired": ["categoric", ["yes", "no"], [0.01, 0.99]],
                "varicose veins": ["categoric", ["yes", "no"], [0.02, 0.98]],
                "hemorrhoids": ["categoric", ["yes", "no"], [0.02, 0.98]],
                "stomachache": ["categoric", ["yes", "no"], [0.1, 0.9]],
                "cough": ["categoric", ["yes", "no"], [0.2, 0.8]],
                "temperature [C]": ["float", [36., 40.],
                                    lambda x, a, b: a + (b - a) * 6 * (0.5 * x ** 2 - (x ** 3) / 3.)],
                "headache": ["categoric", ["yes", "no"], [0.3, 0.7]],
                "loss of appetite": ["categoric", ["yes", "no"], [0.5, 0.5]],
                "vomits": ["categoric", ["never", "sometimes", "often"], [0.5, 0.4, 0.1]],
                "mucus": ["categoric", ["yes", "no"], [0.6, 0.4]],
                "rash": ["categoric", ["yes", "no"], [0.2, 0.8]],
                "bleeding": ["categoric", ["abundant", "scarce", "none"], [0.01, 0.04, 0.95]],
                "biceps reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "brachioradialis reflex": ["categoric",
                                           ["0", "+", "++", "+++"],
                                           [0.02, 0.9, 0.07, 0.01]],
                "extensor digitorum reflex": ["categoric",
                                              ["0", "+", "++", "+++"],
                                              [0.02, 0.9, 0.07, 0.01]],
                "triceps reflex": ["categoric",
                                   ["0", "+", "++", "+++"],
                                   [0.02, 0.9, 0.07, 0.01]],
                "patellar reflex": ["categoric",
                                    ["0", "+", "++", "+++"],
                                    [0.02, 0.9, 0.07, 0.01]],
                "ankle jerk reflex": ["categoric",
                                      ["0", "+", "++", "+++"],
                                      [0.02, 0.9, 0.07, 0.01]],
                "pupillary light reflex": ["categoric",
                                           ["0", "+", "++", "+++"],
                                           [0.02, 0.9, 0.07, 0.01]],
                "accommodation reflex": ["categoric",
                                         ["0", "+", "++", "+++"],
                                         [0.02, 0.9, 0.07, 0.01]],
                "jaw jerk reflex": ["categoric",
                                    ["0", "+", "++", "+++"],
                                    [0.02, 0.9, 0.07, 0.01]],
                "corneal reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "glabellar reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "vestibulo-ocular reflex": ["categoric",
                                            ["0", "+", "++", "+++"],
                                            [0.02, 0.9, 0.07, 0.01]],
                "gag reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "gastrocolic reflex": ["categoric",
                                       ["0", "+", "++", "+++"],
                                       [0.02, 0.9, 0.07, 0.01]],
                "anocutaneous reflex": ["categoric",
                                        ["0", "+", "++", "+++"],
                                        [0.02, 0.9, 0.07, 0.01]],
                "cremasteric reflex": ["categoric",
                                       ["0", "+", "++", "+++"],
                                       [0.02, 0.9, 0.07, 0.01]],
                "mammalian diving reflex": ["categoric",
                                            ["0", "+", "++", "+++"],
                                            [0.02, 0.9, 0.07, 0.01]],
                "muscular defense": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "scratch reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "startle reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "withdrawal reflex": ["categoric",
                                      ["0", "+", "++", "+++"],
                                      [0.02, 0.9, 0.07, 0.01]],
                "cough reflex": ["categoric", ["0", "+", "++", "+++"], [0.02, 0.9, 0.07, 0.01]],
                "memory loss": ["categoric", ["no", "sometimes", "often"], [0.8, 0.15, 0.05]],
                "sweating": ["categoric", ["normal", "abundant"], ["0.7", "0.3"]],
                "disorientation": ["categoric", ["yes", "no"], [0.02, 0.98]],
                "fine motor skills": ["categoric", ["yes", "no"], [0.9, 0.1]],
                "bowel incontinence": ["categoric", ["yes", "no"], [0.9, 0.1]],
                "irascibility": ["categoric", ["yes", "no"], [0.5, 0.5]],
                "somniloquy": ["categoric", ["no", "yes"], [0.6, 0.4]],
                "somnambulism": ["categoric", ["no", "yes"], [0.9, 0.1]],
                "hemoglobin [g/L]": ["float", [140, 8], lambda x, a, b: a + b * norm.ppf(x)],
                "glycosylated hemoglobin [% of Hb]": ["float", [4.2, .4],
                                                      lambda x, a, b: a + b * norm.ppf(x)],
                "hematocrit": ["float", [.4, .05], lambda x, a, b: a + b * norm.ppf(x)],
                "white blood cell count [10^9/L]": ["float", [7, 1],
                                                    lambda x, a, b: a + b * norm.ppf(x)],
                "lymphocytes [10^9/L]": ["float", [2.5, 0.5], lambda x, a, b: a + b * norm.ppf(x)],
                "platelet [10^9/L]": ["float", [250, 50], lambda x, a, b: a + b * norm.ppf(x)],
                "fibrinogen [g/L]": ["float", [2.7, .3], lambda x, a, b: a + b * norm.ppf(x)],
                "vicosity [cP]": ["float", [1.6, 0.08], lambda x, a, b: a + b * norm.ppf(x)],
                "antithrombin [mg/mL]": ["float", [.17, .001], lambda x, a, b: a + b * norm.ppf(x)],
                "vitamin a [mug/L]": ["float", [45, 5], lambda x, a, b: a + b * norm.ppf(x)],
                "vitamin b9 [mug/L]": ["float", [10, 2], lambda x, a, b: a + b * norm.ppf(x)],
                "vitamin b12 [ng/L]": ["float", [400, 100], lambda x, a, b: a + b * norm.ppf(x)],
                "vitamin c [mg/dL]": ["float", [.9, .2], lambda x, a, b: a + b * norm.ppf(x)],
                "homocysteine [mug/dL]": ["float", [90, 20], lambda x, a, b: a + b * norm.ppf(x)],
                "systolic blood pressure": ["float", [105, 8], lambda x, a, b: a + b * norm.ppf(x)],
                "diastolic blood pressure": ["float", [70, 5], lambda x, a, b: a + b * norm.ppf(x)]
    }

    # **** Definition of the sickneses ****
    # Each sickness has a list os values it can take.
    sickness = {"flu": ["yes", "no"],
                "gastroenteritis": ["no", "mild", "severe"],
                "chickenpox": ["yes", "no"],
                "baby-like": ["yes", "no"],
                "creutzfeldt-jakob": ["no", "normal", "severe"]}

    # **** List of trees that define how the we determine the resulting sickness ****
    # Desciption of the underlying decision tree
    schema = {"flu": {"temperature [C]": {"<37": "no",
                                          ">=37:<38": {"headache": {"no": {"mucus": {"yes": "yes",
                                                                                     "no": "no"}},
                                                                    "yes": "yes"}},
                                          ">=38:<39": {"mucus": {"yes": "no",
                                                                 "no": "yes"}},
                                          ">=39": {"mucus": {"yes": "no",
                                                             "no": "yes"}}}},
              "gastroenteritis": {"vomits": {"never": "no",
                                             "sometimes": {"headache": {"yes": "no",
                                                                        "no": {"temperature [C]": {">=39": "no",
                                                                                                   ">=38:<39": "severe",
                                                                                                   ">=37:<38": "mild",
                                                                                                   "<37": "mild"}}}},
                                             "often": {"temperature [C]": {">=39": "no",
                                                                           ">=38:<39": "no",
                                                                           ">=37:<38": "mild",
                                                                           "<37": "severe"}}}},
              "chickenpox": {"headache": {"no": "no",
                                          "yes": {"vomits": {"never": "no",
                                                             "often": "no",
                                                             "sometimes": {"rash": {"yes": "yes",
                                                                                    "no": {"temperature [C]": {">=39": "no",
                                                                                                               ">=38:<39": "no",
                                                                                                               ">=37:<38": "yes",
                                                                                                               "<37": "no"}}}}}}}},
              "baby-like": {"age": {"<1": "yes",
                                    ">=1": {"fine motor skills": {"yes": "no",
                                                                  "no": {"bowel incontinence": {"no": "no",
                                                                                                "yes": {"vomits": {"never": "no",
                                                                                                                   "sometimes": "yes",
                                                                                                                   "often": "yes"}}}}}}}},
              "creutzfeldt-jakob": {"memory loss": {"no": "no",
                                                    "often": {"cough reflex": {"0": "severe",
                                                                               "+": "normal",
                                                                               "++": "no",
                                                                               "+++": "no"}},
                                                    "sometimes": {"disorientation": {"yes": {"cough reflex": {"0": "severe",
                                                                                                              "+": "normal",
                                                                                                              "++": "no",
                                                                                                              "+++": "no"}},
                                                                                     "no": {"irascibility": {"no": "no",
                                                                                                             "yes": {"cough reflex": {"0": "severe",
                                                                                                                                      "+": "normal",
                                                                                                                                      "++": "no",
                                                                                                                                      "+++": "no"}}}}}}}}}

    return symptoms, sickness, schema


if __name__ == "__main__":
    def main():
        from collections import Counter

        symptoms, sickness, schema = init_toy_database()

        fdb = sickness_database(10, symptoms, sickness, schema)
        sys.stdout.write("Floating point database\n {} \n\n".format(fdb))

        transformation_rules = {"temperature": {(-10, 37): "<37",
                                                (37, 38): ">=37:<38",
                                                (38, 39): ">=38:<39",
                                                (39, 50): ">=39"}}
        cdb = sickness_database(10, symptoms, sickness, schema,
                                float2categoric=transformation_rules)
        sys.stdout.write("Categoric database\n {} \n\n".format(cdb))

        ndb = sickness_database(10, symptoms, sickness, schema, add_noise=True, noise_levels=0.2)
        sys.stdout.write("Floating point database with noise\n {} \n\n".format(ndb))

        ################################################################################################

        symptoms, sickness, schema = init_hardcore_database()

        ndata = 1000
        fdb = sickness_database(ndata, symptoms, sickness, schema)
        sys.stdout.write("Hardcore database\n")
        sys.stdout.write("# variables {} \n".format(len(fdb.loc[0])))
        sys.stdout.write("frequency flu - {}% \n".format({k: 100. * v / ndata
                                                          for k, v in Counter(fdb["flu"]).items()}))
        sys.stdout.write("frequency gastroenteritis - {}% \n".format({k: 100. * v / ndata
                                                                      for k, v in Counter(
            fdb["gastroenteritis"]).items()}))
        sys.stdout.write("frequency chickenpox - {}% \n".format({k: 100. * v / ndata
                                                                 for k, v in Counter(
            fdb["chickenpox"]).items()}))
        sys.stdout.write("frequency baby-like - {}%\n".format({k: 100. * v / ndata
                                                               for k, v in
                                                               Counter(fdb["baby-like"]).items()}))
        sys.stdout.write("frequency creutzfeldt-jakob - {}% \n".format({k: 100. * v / ndata
                                                                        for k, v in Counter(
            fdb["creutzfeldt-jakob"]).items()}))

        sys.exit(0)

    main()
