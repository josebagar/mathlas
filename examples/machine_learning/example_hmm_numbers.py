# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
import pandas as pd
from time import time
from collections import Counter
from mathlas.plotting.mathlas_plots import MathlasPlot
from mathlas.machine_learning.hmm_addons import HiddenMarkovClassifier
from mathlas.examples.machine_learning.data.generate_numbers import (draw_zero, draw_one, draw_six,
                                                                     draw_seven, draw_eight)


def categorize_sequence(list_of_sequences, preprocessing_option, n_divisions=None, n_strokes=None):
    """
    Given a sequence of 2-D vectors, this method returns the angle of the difference between two
     consecutive points.

    Parameters
    ----------
    list_of_sequences : list of lists
                        list of sequences to be pre-processed
    preprocessing_option : string
                           can take values "categories", "angle", "displacement", or "co-ordinates"
    n_divisions : integer, optional
                  number of categories in which the angles are going to be classified. Only used
                   if <preprocessing_option> = "categories"
    n_strokes : integer, optional
                number of values kept from the sequence

    Returns
    -------
    out : list of lists
          list of pre-processed data
    """
    data_of_number = []
    for sequence in list_of_sequences:
        new_seq = []
        for ii in range(len(sequence) - 1):
            dx = sequence[ii + 1] - sequence[ii]
            rads = np.arctan2(dx[0], dx[1])
            rads %= 2. * np.pi
            if preprocessing_option == "displacement":
                new_seq.append(dx)
            elif preprocessing_option == "angle":
                new_seq.append(np.array([np.cos(rads), np.sin(rads)]))
            elif preprocessing_option == "categories":
                cat = int(rads / 2. / np.pi * n_divisions)
                new_seq.append(cat)

        if n_strokes is None:
            red_seq = new_seq
        else:
            n_elems = len(new_seq)
            step_size = np.max([int(np.floor(n_elems / n_strokes)), 1])
            red_seq = [new_seq[ii] for ii in range(0, n_elems, step_size)]
        data_of_number.append(red_seq)

    return data_of_number

if __name__ == "__main__":

    ###############################################################################################
    # Seed of the random number generator
    seed = 211104
    # Define generator functions and probabilities of getting a parameter
    generators = {"0": draw_zero, "1": draw_one, "6": draw_six, "7": draw_seven, "8": draw_eight}
    priors = {"0": 0.20, "1": 0.20, "6": 0.20, "7": 0.20, "8": 0.20}
    # How many number must be used to train the HMM
    n_numbers = {"0": 100, "1": 100, "6": 100, "7": 100, "8": 100}
    # Number of hidden states. If <data_preprocessing> != "categories" this value won't be used.
    n_hidden_states = 4
    # ##### Parameters of the pre-processing of the data #####
    # Should use co-ordinates, displacement vectors, angles, or categories?
    data_preprocessing = "displacement"
    # Number of categories in which the strokes are categorized
    n_categories = 16
    # Traget value of the graident of the figure of merit when estimating the parameters of the HMM
    target_gradient = None
    # Number of strokes
    n_strokes = 20
    # Limit the number of used strokes to the following number
    n_steps = None
    # Number of tests used per number for the creation of the confusion matrix
    n_tests = 1000
    ###############################################################################################

    # Initialize variables
    numbers, p = zip(*priors.items())
    np.random.seed(seed)
    problem = "categorical" if data_preprocessing in ["categories"] else "continuous"
    must_estimate_solution = data_preprocessing != "categories"
    n_models = 10 if data_preprocessing == "categories" else 0
    emitted_states = list(range(n_categories)) if data_preprocessing == "categories" else None
    if data_preprocessing == "categories":
        infimum, supremum = None, None
    elif data_preprocessing == "angle":
        infimum, supremum = np.array([-1., -1.]), np.array([1., 1.])
        n_hidden_states = list(range(2, 21))
    elif data_preprocessing == "displacement":
        infimum, supremum = np.array([-1., -1.]), np.array([1., 1.])
        n_hidden_states = list(range(2, 9))
    else:
        raise ValueError("<infimum> and <supremem> are not defined for this datatype")

    data = {}
    sys.stdout.write("Generating training data\n")
    for number, method in generators.items():
        data[number] = [method(seed + ii) for ii in range(n_numbers[number])]
        if data_preprocessing != "co-ordinates":
            data[number] = categorize_sequence(data[number], data_preprocessing,
                                               n_divisions=n_categories, n_strokes=n_strokes)
    # Train models
    t0 = time()
    sys.stdout.write("Training model\n")
    hmc = HiddenMarkovClassifier(data, var_type=problem, n_hidden_states=n_hidden_states,
                                 n_models=n_models, seed=seed, infimum=infimum, supremum=supremum,
                                 must_estimate_solution=must_estimate_solution,
                                 estimation_method="k-means", target_gradient=target_gradient,
                                 emitted_states=emitted_states)
    tf = time()
    training_time = tf - t0
    # Test the classifier and create confussion matrix
    confusion_matrix = pd.DataFrame(np.zeros((len(numbers), len(numbers)+3)), index=sorted(numbers),
                                    columns=sorted(numbers) + ["Precision", "Recall", "F1-score"])
    t0 = time()
    for number in numbers:
        sys.stdout.write("Checking results for number {}\n".format(number))
        sequences = [generators[number](seed * 2 + ii) for ii in range(n_tests)]
        if data_preprocessing != "co-ordinates":
            sequences = categorize_sequence(sequences, data_preprocessing,
                                            n_divisions=n_categories, n_strokes=n_steps)
        # Perform prediction
        p_of_number_given_seq = hmc.predict(sequences)
        labels, probabilities = zip(*p_of_number_given_seq.items())
        most_probable_outcome = np.argmax(np.array(probabilities), axis=0)
        res = Counter([labels[ii] for ii in most_probable_outcome])
        for n in numbers:
            confusion_matrix.loc[number, n] = res.get(n, 0.)

    confusion_matrix *= 100. / n_tests

    f1_score = 0.
    for n in numbers:
        alpha = confusion_matrix.loc[n, n]
        precision = confusion_matrix.loc[n, n] / np.sum(confusion_matrix.loc[:, n])
        recall = confusion_matrix.loc[n, n] / np.sum(confusion_matrix.loc[n, :])
        confusion_matrix.loc[n, "Precision"] = precision * 100.
        confusion_matrix.loc[n, "Recall"] = recall * 100.
        confusion_matrix.loc[n, "F1-score"] = 100. * 2 * precision * recall / (precision + recall)
        f1_score += 1. / confusion_matrix.loc[n, "F1-score"]
    f1_score = len(numbers) / f1_score
    tf = time()
    confusion_time = tf - t0

    # Show results
    sys.stdout.write("{}\n".format(confusion_matrix))
    sys.stdout.write("Harmonic F-1 score: {}\n".format(f1_score))
    sys.stdout.write("Wall time spent in training method: {:.2f}\n".format(training_time))
    sys.stdout.write("Wall time spent in calculating confusion matrix: {:.2f}\n".format(confusion_time))
    sys.stdout.write("# hidden states per number are {}\n".format({n: hmc.models[n].n_hidden_states
                                                                   for n in sorted(numbers)}))

    # Test the classifier. Loops until user closes program
    i = 0
    while True:
        # Get seed
        i += 1
        s = 10 * seed + i
        # Choose number at random at get data
        np.random.seed(s)
        number = np.random.choice(numbers, p=p)
        coords = generators[number](s)
        if data_preprocessing != "co-ordinates":
            data = categorize_sequence([coords], data_preprocessing,
                                       n_divisions=n_categories, n_strokes=n_steps)

        # Perform prediction
        p_of_number_given_seq = hmc.predict(data)
        p_of_number_given_seq = {label: 10 ** value[0]
                                 for label, value in p_of_number_given_seq.items()}
        # Plot number
        plt = MathlasPlot()
        plt.scatter(coords[:, 0], coords[:, 1], title="Number {}".format(number),
                    range_y=[-1.3, 1.3], range_x=[-1.3, 1.3])
        plt.equal_aspect_ratio()
        msg = ""
        for n in numbers:
            msg += "{}: {:.2f}%\n".format(n, p_of_number_given_seq[n] * 100.)
        plt.text(0.05, 0.5, msg, transform=plt.ax.transAxes)
        plt.show()

