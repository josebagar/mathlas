#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import time
import sqlite3
import numpy as np
import pandas as pd
from pathlib import Path
from itertools import product
from mathlas.machine_learning.adaboost import Adaboost
from mathlas.machine_learning.decision_tree import DecisionTree
from mathlas.machine_learning.random_forest import RandomForest
from mathlas.statistics.probability_distribution import ProbabilityDistribution
from mathlas.statistics.probabilistic_choice_object import ProbabilisticChoiceObject
from mathlas.examples.machine_learning.aux_ops import eval_vanilla_function, plot_2contours

"""
Benchmark of decision trees, random forests, and AdaBoost
"""


def f(x):
    if isinstance(x, ProbabilisticChoiceObject):
        return x.p.get(1, 0)
    return x

################################################################################################
# Define benchmark parameters
dt_maxdepths = [5, 10, 15, 20, 25]                  # Maximum depths of the decision trees
rf_ntrees = [5, 10, 15, 20, 25]                     # Number of trees
ab_maxdepths = [1, 2, 3, 4]                         # Maximum depths of the AB weak learners
ab_rounds = [10, 20, 30, 40, 50, 100, 150, 200]     # Number of weak learners of the adaboos
cases = ["lemniscate", "U", "circle", "triangle", "ellipse"]
noises = [0., 0.04]                                 # Standard dev of the noise in the data
nsamples = [1000, 5000]                             # Number of samples to be used
must_plot_cases = True
must_store_plots = True                             # To store plots both <must_plot_cases> and
                                                    # <must_store_plots> must be True. If only
                                                    # <must_plot_cases> is True, figures will
                                                    # be shown but not stored.
verbosity = True

# Check that the path exists and, if not, create it
base_dname = Path('results')
base_dname.mkdir(parents=True, exist_ok=True)

# Initialize storage databases
dt_errors = pd.DataFrame(np.zeros((len(dt_maxdepths), len(cases))),
                         index=dt_maxdepths, columns=cases)
dt_error_stdvs = pd.DataFrame(np.zeros((len(dt_maxdepths), len(cases))),
                              index=dt_maxdepths, columns=cases)
dt_times = pd.DataFrame(np.zeros((len(dt_maxdepths), len(cases))),
                        index=dt_maxdepths, columns=cases)
rf_errors = pd.DataFrame(np.zeros((len(rf_ntrees), len(cases))),
                         index=rf_ntrees, columns=cases)
rf_error_stdvs = pd.DataFrame(np.zeros((len(rf_ntrees), len(cases))),
                              index=rf_ntrees, columns=cases)
rf_times = pd.DataFrame(np.zeros((len(rf_ntrees), len(cases))),
                        index=rf_ntrees, columns=cases)
ab_times = pd.DataFrame(np.zeros((len(ab_maxdepths), len(cases))),
                        index=ab_maxdepths, columns=cases)

################################################################################################
# Start benchmark
for samples, noise in product(nsamples, noises):
    benchmark_dname = base_dname / 'nsamples_{}_noise_{}'.format(samples, noise)
    benchmark_dname.mkdir(parents=True, exist_ok=True)
    suffix = "noise_{}_samples_{}".format(noise, samples)
    results_db_path = benchmark_dname / 'tree_benchmark_{}.sqlite'.format(suffix)
    for case in cases:
        text = '{} - Samples: {} - Noise: {}'.format(case, samples, noise)
        sys.stdout.write("\033[1m{}\n".format(text))
        sys.stdout.write("{}\033[0m\n\n".format('='*len(text)))
        case_dname = benchmark_dname / case
        case_dname.mkdir(parents=True, exist_ok=True)
        # Initialize training and validation databases
        db = eval_vanilla_function(npoints=samples, case=case, noise=noise)
        actual_classes = eval_vanilla_function(npoints=251, seed=None, case=case)
        predicted_classes = actual_classes.copy()

        ab_errors = pd.DataFrame(np.zeros((len(ab_maxdepths), len(ab_rounds))),
                                 index=ab_maxdepths, columns=ab_rounds)
        ab_error_stdvs = pd.DataFrame(np.zeros((len(ab_maxdepths), len(ab_rounds))),
                                      index=ab_maxdepths, columns=ab_rounds)

        ########################################################################################

        # Benchmark of decision tree
        # if verbosity:
        #     sys.stdout.write("DT:\n")
        #
        # for maxdepth in dt_maxdepths:
        #     # Check if plot exists and, therefore, the case has already been calculated
        #     if must_plot_cases:
        #         if must_store_plots:
        #             store_path = case_dname / 'dt_{}_{}_maxdepth_{}.png'.format(case,
        #                                                                         suffix,
        #                                                                         maxdepth)
        #             if store_path.is_file():
        #                 sys.stdout.write("Skipping file {}\n".format(store_path))
        #                 write_dt_stats = False
        #                 continue
        #             else:
        #                 write_dt_stats = True
        #         else:
        #             store_path = None
        #
        #     # Train the decision tree
        #     t0 = time.time()
        #     dt = DecisionTree(db, "c", minDelta=0.05, maxLevels=maxdepth)
        #     t1 = time.time()
        #     dt_times.loc[maxdepth, case] = t1 - t0
        #     if verbosity:
        #         sys.stdout.write("\tTraining time:       {:.3f}s\n".format(t1 - t0))
        #
        #     # Use the decision tree to classify
        #     t0 = time.time()
        #     predicted_classes["c"] = None
        #     dt.categorize(predicted_classes, store_values="raw")
        #     t1 = time.time()
        #     predicted_classes['c'] = predicted_classes['c'].apply(f)
        #     predicted_classes.loc[0, "c"] = 0.
        #     predicted_classes.loc[1, "c"] = 1.
        #     if verbosity:
        #         sys.stdout.write("\tMax depth            {}\n".format(maxdepth))
        #         sys.stdout.write("\tClassification time: {:.3f}s\n".format(t1 - t0))
        #
        #     # Calculate accuracy and plot if required
        #     mean_error = 100. * np.mean(np.abs(predicted_classes["c"] -
        #                                        actual_classes["c"]))
        #     std_error = 100. * np.std(np.abs(predicted_classes["c"] -
        #                                      actual_classes["c"]))
        #     dt_errors.loc[maxdepth, case] = mean_error
        #     dt_error_stdvs.loc[maxdepth, case] = std_error
        #     # Show results on screen if required
        #     if verbosity:
        #         sys.stdout.write("\tError mean:          {:.3f}%\n".format(mean_error))
        #         sys.stdout.write("\tError std:           {:.3f}%\n\n".format(std_error))
        #     if must_plot_cases:
        #         plot_2contours([actual_classes, predicted_classes], store_path)
        #
        # ########################################################################################
        #
        # # Benchmark of random forest
        # # Train the random forest
        # t0 = time.time()
        # rf = RandomForest(db, "c", nTrees=np.max(rf_ntrees), nSamples=100, nTreeVars=2,
        #                   treeDepth=10, minDelta=0.05)
        # t1 = time.time()
        # training_time = t1 - t0
        # if verbosity:
        #     sys.stdout.write("RF:\n")
        #     sys.stdout.write("\tTraining time:       {:.3f}s\n\n".format(t1 - t0))
        #
        # # Use the random forest to classify
        # for ntrees in rf_ntrees:
        #     # Check if plot exists and, therefore, the case has already been calculated
        #     if must_plot_cases:
        #         if must_store_plots:
        #             store_path = case_dname / 'rf_{}_{}_ntress_{}.png'.format(case, suffix, ntrees)
        #             if store_path.is_file():
        #                 sys.stdout.write("Skipping file {}\n".format(store_path))
        #                 write_rf_stats = False
        #                 continue
        #             else:
        #                 write_rf_stats = True
        #         else:
        #             store_path = None
        #
        #     rf_times.loc[ntrees, case] = training_time * ntrees / np.max(rf_ntrees)
        #     t0 = time.time()
        #     predicted_classes["c"] = None
        #     rf.categorize(predicted_classes, store_values="raw", nLearners=ntrees)
        #     t1 = time.time()
        #     predicted_classes['c'] = predicted_classes['c'].apply(f)
        #     predicted_classes.loc[0, "c"] = 0.
        #     predicted_classes.loc[1, "c"] = 1.
        #     if verbosity:
        #         sys.stdout.write("\tntrees               {}\n".format(ntrees))
        #         sys.stdout.write("\tnSamples             {}\n".format(100))
        #         sys.stdout.write("\tClassification time: {:.3f}s\n".format(t1 - t0))
        #
        #     # Calculate accuracy and plot if required
        #     mean_error = 100. * np.mean(np.abs(predicted_classes["c"] -
        #                                        actual_classes["c"]))
        #     std_error = 100. * np.std(np.abs(predicted_classes["c"] -
        #                                      actual_classes["c"]))
        #     rf_errors.loc[ntrees, case] = mean_error
        #     rf_error_stdvs.loc[10, case] = std_error
        #     # Show results on screen if required
        #     if verbosity:
        #         sys.stdout.write("\tError mean:          {:.3f}%\n".format(mean_error))
        #         sys.stdout.write("\tError std:           {:.3f}%\n\n".format(std_error))
        #         plot_2contours([actual_classes, predicted_classes], store_path)

        ########################################################################################

        # Benchmark of adaboost
        sys.stdout.write("AB:\n")
        for maxdepth in ab_maxdepths:
            # Check if plot exists and, therefore, the case has already been calculated
            store_path = {}
            flags = []
            for n_learners in ab_rounds:
                if must_store_plots:
                    fname = "ab_{}_{}_maxdepth_{}_nlearners_{}.png".format(case, suffix,
                                                                           maxdepth,
                                                                           n_learners)
                    store_path[n_learners] = case_dname / fname
                    flags.append(store_path[n_learners].is_file())
                else:
                    store_path[n_learners] = None
                    flags.append(False)
            if np.all(flags):
                sys.stdout.write("Skipping file {}\n".format(store_path[np.max(ab_rounds)]))
                write_ab_stats = False
                continue
            else:
                write_ab_stats = True

            t0 = time.time()
            try:
                ab = Adaboost(db, "c", min_delta=0.05,
                              max_tree_depth=maxdepth,
                              rounds=max(ab_rounds))
            except RuntimeError:
                # Failed to train AdaBoost model, continue to the next one
                sys.stdout.write('Skipping case with maxdepth={} '.format(maxdepth) +
                                 'since no valid AdaBoost model could be trained\n')
                continue
            t1 = time.time()
            ab_times.loc[maxdepth, case] = t1 - t0
            if verbosity:
                sys.stdout.write("\tTraining time:       {:.3f}s\n".format(t1 - t0))

            for n_learners in ab_rounds:
                t0 = time.time()
                predicted_classes["c"] = None
                ab.categorize(predicted_classes, criterion="probabilistic",
                              n_levels=n_learners, store_values='raw')
                t1 = time.time()
                predicted_classes['c'] = predicted_classes['c'].apply(f)
                predicted_classes.loc[0, "c"] = 0.
                predicted_classes.loc[1, "c"] = 1.
                if verbosity:
                    sys.stdout.write("\tmaxdepth             {}\n".format(maxdepth))
                    sys.stdout.write("\tlearners             {}\n".format(n_learners))
                    sys.stdout.write("\tnSamples             {}\n".format(100))
                    sys.stdout.write("\tClassification time: {:.3f}s\n".format(t1 - t0))

                mean_error = 100. * np.mean(np.abs(predicted_classes["c"] -
                                                   actual_classes["c"]))
                std_error = 100. * np.std(np.abs(predicted_classes["c"] -
                                                 actual_classes["c"]))
                ab_errors.loc[maxdepth, n_learners] = mean_error
                ab_error_stdvs.loc[maxdepth, n_learners] = std_error
                # Show results on screen if told to
                if verbosity:
                    sys.stdout.write("\tError mean:          {:.3f}%\n".format(mean_error))
                    sys.stdout.write("\tError std:           {:.3f}%\n\n".format(std_error))
                if must_plot_cases:
                    plot_2contours([actual_classes, predicted_classes], store_path[n_learners])

        # Store adaboost accuracies
        conn = sqlite3.connect(results_db_path.as_posix())
        ab_errors.to_sql("AdaBoost errors - {}".format(case),
                         conn, if_exists="replace")
        ab_error_stdvs.to_sql("AdaBoost error stdvs - {}".format(case),
                              conn, if_exists="replace")
        conn.close()

    # Store results
    conn = sqlite3.connect(results_db_path.as_posix())
    # if write_dt_stats:
    #     dt_errors.to_sql("DeciTree, errors", conn, if_exists="replace")
    #     dt_error_stdvs.to_sql("DeciTree, error stdvs", conn, if_exists="replace")
    #     dt_times.to_sql("DeciTree, time", conn, if_exists="replace")
    # if write_rf_stats:
    #     rf_errors.to_sql("RndForst, errors", conn, if_exists="replace")
    #     rf_error_stdvs.to_sql("RndForst, error stdvs", conn, if_exists="replace")
    #     rf_times.to_sql("RndForst, time", conn, if_exists="replace")
    if write_ab_stats:
        ab_times.to_sql("AdaBoost, time", conn, if_exists="replace")
    conn.close()

sys.exit(0)
