#!/usr/bin/env python3

# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import sys
import numpy as np
from mathlas.optimization.optimizers import (nonlinear_conjugate_gradient,
                                             gradient_descent, enhanced_gradient_descent)


def test_function(x, wanna_gradient=True):
    """
    Branin function used to test the minimization methods.

    Parameters
    ----------
    x : NumPy array
        value at which the function must be evaluated
    wanna_gradient : Boolean, optional
                     Must the method return the gradient? If not, only the value of the function is
                      returned.

    Returns
    -------
    J : float
        value of the function at <x>
    dJ : NumPy array, optional
         gradient. Only returned if <wanna_gradient> = True
    """

    J = ((x[1] - 5.1 * (x[0] ** 2) / (4 * np.pi ** 2) +
          5 * x[0] / np.pi - 6) ** 2 +
          10 * (1 - 1. / (8. * np.pi)) * np.cos(x[0]) + 10)

    if wanna_gradient:
        eps = 1e-6
        dJ = []
        for indx in range(2):
            xp = x.copy()
            xp[indx] += eps
            Jp = test_function(xp, wanna_gradient=False)

            xm = x.copy()
            xm[indx] -= eps
            Jm = test_function(xm, wanna_gradient=False)

            dJ.append((Jp - Jm) / 2. / eps)

        return J, np.asanyarray(dJ)
    else:
        return J


if __name__ == "__main__":

    f = test_function
    x0s = [np.array([-5., 0.]), np.array([-5., 10.]), np.array([15., 0.]), np.array([15., 10.])]
    optimizers = ["gradient_descent", "enhanced_gradient_descent",
                  "nonlinear_conjugate_gradient"]

    for x0 in x0s:
        sys.stdout.write("\n\t Initial point : {}\n".format(x0))
        sys.stdout.write('Minimum must be at (-pi, 12.275), (pi, 2.275), or (9.42478, 2.475)\n')
        sys.stdout.write('Function must take value 0.397887\n')
        for optimizer in optimizers:
            opt = globals()[optimizer]
            x, fX, df, niters = opt(f, x0.copy())
            sys.stdout.write("Method is {:28}, minimum at [{:2.3f}, {:2.3f}],"
                             " f={:.3f}, niters={:5d}\n".format(optimizer, x[0], x[1], fX, niters))



