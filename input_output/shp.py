# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


"""
Generic shapefile reading routines.
Right now they only read the data (attribute) layers
"""

import sys
import ogr
import pandas as pd


def read_shp(path):
    """
    Reads the data from a shapefile and returns
    a (name, DataFrame, spatialref) tuple with the associated info.
    Right now the code can only read SHP files with one layer and
    it has only been tested with SHP files from the spanish cadastre.

    Parameters
    ----------
    path: something convertible to a path string

    Returns
    -------
    data: tuple
          (name, data, spatialref)
          The elements of the tuple are:
            * name: string
                    The internal name of the first layer
            * data: pandas DataFrame
                    The data in the attributes layer in a pandas DataFrame.
            * spatialref: ogr SpatialRef
                          The spatial reference in the first layer.
    """
    shp = ogr.Open(str(path))
    if shp is None:
        sys.stderr.write('Cannot open shapefile "{}"\n'.format(path))
        return None, None, None

    # Process layer
    if shp.GetLayerCount() > 1:
        sys.stderr.write('Cannot read fiels with more than one layer\n')
        sys.stderr.write('\tLayers: {}\n'.format(shp.GetLayerCount()))
        return None, None, None

    layer = shp.GetLayer(0)
    name = layer.GetName()
    ref = layer.GetSpatialRef()

    defn = layer.GetLayerDefn()
    columns = [defn.GetFieldDefn(i).GetName() for i in range(defn.GetFieldCount())]
    nFeatures = layer.GetFeatureCount()
    data = pd.DataFrame(columns=columns,
                        index=range(nFeatures),
                        dtype=object)
    if nFeatures == 0:
        return name, None, ref

    for i in range(nFeatures):
        feature = layer.GetFeature(i)
        for j in range(data.shape[1]):
            # MUCH faster than data.iloc[i, j] = feature.GetField(j),
            # but it does no type checking or anything
            data.set_value(i, columns[j], feature.GetField(j))

    # Set the column types
    feature = layer.GetFeature(0)
    for i, col in enumerate(columns):
        tid = feature.GetFieldType(i)
        if tid in (ogr.OFTInteger, ogr.OFTInteger64):
            if pd.isnull(data[col]).any():
                # Pandas cannot use int type if there are NaN's
                data[col] = data[col].astype(float)
            else:
                data[col] = data[col].astype(int)
        elif tid == ogr.OFTReal:
            data[col] = data[col].astype(float)
        elif tid == ogr.OFTString:
            data[col] = data[col].astype(str)
        else:
            # Data type not understood, look for the meaning of the type
            # at the enum at:
            # http://www.gdal.org/ogr__core_8h.html
            # and implement if needed
            raise NotImplementedError('Cannot determine what field type "{}" means\n'.format(tid))

    return name, data, ref
