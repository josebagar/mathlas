# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Define colors
import colorsys
import numpy as np

mathlas_blue = '#005288'
mathlas_blue_rgb = [0, 82, 136]
mathlas_orange = '#e96d20'
mathlas_orange_rgb = [233, 109, 32]
# Chromatic list colors
intermediate_yellow = '#D9E841'
intermediate_yellow_rgb = [217, 232, 65]
intermediate_green = '#15FF31'
intermediate_green_rgb = [21, 255, 49]
intermediate_cyan = '#15FF31'
intermediate_cyan_rgb = [8, 232, 227]
# Auxiliary colors
comp25 = '#0f0a3d'
comp50 = '#200726'
comp75 = '#3d0a0e'
almost_black = '#262626'
light_grey = [float(248) / float(255)] * 3


def generateListOfColors(v):
    blue_hsv = colorsys.rgb_to_hsv(*[elem / 255. for elem in mathlas_blue_rgb])
    orange_hsv = colorsys.rgb_to_hsv(*[elem / 255. for elem in mathlas_orange_rgb])

    minv = np.min(v)
    maxv = np.max(v)
    colors = []
    for ii in range(len(v)):
        f = (v[ii] - minv) / (maxv - minv)
        c = [a * f + b * (1. - f) for a, b in zip(orange_hsv, blue_hsv)]
        colors.append(colorsys.hsv_to_rgb(*c))

    return colors


def generate_chromatic_list(n):
    """
    Generates a list of colors from mathlas blue to mathlas orange.

    Parameters
    ----------
    n : integer
        number of colors to generate

    Returns
    -------
    out : list
          List of hexadecimal codes of the colors
    """
    # Import interpolation module
    from scipy.interpolate import interp1d
    # Interpolate values of each of the chromatic co-ordinates
    l = []
    for c in range(3):
        X = [0., 0.25, 0.50, 0.75, 1.0]
        Y = [mathlas_blue_rgb[c],
             intermediate_cyan_rgb[c],
             intermediate_green_rgb[c],
             intermediate_yellow_rgb[c],
             mathlas_orange_rgb[c]]
        Xint = np.linspace(0., 1., n)
        Yint = interp1d(X, Y, kind='linear')(Xint)
        l.append([int(np.round(elem)) for elem in Yint])
    l = zip(*l)
    listOfColors = []
    # Convert from RGB space to hex
    for elem in l:
        listOfColors.append(_rgb_to_hex(elem))
    # Return results
    return listOfColors


def _rgb_to_hex(rgb):
    """
    Convert RGB code into a "web" string with hex values

    Parameters
    ----------
    rgb : iterable
          values of the colors in the chromatic space

    Returns
    -------
    out : string
          Hexadecimal code of the color
    """
    return '#%02x%02x%02x' % rgb
