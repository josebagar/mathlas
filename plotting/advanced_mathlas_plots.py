# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from mathlas.plotting.mathlas_plots import MathlasPlot


class AdvancedMathlasPlot(MathlasPlot):
    def voronoi_plot(self, obj, title=None,
                     range_x=None, range_y=None,
                     axis_labels=None, fontsize=25):
        """
        Plots the 2D Voronoi diagram of a set of points.

        Parameters
        ----------
        obj : Numpy array or voronoi object.
              If a Voronoi object is provided it is plotted. Otherwise, the
              coordinates of the point must be provided as a Numpy array of
              size `Nx2`, where N is the number of points.
        range_x : tuple, optional
            The range where the X variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        range_y : tuple, optional
            The range where the Y variables can span. If given, the graph will
            only show that part of the plot.
            If None (default), the range will be automatically computed from
            the input.

        axis_labels : tuple with strings
            A tuple of two strings: the first will be used as the label for the
            X axis and the second one will be used as the label for the Y axis.
            If None, no labels will be assigned

        title : string, optional
                A title for the plot.

        fontsize : integer or string, optional
                   The size of the title in points, or a string with its
                   qualitative size. Allowed values for the string are:
                        * ``xx-small``
                        * ``x-small``
                        * ``small``
                        * ``medium``
                        * ``large``
                        * ``x-large``
                        * ``xx-large``
                   The default font size is 25pt.

        Returns
        -------
        Nothing : None.
                  Although the method does not return any variable, it plots
                  the voronoi diagram.
        """

        from scipy.spatial import Voronoi, voronoi_plot_2d

        ax = self.gca()

        try:
            self.fig = voronoi_plot_2d(obj, ax=ax)
        except ValueError:
            vor = Voronoi(obj)
            self.fig = voronoi_plot_2d(vor, ax=ax)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        self._framework_treatment(title=title, ax=ax,
                                  range_x=range_x, range_y=range_y,
                                  fontsize=fontsize)

    def plot_prediction_and_uncertainty(self, X, Y, S_larger, S_lower=None,
                                        prediction_linestyle="--",
                                        boundary_linestyle="--",
                                        subplot_index=111,
                                        axis_labels=None, title=None,
                                        fontsize=25,
                                        range_x=None, range_y=None,
                                        fill_alpha=0.5):
        """
        Plots a 1D response and its uncertainty interval.

        Parameters
        ----------
        X : List or Numpy array.
            List of values of the feat.

        Y : List or Numpy array.
            List of values of the response.

        S_larger : List or Numpy array.
                   List of values of the uncertainty to be showed for values
                   larger than the prediction.

        S_lower : List or Numpy array, optional.
                  List of values of the uncertainty to be showed for values
                  lower than the prediction.

                  If None (default) the distribution is supposed to be
                  symmetric and the values of 'S_larger` are used.

        prediction_linestyle : String, optional.
                               Linestyle used in the plot of the prediction.

                               Default value is the dashed line.

        boundary_linestyle : String, optional.
                             Linestyle used in the plot of the boundaries of
                             the uncertainty.

                             Default value is the dashed line.

        subplot_index : integer, optional
                        Index of the subplot where the plotting will occur.

        Raises
        ------
        ValueError
            If the shape of `X`, `Y`, `S_larger` or `S_lower` is not of the
            form `Nx1` and N is the same for all the four variables.
        """

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        ax = f.add_subplot(subplot_index)

        # If S_lower is not provided, distribution is supposed to be symmetric.
        if S_lower is None:
            S_lower = S_larger

        # Check that input types are correct. Otherwise change them
        X = np.asarray(X)
        if len(X.shape) == 1:
            pass
        elif len(X.shape) == 2 and X.shape[1] == 1:
            X = np.reshape(X, (len(X),))
        else:
            raise ValueError("X has an incorrect type")

        Y = np.asarray(Y)
        if isinstance(Y, list) or len(Y.shape) == 1:
            pass
        elif len(Y.shape) == 2 and Y.shape[1] == 1:
            Y = np.reshape(Y, (len(Y),))
        else:
            raise ValueError("Y has an incorrect type")

        S_larger = np.asarray(S_larger)
        if isinstance(S_larger, list) or len(S_larger.shape) == 1:
            pass
        elif len(S_larger.shape) == 2 and S_larger.shape[1] == 1:
            S_larger = np.reshape(S_larger, (len(S_larger),))
        else:
            raise ValueError("S_larger has an incorrect type")

        S_lower = np.asarray(S_lower)
        if isinstance(S_lower, list) or len(S_lower.shape) == 1:
            pass
        elif len(S_lower.shape) == 2 and S_lower.shape[1] == 1:
            S_lower = np.reshape(S_lower, (len(S_lower),))
        else:
            raise ValueError("S_lower has an incorrect type")

        # Check that shapes coincide
        if not (len(X) == len(Y) == len(S_larger) == len(S_lower)):
            raise ValueError("Shapes of the numeric inputs must be the same.")

        ax.fill_between(X, Y - S_lower, Y + S_larger,
                        facecolor=self.mathlas_orange, alpha=fill_alpha,
                        interpolate=True)
        self.plot(X, Y, linestyle=prediction_linestyle,
                  subplot_index=subplot_index)
        self.plot(X, Y + S_larger, color="mathlas orange",
                  linestyle=boundary_linestyle, subplot_index=subplot_index)
        self.plot(X, Y - S_lower, color="mathlas orange",
                  linestyle=boundary_linestyle, subplot_index=subplot_index)

        # Plot axis labels, if provided
        if axis_labels is not None:
            plt.xlabel(axis_labels[0], fontsize=fontsize)
            plt.ylabel(axis_labels[1], fontsize=fontsize)

        self._framework_treatment(ax, title, range_x, range_y)

    def contour_plot_from_meshes(self, x, y, Z, var_labels=None,
                                 reference_point=None, subplot_index=111):
        """
        Plot the contours of <Z> (matrix) given the co-ordinates of the
         independent variables, X and Y (vectors). The MathlasPlot object must
         be initialized outside the method and provided to it as an input.
        """

        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig
        _ = f.add_subplot(subplot_index)

        # Perform assertions
        nx = x.shape[0]
        ny = y.shape[0]
        nz, mz = Z.shape
        if mz == nx and nz == ny:
            pass
        elif mz == ny and nz == nx:
            Z = Z.T
        else:
            raise ValueError("Z does not have the correct shape")
        # Clean data
        Z[np.isnan(Z)] = 0.

        # Generate mesh for x-axis
        X = np.array([x])
        for _ in range(len(y) - 1):
            X = np.concatenate((X, np.array([x])))

        # Generate mesh for y-axis
        Y = np.array([y])
        for _ in range(len(x) - 1):
            Y = np.concatenate((Y, np.array([y])))
        Y = Y.T

        # Normalize value
        mx = np.max(Z, 0)
        mx[mx < 1e-30] = 1e-30

        if reference_point is None:
            V = Z
        elif isinstance(reference_point, (int, float)):
            indx = np.argmin(np.abs(X - reference_point))
            V = Z[:, indx]
        else:
            raise ValueError("Variable reference_point is not correctly " +
                             "defined")

        # Select levels
        nlevels = 6
        levels = [2e-12, 5e-12, 1e-11, 2e-11, 5e-11, 1e-10, 2e-10, 5e-10,
                  1e-9, 2e-9, 5e-9, 1e-8, 2e-8, 5e-8, 1e-7, 2e-7, 5e-7,
                  1e-6, 2e-6, 5e-6, 1e-5, 2e-5, 5e-5, 1e-4, 2e-4, 5e-4,
                  1e-3, 2e-3, 5e-3, 1e-2, 2e-2, 5e-2, 1e-1, 2e-1, 5e-1,
                  1e0, 2e0, 5e0, 1e1, 2e1, 5e1, 1e2, 2e2, 5e3,
                  1e4, 2e4, 5e4, 1e5, 2e5, 5e5, 1e6, 2e6, 5e6, 1e7]
        l = [l < np.max(V) for l in levels]
        ii = 0
        while l[ii]:
            ii += 1
            if ii + 1 == len(levels):
                break
        these_levels = levels[ii - nlevels + 1:ii + 1]

        self.contourf(X, Y, Z, axis_labels=var_labels,
                      levels=these_levels, extend="max",
                      subplot_index=subplot_index)

    def map_scatter(self, xyPoints=None, labels=None, projection='merc',
                    llcrnrlat=35, urcrnrlat=44, llcrnrlon=-10, urcrnrlon=5,
                    parallels=None, meridians=None,
                    title=None, subplot_index=111):
        """
        Draw a scatter plot over the map of a region.

        Parameters
        ----------
        xyPoints : 2D element or None
                  `xyPoints` can be of any type that can be converted into a
                  list of lists, including a pandas DataFrame.
                  Each of the rows of `xyPoints` corresponds to a point in the
                  map, with the first column corresponding to that point's
                  longitude and the second column corresponding to the
                  point's latitude.
                  If `xyPoints` is a pandas DataFrame and `labels` is not
                  given, the indices of the DataFrame will be used as point
                  labels.
        labels : list or similar or None
                 An object with the same length as the number of rows in
                 `xyPoints` with the labels to show next to the plotted points.
        projection : string
                     projection to use for the plot. Defaults to Mercator.
                     A full list can be obtained at
                        http://matplotlib.org/basemap/users/mapsetup.html
        llcrnrlat : float
                    Lower corner value of the latitude that will be plotted.
                    Defaults to 35.
        ulcrnrlat : float
                    Upper corner value of the latitude that will be plotted
                    Defaults to 44.
        llcrnrlon : float
                    Lower corner value of the longitude that will be plotted
                    Defaults to -10.
        ulcrnrlon : float
                    Upper corner value of the longitude that will be plotted
                    Defaults to 5.
        parallels : iterable or floats or None
                    Iterable with the latitudes at which the parallels must be
                    plotted.
        meridians : iterable of floats or None
                    Iterable with the longitudes at which the meridians must be
                    plotted.
        title : string or None
                Figure title to display.
        subplot_index : integer
                    Integer describing the subplot index.
                    Defaults to `111`.

        Raises
        ------
        ValueError
            If the number of labels does not match the number of points

        Example
        -------
        p = MathlasPlot()
        xyPoints = pd.DataFrame([[-3.7, 40.3]], index=('Madrid',))
        p.map_scatter(xyPoints, title="Where's Waldo?")

        plt.show()
        """
        print('Warning: This method is not working properly yet')
        # Create figure
        if not hasattr(self, "fig"):
            self._create_figure()
        f = self.fig

        # Create an axes instance
        ax = f.add_subplot(subplot_index)

        if xyPoints is not None:
            if labels is None:
                if isinstance(xyPoints, pd.DataFrame):
                    labels = xyPoints.index
                    xyPoints = xyPoints.values
            else:
                if len(xyPoints) != len(labels):
                    raise ValueError('Number of points does not match labels')
        else:
            xyPoints = []

        # Convenience function: convert the xyPoints to a list
        xyPoints = list(xyPoints)

        # llcrnrlat,llcrnrlon,urcrnrlat,urcrnrlon
        # are the lat/lon values of the lower left and upper right corners
        # of the map.
        # lat_ts is the latitude of true scale.
        # resolution = 'c' means use crude resolution coastlines.
        m = Basemap(projection=projection,
                    llcrnrlat=llcrnrlat, urcrnrlat=urcrnrlat,
                    llcrnrlon=llcrnrlon, urcrnrlon=urcrnrlon,
                    lat_ts=((llcrnrlat+urcrnrlat)/2.0),
                    resolution='i')
        m.drawcoastlines()
        m.drawcountries()
        m.fillcontinents(color='#cc9966', lake_color='white')
        # m.bluemarble()
        if parallels:
            m.drawparallels(parallels)
        if meridians:
            m.drawmeridians(meridians)
        m.drawmapboundary(fill_color='white')
        # Plot the cities
        for i, point in enumerate(xyPoints):
            x, y = m(point[0], point[1])
            m.plot(x, y, 'bo', color=self.mathlas_orange)
            if labels is not None:
                plt.text(x+10000, y+10000, labels[i])

        # Set title, and make sure the look conforms to the standard
        self._framework_treatment(ax, title=title)
