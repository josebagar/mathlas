# Copyright 2017 Mathlas Consulting S.L.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import unittest
import numpy as np
import pandas as pd
import pandas.util.testing as pdt
from pathlib import Path
from mathlas.statistics.guilbaud import guilbaud_test


class TestGuilbaudTest(unittest.TestCase):
    """
    Test the guilbaud_test function with numerical example (and some modifications of it) in
    Guilbaud's paper [1]

    References
    ----------
    [1] Guilbaud, O., Exact Kolmogorov-type tests for left-truncated and/or right-censored data
    Journal of the American Statistical Association, 1988, 83, 213-221.
    """
    guilbaud_data = [[0., 6., False],
                     [0., 6., False],
                     [0., 6., False],
                     [0., 6., True],
                     [0., 7., False],
                     [0., 9., True],
                     [0., 10., True],
                     [0., 10., False],
                     [0., 11., True],
                     [0., 13., False],
                     [0., 16., False],
                     [0., 17., True],
                     [0., 19., True],
                     [0., 20., True],
                     [0., 22., False],
                     [0., 23., False],
                     [0., 25., True],
                     [0., 32., True],
                     [0., 32., True],
                     [0., 34., True],
                     [0., 35., True]]
    guilbaud_data = pd.DataFrame(guilbaud_data, columns=["entry", "exit", "censored"])

    fn = Path('.') / 'guilbaud_test_example_results.csv'
    guilbaud_result = pd.read_csv(fn, sep=';', dtype=float)
    guilbaud_result['N0'] = guilbaud_result['N0'].astype(int)
    guilbaud_result['N1'] = guilbaud_result['N1'].astype(int)
    guilbaud_result['test_result'] = guilbaud_result['test_result'].astype(bool)

    guilbaud_truncated_data = [[0., 6., False],
                               [0., 6., False],
                               [0., 6., False],
                               [6., 6., True],
                               [3., 7., False],
                               [3., 9., True],
                               [0., 10., True],
                               [6., 10., False],
                               [3., 11., True],
                               [3., 13., False],
                               [6., 16., False],
                               [6., 17., True],
                               [0., 19., True],
                               [0., 20., True],
                               [3., 22., False],
                               [3., 23., False],
                               [6., 25., True],
                               [6., 32., True],
                               [0., 32., True],
                               [3., 34., True],
                               [6., 35., True]]
    guilbaud_truncated_data = pd.DataFrame(guilbaud_truncated_data, columns=["entry", "exit",
                                                                             "censored"])

    fn = Path('.') / 'guilbaud_test_truncated_example_results.csv'
    guilbaud_truncated_result = pd.read_csv(fn, sep=';', dtype=float)
    guilbaud_truncated_result['N0'] = guilbaud_truncated_result['N0'].astype(int)
    guilbaud_truncated_result['N1'] = guilbaud_truncated_result['N1'].astype(int)
    guilbaud_truncated_result['test_result'] = guilbaud_truncated_result['test_result'].astype(bool)

    guilbaud_data_fail = [[0., 6., False],
                          [0., 6., False],
                          [6., 6., True],
                          [3., 7., False],
                          [0., 6., False],
                          [3., 9., True],
                          [0., 10., True],
                          [6., 10., False],
                          [3., 11., True],
                          [3., 13., False],
                          [6., 16., False],
                          [6., 16., False],
                          [0., 16., False],
                          [0., 16., False],
                          [3., 16., False],
                          [3., 16., False],
                          [6., 17., False],
                          [6., 17., False],
                          [0., 17., False],
                          [3., 17., False],
                          [6., 17., False]]
    guilbaud_data_fail = pd.DataFrame(guilbaud_data_fail, columns=["entry", "exit",
                                                                   "censored"])

    @staticmethod
    def surv(x):
        """
        Survival function to be tested
        """
        return np.exp(-x/40.)

    def testGuilbaudTestAllGuilbaud(self):
        self.assertTrue(guilbaud_test(self.surv, self.guilbaud_data, 0.1))

    def testGuilbaudTestAllGuilbaudReturnTrue(self):
        result = guilbaud_test(self.surv, self.guilbaud_data, 0.1, return_iterations_data=True)
        self.assertTrue(result[0])
        pdt.assert_frame_equal(self.guilbaud_result, result[1])

    def testGuilbaudTestAllGuilbaudTruncated(self):
        self.assertTrue(guilbaud_test(self.surv, self.guilbaud_truncated_data, 0.1))

    def testGuilbaudTestAllGuilbaudTruncatedReturnTrue(self):
        result = guilbaud_test(self.surv, self.guilbaud_truncated_data, 0.1,
                               return_iterations_data=True)
        self.assertTrue(result[0])
        pdt.assert_frame_equal(self.guilbaud_truncated_result, result[1])

    def testGuilbaudTestAllGuilbaudFail(self):
        self.assertFalse(guilbaud_test(self.surv, self.guilbaud_data_fail, 0.1))


if __name__ == "__main__":
    unittest.main()