# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.special import gamma, polygamma


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the Weibull pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with lambda and kappa.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    k, l = hyperparameters
    # Evaluate pdf
    xi = samples / l
    return (k / l) * (xi ** (k - 1.)) * np.exp(-xi ** k)


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the Weibull distribution for a given set of
    points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with lambda and kappa.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    k, l = hyperparameters
    # Evaluate cdf
    return np.array([1. - np.exp(-(x / l) ** k) if x >= 0 else 0.
                     for x in samples])


def get_cdf_point(quantiles, hyperparameters):
    """
    Evaluates the point at which the quantile takes the given value for the
    Weibull distribution.

    Parameters
    ----------
    quantiles : iterable
                One-dimensional NumPy array with values or float for those quantile
                whose point must be calculated.

    hyperparameters : iterable
                      Iterable with lambda and kappa.

    Returns
    -------
    X : float or iterable
        Value of the point. If X is a float, return value is a float.
        Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    k, l = hyperparameters
    # Evaluate cdf
    return np.array([l * (- np.log(1. - q)) ** (1. / k) for q in quantiles])


def fit_from_samples(samples):
    """
    Fits the data to a Weibull distribution.

    Parameters
    ----------
    samples : iterable
        One-dimensional dataset.

    Returns
    -------
    k : float
        exponent of the Weibull distribution
    l : float
        scale factor of the Weibull distribution
    loglikelihood : float
                    natural logarithm of the likelihood function
    """
    # Evaluate some useful variables
    n_samples = len(samples)
    f1 = -1 * np.average(np.log(samples))
    # Calculate k parameter. Regula Falsi is used to calculate it.
    k = 50
    should_loop = True
    it = 0
    while should_loop:
        a, b = 0., 0.
        for x in samples:
            a += x ** k * np.log(x)
            b += x ** k
        f0 = a / b

        new_k = 1. / (f0 + f1)
        should_loop = np.abs(new_k - k) > 1e-3
        it += 1
        if it <= 10:
            k = new_k
        else:
            k += 0.5 * (new_k - k)
    # Calculate lambda parameter
    l = 0.
    for x in samples:
        l += x ** k
    l /= n_samples
    l **= 1. / k
    # Calculate likelihood
    log_likelihood = n_samples * np.log(k / l)
    for x in samples:
        log_likelihood += (k - 1.) * np.log(x / l)
        log_likelihood -= (x / l) ** k
    log_likelihood /= n_samples
    # Return results
    return k, l, log_likelihood


def fit_from_statistics(mean, stdev):
    """
    Fits the statistics to a Weibull distribution.

    Parameters
    ----------
    mean : iterable
           Average values

    stdev : iterable
            Standard deviation

    Returns
    -------
    lambda : iterable or float
             first exponent
    kappa : iterable or float
            second exponent

    Notes
    -----
    The length of `lambda` and `kappa` must be equal.
    """
    if len(mean) != len(stdev):
        raise ValueError("Inputs must be of the same size.")
    # Fitting from mean values and variances
    K = []
    for m, v in zip(mean, stdev):
        k = 50.
        constant = v * v / (m + 1e-6) ** 2
        should_loop = True
        it = 0
        while should_loop:
            g2 = gamma(1. + 2. / k)
            g1 = gamma(1. + 1. / k)
            f = g2 / g1 / g1 - 1. - constant

            dg2 = g2 * polygamma(0, 1. + 2. / k) * (-2. / k / k)
            dg1 = g1 * polygamma(0, 1. + 1. / k) * (-1. / k / k)
            df = dg2 / g1 ** 2 - 2. * g2 * dg1 / g1 ** 3.

            dk = f / df
            it += 1
            should_loop = np.abs(dk) > 1e-3 and np.abs(f) > 1e-6 and it < 500
            while k - dk < 1e-1:
                dk /= 2
            k -= dk
        K.append(k)
    K = np.array(K)
    L = np.array([m / gamma(1. + 1. / k) for m, k in zip(mean, K)])
    # Return results
    return K, L


def jacobian(MEAN, STDEV, K, L):
    """
    Jacobian used for the transformation of variables from Weibull to
    (mean, standard deviation).

    Parameters
    ----------
    MEAN : NumPy Array
           Mean values

    STDEV : NumPy Array
            Standard deviation

    K : NumPy Array
        Value of the exponent, first parameter of the Weibull distribution

    L : NumPy Array
        Value of the scale factor, second parameter of the Weibull distribution

    Returns
    -------
    out : NumPy Array
          Value of the determinant of the jacobian
    """
    jacobian = []
    for stdev, l, k in zip(STDEV, L, K):
        g1 = gamma(1. + 1. / k)
        dg1 = g1 * polygamma(0, 1. + 1. / k)
        g2 = gamma(1. + 2. / k)
        dg2 = g2 * polygamma(0, 1. + 2. / k)
        a11 = g1
        a21 = 2 * (stdev / l) ** 2
        a12 = - (l / k ** 2) * dg1
        a22 = - (2. * (l / k) ** 2) * (dg2 - g1 * dg1)
        A = np.array([[a11, a12], [a21, a22]])
        Bmu = np.array([[1.], [0.]])
        Bsg = np.array([[0.], [2. * stdev]])

        dkdmu, dldmu = np.linalg.solve(A, Bmu)
        dkdsg, dldsg = np.linalg.solve(A, Bsg)
        jacobian.append(dldmu * dkdsg - dkdmu * dldsg)

    return np.reshape(np.array(jacobian), (len(jacobian),))
