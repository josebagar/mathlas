# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.stats import expon, ksone
from mathlas.plotting.mathlas_plots import MathlasPlot


def generate_samples(n_samples, generator, random_seed, left_truncation=None, right_truncation=None,
                     left_censorship_ratio=0., right_censorship_ratio=0.):
    """
    Generate samples from a given 1D probability distribution simulating, also, truncation and censoring effects. If
     samples are left/right truncated no sample with a value lower/higher than a given threshold will be present. If
     samples are left/right censored we do not know the exact values of the sample, only that they are lower/higher or
     equal than the given value (that is, the provided value is a right/left bound for the true value, which is
     unknown).

    Parameters
    ----------
    n_samples : integer
                number of samples to be generated.
    generator : method
                method that generates random samples without accounting for truncation or censoring. It must have one
                 input (number of samples to be generated) and one output (a NumPy array that contains the samples).
    random_seed : integer
                  seed used to generate random numbers.
    left_truncation : None or float in the range [0, 1]
                      No sample with a value lower than left_truncation will be generated. If None, there won't be left
                       truncation.
    right_truncation : None or float in the range [0, 1]
                        No sample with a value higher than right_truncation will be generated. If None, there won't be
                        right truncation.
    left_censorship_ratio : float in the range [0, 1]
                            ratio of the values that will be censored from the left
    right_censorship_ratio : float in the range [0, 1]
                             ratio of the values that will be censored from the right

    Returns
    -------
    samples : NumPy array
              Array of floats
    left_censored : NumPy array
                    Array of Boolean values which are True if the value is left censored and False otherwise.
    right_censored : NumPy array
                     Array of Boolean values which are True if the value is right censored and False otherwise.
    """

    # Check inputs
    if (left_truncation is not None and right_truncation is not None and
            left_truncation >= right_truncation):
        raise ValueError("Value for left truncation must be lower than value for right truncation.")
    if left_censorship_ratio + right_censorship_ratio >= 1.:
        raise ValueError("Ratio of left and right censored data cannot be larger than one.")
    if left_censorship_ratio is not None and not (0. <= left_censorship_ratio <= 1.):
        raise ValueError("<left_censorship_ratio> must be None or a float in range [0, 1]")
    if right_censorship_ratio is not None and not (0. <= right_censorship_ratio <= 1.):
        raise ValueError("<right_censorship_ratio> must be None or a float in range [0, 1]")

    # Generate samples
    ss = 0
    samples = generator(n_samples, random_seed + ss)
    # Truncate samples and replace those dropped
    while True:
        ss += 1
        left_truncated = np.array([True] * n_samples) if left_truncation is None \
            else samples > left_truncation
        right_truncated = np.array([True] * n_samples) if right_truncation is None \
            else samples > right_truncation
        mask = np.logical_and(left_truncated, right_truncated)
        if np.sum(mask) == n_samples:
            break
        samples = np.concatenate([samples[mask],
                                  generator(n_samples - np.sum(mask), random_seed + ss)])
    # Censor samples
    np.random.seed(random_seed)
    left_censored = np.random.rand(n_samples) < left_censorship_ratio
    right_censored = np.random.rand(n_samples) < right_censorship_ratio
    # MAke sure no sample is censored in both directions
    common_mask = np.logical_and(left_censored, right_censored)
    new_values = np.random.rand(np.sum(common_mask)) < 0.5
    left_censored[common_mask] = new_values
    right_censored[common_mask] = ~new_values

    samples = np.array([v if not f else left_truncation + (v - left_truncation) * np.random.rand()
                        for v, f in zip(samples, right_censored)])
    samples = np.array([v if not f else right_truncation - (right_truncation - v) * np.random.rand()
                        for v, f in zip(samples, left_censored)])

    return samples, left_censored, right_censored


def sorted_samples(samples, left_censored, right_censored):
    """
    Sort the samples in ascending order, bringing the same ordering to its associated censorship lists .

    Parameters
    ----------
    samples : NumPy array
              Array of floats
    left_censored : NumPy array
                    Array of Boolean values which are True if the value is left censored and False otherwise.
    right_censored : NumPy array
                     Array of Boolean values which are True if the value is right censored and False otherwise.

    Returns
    -------
    samples : NumPy array
              Array of floats
    left_censored : NumPy array
                    Array of Boolean values which are True if the value is left censored and False otherwise.
    right_censored : NumPy array
                     Array of Boolean values which are True if the value is right censored and False otherwise.
    """

    samples, indices = zip(*sorted(zip(samples, range(len(samples)))))
    samples = list(samples)
    indices = list(indices)
    left_censored = left_censored[indices]
    right_censored = right_censored[indices]

    return samples, left_censored, right_censored


def plot_distribution(samples, cdf, range_x, left_truncation=None, right_truncation=None,
                      left_censored=None, right_censored=None, alpha=None, ks_testing_function=None):
    """
    Plots the theoretical and empirical Cumulative Density Functions. It can only plot the validity margins

    Parameters
    ----------
    samples : NumPy array
              Array of floats
    cdf : method
          method to calculate the theoretical cumulative density function
    range_x : list
              minimum and maximum values to be plotted. This values are overridden if there ara samples with values
               outside this range.
    left_truncation : None or float in the range [0, 1]
                      No sample with a value lower than left_truncation is present because they have not been
                       registered.
    right_truncation : None or float in the range [0, 1]
                       No sample with a value lower than right_truncation is present because they have not been
                        registered.
    left_censored : NumPy array
                    Array of Boolean values which are True if the value is left censored and False otherwise.
    right_censored : NumPy array
                     Array of Boolean values which are True if the value is right censored and False otherwise.
    alpha : float, optional
            p-value
    ks_testing_function : method
                          method to evaluate the p-value of the KS statistic. It has three inputs (value of the error,
                           number of samples, number of censored samples) and returns one output (the p-value).

    Returns
    -------
    None
    """

    # Function to generate many points of the empirical CDF for each step
    def fill_in_the_gap(x0, x1, f0, f1, delta=5e-2):
        n = int(np.ceil(x1 - x0) / delta)
        dx = [x0 + (x1 - x0) * ii / (n - 1) for ii in range(n)]
        dy = [f0] * (n-1) + [f1]

        return dx, dy

    # Check inputs
    if np.any(sorted(samples) != samples):
        raise ValueError("Samples must be provided sorted in ascending order ")
    if ((left_censored is not None and np.sum(left_censored) != 0) and
            (right_censored is not None and np.sum(right_censored) != 0)):
        raise ValueError("Samples can only be censored by, at most, one side")
    if range_x[0] > np.min(samples):
        range_x[0] = np.min(samples)
    if range_x[1] < np.max(samples):
        range_x[1] = np.max(samples)
    if left_censored is None:
        left_censored = np.array([False] * len(samples))
    if right_censored is None:
        right_censored = np.array([False] * len(samples))

    # Initiate figure
    plt = MathlasPlot()
    # Calculate theoretical and empirical CDFs and plot them
    x, ecdf = [range_x[0]], [0.]
    x_lc, y_lc = [], []
    x_rc, y_rc = [], []
    for ii, s in enumerate(sorted(samples)):
        dx, dy = fill_in_the_gap(x[-1], s, ii / len(samples), (ii + 1) / len(samples))
        x.extend(dx)
        ecdf.extend(dy)
        if left_censored[ii]:
            x_lc.append(s)
            y_lc.append((ii + 1) / len(samples))
        if right_censored[ii]:
            x_rc.append(s)
            y_rc.append((ii + 1) / len(samples))
    dx, dy = fill_in_the_gap(x[-1], range_x[1], ecdf[-1], 1.)
    x.extend(dx)
    ecdf.extend(dy)
    plt.plots(x, [cdf(x), ecdf], colors=["mathlas blue", "mathlas orange"],
              var_labels=["Theoretical CDF", "Empirical CFD"], range_x=range_x, range_y=(0, 1))
    plt.scatter(x_lc, y_lc, marker="<", marker_size=60, range_x=range_x, range_y=(0, 1))
    plt.scatter(x_rc, y_rc, marker=">", marker_size=60, range_x=range_x, range_y=(0, 1))
    # Do we want to plot the thresholds?
    if ks_testing_function is not None:
        # Let's get some parameters
        n_samples = len(samples)
        n_censored_left = np.sum(left_censored)
        n_censored_right = np.sum(right_censored)
        # Calculate margin
        if alpha is None:
            delta, alpha = ks_test(samples, cdf, ks_testing_function,
                                   left_truncation=left_truncation, right_truncation=right_truncation,
                                   left_censored=left_censored, right_censored=right_censored)
            print(delta, alpha)
        else:
            delta = get_statistic_for_given_alpha(alpha, ks_testing_function, n_samples)

        # Calculate un-censored , un-truncated thresholds
        x = np.linspace(*range_x, num=500)
        tp = [np.min([cdf(s) + delta, 1.]) for s in x]
        tm = [np.max([cdf(s) - delta, 0.]) for s in x]
        # Modify thresholds to account for the effects of truncating
        if left_truncation is not None:
            tp_ref = cdf(left_truncation) + delta
            tp = [t if s > left_truncation else tp_ref for s, t in zip(x, tp)]
            tm = [t if s > left_truncation else 0. for s, t in zip(x, tm)]
        if right_truncation is not None:
            tm_ref = cdf(right_truncation) - delta
            tp = [t if s < right_truncation else tm_ref for s, t in zip(x, tp)]
            tm = [t if s < right_truncation else 0. for s, t in zip(x, tm)]
        # Modify thresholds to account the use of censored data
        if n_censored_left != 0:
            tp_ref = n_censored_left / n_samples
            tp = [t if t > tp_ref else tp_ref for t in tp]
            tm = [t if t > tp_ref else 0. for t in tm]
        if n_censored_right != 0:
            tm_ref = 1. - n_censored_right / n_samples
            tp = [t if t < tm_ref else 1. for t in tp]
            tm = [t if t < tm_ref else tm_ref for t in tm]
        # Plot thresholds
        plt.plot(x, tp, color="k", linestyle="-.", linewidth="1")
        plt.plot(x, tm, color="k", linestyle="-.", linewidth="1")

    plt.show()


def ks_test(samples, cdf, ks_testing_function, left_truncation=None, right_truncation=None,
            left_censored=None, right_censored=None):
    """

    Parameters
    ----------
    samples : NumPy array
              Array of floats
    cdf : method
          method to calculate the theoretical cumulative density function
    ks_testing_function : method
                          method to evaluate the p-value of the KS statistic. It has three inputs (value of the error,
                           number of samples, number of censored samples) and returns one output (the p-value).
    left_truncation : None or float in the range [0, 1]
                      No sample with a value lower than left_truncation is present because they have not been
                       registered.
    right_truncation : None or float in the range [0, 1]
                       No sample with a value lower than right_truncation is present because they have not been
                        registered.
    left_censored : NumPy array
                    Array of Boolean values which are True if the value is left censored and False otherwise.
    right_censored : NumPy array
                     Array of Boolean values which are True if the value is right censored and False otherwise.

    Returns
    -------
    d : float
        KS statistic
    alpha : float
            p-value
    """

    # Check inputs
    if np.any(sorted(samples) != samples):
        raise ValueError("Samples must be provided sorted in ascending order ")
    if ((left_censored is not None and np.sum(left_censored) != 0) and
            (right_censored is not None and np.sum(right_censored) != 0)):
        raise ValueError("Samples can only be censored by, at most, one side")
    if left_censored is None:
        left_censored = np.array([False] * len(samples))
    if right_censored is None:
        right_censored = np.array([False] * len(samples))

    n_samples = len(samples)
    n_censored = np.sum(left_censored) + np.sum(right_censored)

    errors = [np.abs(cdf(s) - (ii + 1) / n_samples) for ii, s in enumerate(samples)]

    d = np.sort(errors)[::-1][n_censored]

    return d, ks_testing_function(d, n_samples, n_censored)


def get_statistic_for_given_alpha(alpha, ks_testing_function, n_samples, n_censored=0, max_delta=1e-6):
    """
    Method to calculate the value of the input variable (error) of the KS distribution that return a given p-value. The
     non-linear equaion is solved using a bisection method.

    Parameters
    ----------
    alpha : float
            p-value
    ks_testing_function : method
                          method to evaluate the p-value of the KS statistic. It has three inputs (value of the error,
                           number of samples, number of censored samples) and returns one output (the p-value).
    n_samples : integer
                number of samples
    n_censored : integer
                 number of censored samples
    max_delta : float
                maximum error allowed by the bisection method

    Returns
    -------
    out : float
          value of the error associated to the inputted level of confidence.
    """

    x = [0., 0.5, 1.]
    y = [ks_testing_function(xi, n_samples, n_censored) - alpha for xi in x]
    if y[0] * y[2] > 0:
        raise ValueError("Initial values do not allow to run a bisection method")
    while x[2] - x[0] > max_delta:
        if y[0] * y[1] < 0:
            x = [x[0], 0.5 * (x[0] + x[1]), x[1]]
            y = [y[0], ks_testing_function(x[1], n_samples, n_censored) - alpha , y[1]]
        else:
            x = [x[1], 0.5 * (x[1] + x[2]), x[2]]
            y = [y[1], ks_testing_function(x[1], n_samples, n_censored) - alpha, y[2]]

    return x[1]


if __name__ == "__main__":

    ###########################################################################
    # Input parameters
    n_points = 10
    scale_param = 40.
    left_truncation = 6
    right_truncation = None
    left_censorship_ratio = 0.
    right_censorship_ratio = .2
    seed = 2111041
    ###########################################################################
    # Input functions
    generator_of_samples = lambda n, s: expon.rvs(size=n, scale=scale_param, random_state=s)
    cdf = lambda x: expon.cdf(x, scale=scale_param)
    # TODO: Substitute this method for the correct function
    ks_testing_function = lambda d, n, nc: 2. * ksone.sf(d, n)

    ###########################################################################
    # Generate samples
    x, lc, rc = generate_samples(n_points, generator_of_samples, random_seed=seed,
                                 left_truncation=left_truncation, right_truncation=right_truncation,
                                 left_censorship_ratio=left_censorship_ratio,
                                 right_censorship_ratio=right_censorship_ratio)
    ###########################################################################
    # Sort samples is ascending order
    x, lc, rc = sorted_samples(x, lc, rc)
    ###########################################################################
    # Plot distributions
    plot_distribution(x, cdf, (0, 200), left_truncation=left_truncation, right_truncation=right_truncation,
                      left_censored=lc, right_censored=rc, ks_testing_function=ks_testing_function)
    plot_distribution(x, cdf, (0, 200), left_truncation=left_truncation, right_truncation=right_truncation,
                      left_censored=lc, right_censored=rc, ks_testing_function=ks_testing_function, alpha=.1)

    ks_test(x, cdf, ks_testing_function,
            left_censored=lc, right_censored=rc)
