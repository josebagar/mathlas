# Copyright 2017 Joseba Echevarria García
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from scipy.special import polygamma, betainc
from scipy.special import beta as betaf


def get_pdf(samples, hyperparameters):
    """
    Evaluates the values of the Beta pdf for a given set of points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the pdf is to be evaluated

    hyperparameters : iterable
                      Iterable with alpha and beta.

    Returns
    -------
    pdf : float or iterable
          Value of the pdf. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.

    Notes
    -----
    Values must be normalized in the interval [0, 1]. No check is performed.
    User must check it.
    """
    # Unzip hyperparameters
    alpha, beta = hyperparameters

    # Evaluate pdf
    B = betaf(alpha, beta)
    if B == 0:
        ly = ((alpha - 1.) * np.log(samples + 1e-300))
        ly += ((beta - 1.) * np.log(1. - samples + 1e-300))
        a = alpha - 1.
        b = beta - 1.
        c = alpha + beta - 1.
        lb = (a * np.log(a) - a + 0.5 * np.log(2. * np.pi * a) +
              b * np.log(b) - b + 0.5 * np.log(2. * np.pi * b) -
              c * np.log(c) + c - 0.5 * np.log(2. * np.pi * c))
        return np.exp(ly - lb)
    else:
        y = (samples ** (alpha - 1.)) * ((1. - samples) ** (beta - 1.))
        return y / B


def get_cdf_quantile(samples, hyperparameters):
    """
    Evaluates the quantiles of the Beta distribution for a given set of
    points.

    Parameters
    ----------
    samples : iterable
              One-dimensional NumPy array with values or float where
               the cdf is to be evaluated

    hyperparameters : iterable
                      Iterable with alpha and beta.

    Returns
    -------
    cdf : float or iterable
          Value of the quantile. If X is a float, return value is a float.
          Otherwise the return value is a NumPy array.

    Notes
    -----
    Values must be normalized in the interval [0, 1]. No check is performed.
    User must check it.
    """
    # Unzip hyperparameters
    alpha, beta = hyperparameters
    # Evaluate cdf
    Z = []
    for x in samples:
        if x < 0.:
            v = 0.
        elif x > 1.:
            v = 1.
        else:
            v = betainc(alpha, beta, x)
        Z.append(v)
    return np.array(Z)


def get_cdf_point(quantiles, hyperparameters):
    """
    Evaluates the point at which the quantile takes the given value for the
    Beta distribution.

    Parameters
    ----------
    quantiles : iterable
                One-dimensional NumPy array with values or float for those quantile
                whose point must be calculated.

    hyperparameters : iterable
                      Iterable with alpha and beta.

    Returns
    -------
    X : float or iterable
        Value of the point. If X is a float, return value is a float.
        Otherwise the return value is a NumPy array.
    """
    # Unzip hyperparameters
    alpha, beta = hyperparameters
    # Solve the incomplete beta using a bisection method
    result = []
    for q in quantiles:
        x0, x2 = 0., 1.
        y0 = betainc(alpha, beta, x0) - q
        y2 = betainc(alpha, beta, x2) - q
        if y0 * y2 > 0:
            raise ValueError("Something strange has happened")
        err = x2 - x0
        while err > 1e-4:
            x1 = 0.5 * (x0 + x2)
            y1 = betainc(alpha, beta, x1) - q

            if y0 * y1 > 0:
                x0 = x1
                y0 = y1
            else:
                x2 = x1
                y2 = y1
            err = x2 - x0
        result.append(x1)

    return np.array(result)


def fit_from_samples(samples):
    """
    Fits the data to a beta distribution.

    Parameters
    ----------
    samples : iterable
              One-dimensional dataset.

    Returns
    -------
    alpha : iterable or float
            first exponent
    beta : iterable or float
           second exponent
    log_likelihood : float
                     natural logarithm of the likelihood function

    Notes
    -----
    Values must be normalized in the interval [0, 1]. No check is performed.
    User must check it.
    """
    # Evaluate some useful variables
    N = len(samples)
    a, b = 0., 0.
    g0, g1 = 1., 1.
    for x in samples:
        a += np.log(x)
        b += np.log(1. - x)
        g0 *= x ** (1. / N)
        g1 *= (1. - x) ** (1. / N)
    a /= N
    b /= N
    # Initialze values of alpha and beta
    alpha = 0.5 + 0.5 * g0 / (1. - g0 - g1)
    beta = 0.5 + 0.5 * g1 / (1. - g0 - g1)

    # Calculate values of alpha and beta using a Newton method to iterate
    should_loop = True
    while should_loop:
        # Calculate some useful values
        a0, a1 = polygamma(0, alpha), polygamma(1, alpha)
        b0, b1 = polygamma(0, beta), polygamma(1, beta)
        c0, c1 = polygamma(0, alpha + beta), polygamma(1, alpha + beta)
        # Evaluate function
        f = np.array([[a0 - c0 - a], [b0 - c0 - b]])
        # Evaluate Jacobian of the function
        jf = np.array([[a1 - c1, -c1], [-c1, b1 - c1]])
        # Modify values and decide if continue iteration
        delta = np.linalg.solve(jf, f)
        alpha -= delta[0, 0]
        beta -= delta[1, 0]
        should_loop = np.linalg.norm(delta) > 1e-6

    # Evaluate if the solution is a local maximum or not
    a1 = polygamma(1, alpha)
    b1 = polygamma(1, beta)
    c1 = polygamma(1, alpha + beta)
    if not (a1 - c1 > 0 and b1 - c1 > 0):
        raise ValueError("Solution found is not a local maximum")

    # Calculate likelihood
    if alpha < 100 or beta < 100:
        log_likelihood = - N * np.log(betaf(alpha, beta))
    else:
        # For large alpha and beta the evaluation of the beta function may
        # lead to an overflow. Logarithm of the beta function is evaluated
        # using the Stirling approximation
        a = alpha - 1.
        b = beta - 1.
        c = alpha + beta - 1.
        log_likelihood = (a * np.log(a) - a + 0.5 * np.log(2. * np.pi * a) +
                          b * np.log(b) - b + 0.5 * np.log(2. * np.pi * b) -
                          c * np.log(c) + c - 0.5 * np.log(2. * np.pi * c))
        log_likelihood *= -N
    lg0, lg1 = 0., 0.
    for x in samples:
        lg0 += np.log(x)
        lg1 += np.log(1. - x)
    log_likelihood += (alpha - 1.) * lg0 + (beta - 1.) * lg1
    log_likelihood /= N

    # Return results
    return alpha, beta, log_likelihood


def fit_from_statistics(mean, stdev):
    """
    Fits the statistics to a Beta distribution.

    Parameters
    ----------
    mean : iterable
           Average values

    stdev : iterable
            Standard deviation

    Returns
    -------
    alpha : iterable or float
            first exponent
    beta : iterable or float
           second exponent

    Notes
    -----
    The length of `alpha` and `beta` must be equal.

    Notes
    -----
    Values must be normalized in the interval [0, 1]. No check is performed.
    User must check it.
    """
    if len(mean) != len(stdev):
        raise ValueError("Inputs must be of the same size.")
    # Choose data to fit
    # Assign values to the maximum and the minimum
    alpha, beta = [], []
    for m, std in zip(mean, stdev):
        # Initialize some parameters
        v = std * std
        ba = 1. / (m + 1e-6) - 1.
        constant = ba / (1. + ba) ** 2 / (v + 1e-6)
        # Iterate until a solution is found
        a = 2.
        should_loop = True
        while should_loop:
            new_a = constant / (1. + ba + 1. / a)
            should_loop = np.abs(new_a - a) > 1e-3
            a = new_a
        alpha.append(a)
        beta.append(ba * a)

    return np.array(alpha).T, np.array(beta).T


def jacobian(MEAN, STDEV, A, B):
    """
    Jacobian used for the transformation of variables from Beta to
    (mean, standard deviation).

    Parameters
    ----------
    MEAN : NumPy Array
           Mean values

    STDEV : NumPy Array
            Standard deviation

    A : NumPy Array
        Value of the first exponent, first parameter of the Beta distribution

    B : NumPy Array
        Value of the second exponent, second parameter of the Beta distribution

    Returns
    -------
    out : NumPy Array
          Value of the determinant of the jacobian
    """
    jacobian = []
    for mean, stdev, a, b in zip(MEAN, STDEV, A, B):
        c1 = (a + b) ** 2
        c2 = (a + (a ** 2) / mean)
        a11 = b / c1
        a21 = -b * (mean ** 2) * (1. + 2. * a / mean) / (c2 ** 2)
        a12 = -a / c1
        a22 = (mean ** 2) / c2
        A = np.array([[a11, a12], [a21, a22]])
        b1sg = -(2 * mean * b + (a ** 2) * b / c2) / c2
        Bmu = np.array([[1.], [0.]])
        Bsg = np.array([[b1sg], [2. * stdev]])

        dadmu, dbdmu = np.linalg.solve(A, Bmu)
        dadsg, dbdsg = np.linalg.solve(A, Bsg)
        jacobian.append(dbdmu * dadsg - dadmu * dbdsg)

    return np.reshape(np.array(jacobian), (len(jacobian),))
