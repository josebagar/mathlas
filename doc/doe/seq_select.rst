.. mathlas DoE module documentation

Sequential select
=================

Description
-----------
This module includes methods used to construct and manipulate a DoE model.

Documentation
-------------

.. automodule:: mathlas.doe.seq_select
    :members:
