.. mathlas DoE module documentation

Samples
=======

Description
-----------
This module includes methods used to construct and manipulate a Sample
object.
The functionality includes:

    * Constructing the model from a training database.
    * Obtaining predictions and RMSE values for given points.

Documentation
-------------

.. module:: mathlas.doe.sample
.. autoclass:: Sample
    :members:

