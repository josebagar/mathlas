.. mathlas surrogate modelling RBF module documentation

RBF estimator
=============

Description
-----------
This module includes methods used to construct and manipulate a RBF
estimator.
The functionality includes:

    * Constructing the model from a training database.
    * Obtaining predictions and RMSE values for given points.

Documentation
-------------

.. module:: mathlas.surrogate_modelling.rbf
.. autoclass:: RBF
    :members:

