.. guilbaud module documentation

Guilbaud
========

Description
-----------
This module includes methods to evaluate Guilbaud's version of Kolmogorov-Smirnov test for
truncated and/or censored data.

Documentation
-------------

.. automodule:: mathlas.statistics.guilbaud
    :members: guilbaud_test
